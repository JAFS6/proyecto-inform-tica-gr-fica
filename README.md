Proyecto final de la asignatura Informática gráfica. Curso 2011 - 2012.

El proyecto consiste en una aplicación gráfica en 3D que representa una simulación de vida de un rebańo de ovejas. Además se aplica todo lo aprendido a lo largo de la asignatura. Desarrollado en el lenguaje de programación C++ usando la librería gráfica OpenGL.

El ejecutable del proyecto es el archivo "p8" y está compilado para Linux.

El código para gestionar la ventana de visualización y el Makefile nos fue dado por los profesores de la asignatura.
El código de la aplicación y los modelos fue escrito por Juan Antonio Fajardo Serrano.

Capturas de pantalla de la aplicación en funcionamiento:

![Overview.jpg](https://bitbucket.org/repo/byrbob/images/1768123953-Overview.jpg)

![Ovejas y ovejitas.jpg](https://bitbucket.org/repo/byrbob/images/3497283825-Ovejas%20y%20ovejitas.jpg)

![Ovejas con y sin lana.jpg](https://bitbucket.org/repo/byrbob/images/1645319590-Ovejas%20con%20y%20sin%20lana.jpg)

![Primitivas.jpg](https://bitbucket.org/repo/byrbob/images/701468688-Primitivas.jpg)

![balancin_y_tablero.jpg](https://bitbucket.org/repo/byrbob/images/1312739949-balancin_y_tablero.jpg)