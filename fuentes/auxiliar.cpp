#include "auxiliar.h"

/**************************************************************
***************** FUNCIONES DE ORDENACI�N ***********************
***************************************************************/

void ordenacionInsercionParIntFloat(pair< int,float > * v, int util)
{
      pair< int,float > aux;
      int j;
      for (int i=1; i<util; i++){
         aux=v[i];
         for (j=i-1; j>=0 && v[j].second>aux.second; j--){
            v[j+1] = v[j];
            v[j] = aux;
         }
      }
}

/**************************************************************
***************** FUNCIONES MATEM�TICAS ***********************
***************************************************************/

float distanciaEuclidea2D(float x1, float y1, float x2, float y2)
{
    return (sqrt((pow(x2-x1,2))+(pow(y2-y1,2))));
}

float normaliza_angulo(float angulo)
{
    float res = angulo;
    while(res >= 360){
        res -= 360;
    }
    while(res <= -360){
        res += 360;
    }
    return res;
}

float grados2radianes(float angulo)
{
    return ((angulo * PI)/180.0);
}

float radianes2grados(float angulo)
{
    return ((angulo * 180)/PI);
}

void normalizaVector(float &x,float &y,float &z)
{
	float modulo = moduloVector(x,y,z);
	x = x / modulo;
	y = y / modulo;
	z = z / modulo;
}

void normalizaVector(Punto3D &v)
{
	float modulo = moduloVector(v);
	v.x = v.x / modulo;
	v.y = v.y / modulo;
	v.z = v.z / modulo;
}

float moduloVector(float x,float y,float z)
{
	return sqrt(x*x+y*y+z*z);
}

float moduloVector(Punto3D v)
{
	return sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
}

Punto3D productoVectorial(Punto3D a,Punto3D b)
{
	Punto3D res;
	
	res.x = a.y*b.z - a.z*b.y;
	res.y = a.z*b.x - a.x*b.z;
	res.z = a.x*b.y - a.y*b.x;
	
	return res;
}

float productoEscalar(Punto3D a,Punto3D b)
{
	return (a.x * b.x)+(a.y * b.y)+(a.z * b.z);
}

Punto3D rotX(float ang,Punto3D v)
{
	Punto3D res;

	res.x = v.x;
	res.y = v.y*cos(ang)-v.z*sin(ang);
	res.z = v.y*sin(ang)+v.z*cos(ang);

	return res;
}

Punto3D rotY(float ang,Punto3D v)
{
	Punto3D res;

	res.x = v.x*cos(ang)+v.z*sin(ang);
	res.y = v.y;
	res.z = -v.x*sin(ang)+v.z*cos(ang);

	return res;
}

Punto3D rotZ(float ang,Punto3D v)
{
	Punto3D res;

	res.x = v.x*cos(ang)-v.y*sin(ang);
	res.y = v.x*sin(ang)+v.y*cos(ang);
	res.z = v.z;

	return res;
}

int rendondea(float n)
{
	if(n > floor(n) + 0.5) return ceil(n);
	else return floor(n);
}

/**************************************************************
***************** FUNCIONES DE TEXTURAS ***********************
***************************************************************/

int CargaTextura( const char * filename)
{
	unsigned width,height;
	
	return CargaTextura( filename, width, height);
}

int CargaTextura( const char * filename, unsigned & width, unsigned & height)
{
	std::vector<unsigned char> image;
	
	// Leer la imagen
	unsigned error = lodepng::decode(image, width, height, filename);
	
	// Si hay error se muestra y se devuelve -1
	if(error){
		std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
		return -1;
	}
	
	//Ahora los pixeles est�n en el vector "image", 4 bytes por pixel, ordenados RGBARGBA...
	
	unsigned char * data;
    
    data = new unsigned char[width * height * 4];
    
    // Almacenar en vector
    int t = image.size();
    
    for(int i=0; i<t; ++i){
    	data[i] = image[i];
    }

    GLuint texture;
        
    // Pido nombre para la textura
    glGenTextures( 1, &texture );
	
    // Selecciono la textura actual
    glBindTexture( GL_TEXTURE_2D, texture );
	
    // select modulate to mix texture with color for shading
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

    // when texture area is small, bilinear filter the closest mipmap
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
    
    // when texture area is large, bilinear filter the first mipmap
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // if wrap is true, the texture wraps over at the edges (repeat)
    //       ... false, the texture ends at the edges (clamp)
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

    // build our texture mipmaps
    gluBuild2DMipmaps( GL_TEXTURE_2D, 4, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data );
    
    // liberar la memoria
    delete [] data;

    return texture;
}

float angulo2Vectores(float X1, float Z1, float X2, float Z2)
{
	Punto3D v1;
	v1.x = X1;
	v1.y = 0;
	v1.z = Z1;
	float modulo1 = moduloVector(v1);
	
	Punto3D v2;
	v2.x = X2;
	v2.y = 0;
	v2.z = Z2;
	float modulo2 = moduloVector(v2);
	
	float producto_escalar = productoEscalar(v1,v2);
	
	return radianes2grados( acos(producto_escalar/(modulo1*modulo2)) );
}

/***********************************************************
***************** FUNCIONES DE AYUDA ***********************
************************************************************/

bool aLaDerecha(float Xorigen, float Zorigen, float Xdestino, float Zdestino, float Xpunto, float Zpunto)
{
	// Obtener vector destino->origen
	Punto2D v_destino_origen;
	
	v_destino_origen.x = Xorigen - Xdestino;
	v_destino_origen.z = Zorigen - Zdestino;
	
	// Obtener vector perpendicular al vector destino->origen
	Punto2D v_perpendicular;
	
	v_perpendicular.x = -v_destino_origen.z;
	v_perpendicular.z =  v_destino_origen.x;
	
	// Obtener vector destino->punto
	Punto2D v_destino_punto;
	
	v_destino_punto.x = Xpunto - Xdestino;
	v_destino_punto.z = Zpunto - Zdestino;
	
	if(angulo2Vectores(v_destino_punto.x,v_destino_punto.z,v_destino_origen.x,v_destino_origen.z) < 90 && 
	   angulo2Vectores(v_destino_punto.x,v_destino_punto.z,v_perpendicular.x,v_perpendicular.z) < 90)
		return true;
	else 
		return false;
}

bool dentroCuadrado(float x, float z, float e1x, float e1z, float e2x, float e2z)
{
	if(x < e1x)
		return false;
	
	if(x > e2x)
		return false;
		
	if(z < e1z)
		return false;
		
	if(z > e2z)
		return false;
	
	return true;
}

void imprimirAyuda()
{
	cout << "\n****************** AYUDA EN CONSOLA *************************\n";
    cout << "\nVista:\n\n";
    
    cout << "    Tecla de cursor Derecha  : Girar vista hacia la derecha\n";
	cout << "    Tecla de cursor Izquierda: Girar vista hacia la izquierda\n";
	cout << "    Tecla de cursor Arriba   : Girar vista hacia abajo\n";
	cout << "    Tecla de cursor Abajo    : Girar vista hacia arriba\n";
	cout << "    Q                        : Inclinar vista hacia la izquierda\n";
	cout << "    E                        : Inclinar vista hacia la derecha\n\n";

	cout << "    W     : Desplazar vista hacia delante (Avanzar)\n";
	cout << "    S     : Desplazar vista hacia atras (Retroceder)\n";
	cout << "    A     : Desplazar vista lateralmente hacia la izquierda\n";
	cout << "    D     : Desplazar vista lateralmente hacia la derecha\n";
	cout << "    CTRL+W: Desplazar vista verticalmente hacia arriba\n";
	cout << "    CTRL+S: Desplazar vista verticalmente hacia abajo\n\n";
	
	cout << "    C: Cambiar vista actual introduciendo los valores VRP, VPN, y VUP en consola\n\n";

	cout << "    CTRL+R: Reiniciar c�mara actual\n\n";
	
	cout << "    CTRL+1: Seleccionar c�mara 1 (Seleccionada por defecto)\n";
	cout << "    CTRL+2: Seleccionar c�mara 2\n";
	cout << "    CTRL+3: Seleccionar c�mara 3\n";
	cout << "    CTRL+4: Seleccionar c�mara 4\n";
	cout << "    CTRL+5: Seleccionar c�mara 5\n";
	cout << "    CTRL+6: Seleccionar c�mara 6\n";
	cout << "    CTRL+7: Seleccionar c�mara 7\n";
	cout << "    CTRL+7: Seleccionar c�mara 8 (C�mara sobre las ovejas)\n\n";
	
	cout << "    TAB: Cambiar c�mara a la oveja siguiente (S�lo si la c�mara 8 est� seleccionada)\n";
	cout << "    TAB: Cambiar c�mara a la oveja anterior  (S�lo si la c�mara 8 est� seleccionada)\n\n";

    cout << "\nModos de visualizaci�n:\n\n";

	cout << "    F1: Puntos\n";
	cout << "    F2: L�neas\n";
	cout << "    F3: Ajedrez\n";
	cout << "    F4: S�lido (por defecto)\n";
	cout << "    F5: Sin iluminaci�n\n";
	cout << "    F6: Iluminaci�n plana\n";
	cout << "    F7: Iluminaci�n Gouraud (por defecto)\n";
	cout << "    Nota: Estando en los modos F3 y F4 se tienen 3 estados con los modos F5 a F7\n\n";
	
	cout << "\nFuentes de luz:\n\n";
	
	cout << "    1: Encender/Apagar fuente de luz 1. Posici�n ( 0, 1, 0, 0) (Encendida por defecto)\n";
	cout << "    2: Encender/Apagar fuente de luz 2. Posici�n ( 0, 1, 1, 0) (Encendida por defecto)\n";
	cout << "    3: Encender/Apagar fuente de luz 3. Posici�n ( 1, 1, 0, 0) (Encendida por defecto)\n";
	cout << "    4: Encender/Apagar fuente de luz 4. Posici�n ( 0, 1,-1, 0) (Encendida por defecto)\n";
	cout << "    5: Encender/Apagar fuente de luz 5. Posici�n (-1, 1, 0, 0) (Encendida por defecto)\n\n";
	
	cout << "    9: Encender todas las luces\n";
	cout << "    0: Apagar todas las luces\n\n";
	
	cout << "    G: Activar/Desactivar luces est�ticas. (Activado por defecto) Luz est�tica: Luz fijada a un punto de la escena, si la escena gira, la luz tambi�n.\n\n";
	
	cout << "\nAnimaci�n:\n\n";
	
	cout << "    I: Iniciar animaci�n o reanudar si estaba pausada\n";
	cout << "    P: Pausar la animaci�n\n";
	cout << "    U: Detener animaci�n\n";
	cout << "    L: Elegir velocidad lineal (elegida por defecto)\n";
	cout << "    K: Elegir velocidad senoidal\n";
	cout << "    +: Aumentar velocidad\n";
	cout << "    -: Disminuir velocidad\n";
	cout << "    M: Aumentar velocidad angular\n";
	cout << "    N: Disminuir velocidad angular\n\n";
	
	cout << "\nReba�o:\n\n";
	
	cout << "    INICIO: A�adir cr�a\n";
	cout << "    FIN   : Eliminar oveja de mayor edad\n";
	cout << "    F     : Mostrar n�mero actual de ovejas\n\n";
	
    cout << "\nOtros:\n\n";

	cout << "    ALT+E: Activar/Desactivar representaci�n de los ejes de coordenadas del mundo (Desactivado por defecto)\n";
	cout << "    T    : Activar/Desactivar texturas (Activado por defecto)\n";
	cout << "    ESC  : Salir de la aplicaci�n\n";
	cout << "    H    : Mostrar ayuda en consola\n\n";
	cout << "\n*************************************************************\n";
    cout << endl;
}
