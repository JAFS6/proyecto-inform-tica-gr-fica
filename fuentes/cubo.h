#ifndef CUBO_H
#define CUBO_H

#include "_object3d.h"

class Cubo : public _Object3D
{
    private:
        float lado;
        
    public:
        /**
          @brief Crea un cubo plateado de lado 1
        */
        Cubo();
        
        /**
          @brief Crea un cubo plateado de lado l
          @param[in] l El tama�o del lado del cubo
        */
        Cubo(float l);
        
        /**
          @brief Obtiene la medida del lado del cubo
          @return La medida del lado
        */
        float getLado();
        
        /**
          @brief Asigna una nueva medida al lado del cubo y recalcula la geometr�a
          @param[in] l La nueva medida del lado
        */
        void setLado(float l);
        
    private:
        /**
          @brief Crea la geometr�a del cubo
        */
        void creaGeometria();
        
        /**
          @brief Crea la topolog�a del cubo
        */
        void creaTopologia();
};

#endif // CUBO_H

