#ifndef COCHE_H
#define COCHE_H

#include "rueda.h"
#include "faro.h"
#include "chasis.h"
#include "capota.h"
#include "_modelo3d.h"

class Coche : public _Modelo3D
{
	private:
		Rueda rueda;
		Faro faro;
		Chasis chasis;
		Capota capota;
		
	public:
		/**
			@brief Crea un coche
        */
		Coche();
		
		/**
			@brief Dibuja el coche en posición horizontal centrado en los ejes X,Y,Z orientado en el sentido positivo del eje Z
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el coche en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
};

#endif // COCHE_H

