#include "cubo.h"

Cubo::Cubo()
{
    reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
    lado = 1;
	creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

Cubo::Cubo(float l)
{
    reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
	if(l <= 0){
		lado = 1;
	}
	else{
		lado = l;
	}
	creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

float Cubo::getLado()
{
    return lado;
}

void Cubo::setLado(float l)
{
    if(l > 0){
        lado = l;
        creaGeometria();
        calcularNormalesCaras();
		calcularNormalesVertices();
    }
}

void Cubo::creaGeometria()
{
    float medio_lado = lado/2;
    vertices.clear();
    Punto3D p;
    // Abajo
    p.y = -medio_lado;
    //
    p.x = medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
    p.x = medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
    // Arriba
    p.y = medio_lado;
    //
    p.x = medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
    p.x = medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
}

void Cubo::creaTopologia()
{
    Cara c;
    caras.clear();
    // C0
    c.v1 = 0;
    c.v2 = 5;
    c.v3 = 4;
    caras.push_back(c);
    // C1
    c.v1 = 0;
    c.v2 = 1;
    c.v3 = 5;
    caras.push_back(c);
    // C2
    c.v1 = 1;
    c.v2 = 6;
    c.v3 = 5;
    caras.push_back(c);
    // C3
    c.v1 = 1;
    c.v2 = 2;
    c.v3 = 6;
    caras.push_back(c);
    // C4
    c.v1 = 2;
    c.v2 = 7;
    c.v3 = 6;
    caras.push_back(c);
    // C5
    c.v1 = 2;
    c.v2 = 3;
    c.v3 = 7;
    caras.push_back(c);
    // C6
    c.v1 = 3;
    c.v2 = 4;
    c.v3 = 7;
    caras.push_back(c);
    // C7
    c.v1 = 3;
    c.v2 = 0;
    c.v3 = 4;
    caras.push_back(c);
    // C8
    c.v1 = 5;
    c.v2 = 7;
    c.v3 = 4;
    caras.push_back(c);
    // C9
    c.v1 = 5;
    c.v2 = 6;
    c.v3 = 7;
    caras.push_back(c);
    // C10
    c.v1 = 0;
    c.v2 = 2;
    c.v3 = 1;
    caras.push_back(c);
    // C11
    c.v1 = 0;
    c.v2 = 3;
    c.v3 = 2;
    caras.push_back(c);
}

