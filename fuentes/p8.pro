######################################################################
# Automatically generated by qmake (2.01a) dom jun 17 12:07:30 2012
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .
QT += opengl
# Input
HEADERS += _impostor.h \
           _modelo3d.h \
           _o3d_animado.h \
           _o3d_modelo_animado.h \
           _o3d_solido_animado.h \
           _object3d.h \
           _object3dg.h \
           auxiliar.h \
           axis.h \
           camara.h \
           capota.h \
           casilla.h \
           chasis.h \
           cilindro.h \
           cilindrog.h \
           coche.h \
           cono.h \
           conog.h \
           cubo.h \
           cubog.h \
           escena.h \
           esfera.h \
           faro.h \
           lodepng.h \
           luz.h \
           oveja.h \
           oveja_sin.h \
           ply_object.h \
           random.h \
           rueda.h \
           tablero.h \
           tetraedro.h \
           tetraedrog.h \
           ventana.h \
           visoropengl.h
SOURCES += _impostor.cpp \
           _modelo3d.cpp \
           _o3d_animado.cpp \
           _o3d_modelo_animado.cpp \
           _o3d_solido_animado.cpp \
           _object3d.cpp \
           _object3dg.cpp \
           auxiliar.cpp \
           axis.cpp \
           camara.cpp \
           capota.cpp \
           casilla.cpp \
           chasis.cpp \
           cilindro.cpp \
           cilindrog.cpp \
           coche.cpp \
           cono.cpp \
           conog.cpp \
           cubo.cpp \
           cubog.cpp \
           escena.cpp \
           esfera.cpp \
           faro.cpp \
           lodepng.cpp \
           luz.cpp \
           oveja.cpp \
           oveja_sin.cpp \
           ply_object.cpp \
           practica8.cpp \
           rueda.cpp \
           tablero.cpp \
           tetraedro.cpp \
           tetraedrog.cpp \
           ventana.cpp \
           visoropengl.cpp
