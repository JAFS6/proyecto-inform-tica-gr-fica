#include "cilindrog.h"

CilindroG::CilindroG()
{
    altura = 1;
    radio = 0.5;
    num_divisiones = 36;
    creaGeometria();
}

void CilindroG::dibuja()
{
	glColor3f(color.r,color.g,color.b);
    // Tapa inferior
    glBegin(GL_TRIANGLE_FAN);
        glVertex3f(vertices[0].x,vertices[0].y,vertices[0].z);
        for(int i=num_divisiones; i>0; --i){
            glVertex3f(vertices[i].x,vertices[i].y,vertices[i].z);
        }
        glVertex3f(vertices[num_divisiones].x,vertices[num_divisiones].y,vertices[num_divisiones].z);
    glEnd();
    // Tapa superior
    glBegin(GL_TRIANGLE_FAN);
        int n = num_divisiones+1; // Para no hacer la misma operación 3 veces
        glVertex3f(vertices[n].x,vertices[n].y,vertices[n].z);
        n = vertices.size(); // Para no hacer la misma operación num_divisiones veces
        for(int i=num_divisiones+2; i<n; ++i){
            glVertex3f(vertices[i].x,vertices[i].y,vertices[i].z);
        }
        n = num_divisiones+2; // Para no hacer la misma operación 3 veces
        glVertex3f(vertices[n].x,vertices[n].y,vertices[n].z);
    glEnd();
    // Tira lateral
    int i,j;
    glBegin(GL_TRIANGLE_STRIP);
        n = vertices.size()-1-num_divisiones;
        for(i=vertices.size()-1,j=n-1; i>n; --i,--j){ // i denota los vértices superiores, j los inferiores
            glVertex3f(vertices[j].x,vertices[j].y,vertices[j].z);
            glVertex3f(vertices[i].x,vertices[i].y,vertices[i].z);
        }
        // Repetimos los 2 primeros vértices para formar la cara que falta para cerrar
        i = vertices.size()-1;
        j = n-1;
        glVertex3f(vertices[j].x,vertices[j].y,vertices[j].z);
        glVertex3f(vertices[i].x,vertices[i].y,vertices[i].z);
    glEnd();
}

float CilindroG::getAltura()
{
    return altura;
}

void CilindroG::setAltura(float a)
{
    if(a > 0){
        altura = a;
        creaGeometria();
    }
}

float CilindroG::getRadio()
{
    return radio;
}

void CilindroG::setRadio(float r)
{
    if(r > 0){
        radio = r;
        creaGeometria();
    }
}

float CilindroG::getNumDiv()
{
    return num_divisiones;
}

void CilindroG::setNumDiv(float nd)
{
    if(nd >= 3){
        num_divisiones = nd;
        creaGeometria();
    }
}

void CilindroG::creaGeometria()
{
    vertices.clear();
    Punto3D p;
    // Centro de la base inferior
    p.x = 0;
    p.y = -(altura/2.0);
    p.z = 0;
    vertices.push_back(p);

    float angulo = (2*PI)/num_divisiones;
    // Puntos de la base inferior
    for(int i=0; i<num_divisiones; ++i){
        p.x = cos(angulo*i)*radio;
        p.z = -sin(angulo*i)*radio;
        vertices.push_back(p);
    }

    int n = vertices.size();

    for(int i=0; i<n; ++i){
        p.x = vertices[i].x;
        p.y = -(vertices[i].y);
        p.z = vertices[i].z;
        vertices.push_back(p);
    }
}
