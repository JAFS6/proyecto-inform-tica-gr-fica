#ifndef PLY_OBJECT_H
#define PLY_OBJECT_H

#include "_object3d.h"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

class PLY_Object : public _Object3D
{
	public:
		/**
			@brief Crea un objeto vacío (será necesario establecer el material posteriormente)
		*/
		PLY_Object();
		
		/**
			@brief Crea el objeto cargando un modelo en base a sus vértices y sus caras (será necesario establecer el material posteriormente)
			@param nvertices El vector con los vértices del modelo a cargar
			@param ncaras El vector con las caras del modelo a cargar
		*/
		PLY_Object(vector<float> nvertices,vector<int> ncaras);
		
		/**
			@brief Crea el objeto cargando un modelo desde fichero PLY (será necesario establecer el material posteriormente)
			@param[in] fich El nombre del fichero
		*/
		PLY_Object(char * fich);
		
		/**
			@brief Carga un modelo en base a sus vértices y sus caras
			@param nvertices El vector con los vértices del modelo a cargar
			@param ncaras El vector con las caras del modelo a cargar
		*/
		void cargarModelo(vector<float> nvertices,vector<int> ncaras);
		
		/**
			@brief Carga un nuevo modelo leído desde fichero PLY
			@param[in] fich El nombre del fichero
		*/
		void cargarModelo(char * fich);
		
	private:
		/**
        	@brief No se usa
        */
        void creaGeometria() {}
        
        /**
        	@brief No se usa
        */
        void creaTopologia() {}
        
        /**
        	@brief Centra el modelo leido en los ejes de coordenadas
        */
        void centrarMalla();
};

#endif // PLY_OBJECT_H

