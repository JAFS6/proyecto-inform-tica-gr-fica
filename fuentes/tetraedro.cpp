#include "tetraedro.h"

Tetraedro::Tetraedro()
{
	reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
    lado = 1;
	creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

Tetraedro::Tetraedro(float l)
{
    reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
	if(l <= 0){
		lado = 1;
	}
	else{
		lado = l;
	}
	creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

float Tetraedro::getLado()
{
    return lado;
}

void Tetraedro::setLado(float l)
{
    if(l > 0){
        lado = l;
        creaGeometria();
        calcularNormalesCaras();
		calcularNormalesVertices();
    }
}

void Tetraedro::creaGeometria()
{
    vertices.clear();
    Punto3D p;

    // V�rtice superior
    p.x = 0;
    p.y = ((sqrt(6)/3.0)*lado)/2;
    p.z = 0;
    vertices.push_back(p);

    // Para los dem�s v�rtices, la altura ser�
    p.y = -p.y;

    // V�rtice alineado con el semieje X positivo
    p.x = (sqrt(pow(lado,2)-(pow(lado/2.0,2))))/2.0;
    p.z = 0;
    vertices.push_back(p);

    float radio = distanciaEuclidea2D(p.x,p.z,0,0);
    p.x =  cos(grados2radianes(120))*radio;
    p.z = -sin(grados2radianes(120))*radio;
    vertices.push_back(p);

    p.x =  cos(grados2radianes(-120))*radio;
    p.z = -sin(grados2radianes(-120))*radio;
    vertices.push_back(p);
}

void Tetraedro::creaTopologia()
{
    Cara c;
    caras.clear();

    c.v1 = 0;

    c.v2 = 1;
    c.v3 = 2;
    caras.push_back(c);

    c.v2 = 2;
    c.v3 = 3;
    caras.push_back(c);

    c.v2 = 3;
    c.v3 = 1;
    caras.push_back(c);

    c.v1 = 1;
    c.v2 = 3;
    c.v3 = 2;
    caras.push_back(c);
}

