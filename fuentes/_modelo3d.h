#ifndef _MODELO3D_H_
#define _MODELO3D_H_

#include <GL/gl.h>
#include "auxiliar.h"

class _Modelo3D
{
	protected:
		float tam_x; // Tamaño de la caja englobante en el eje X
		float tam_y; // Tamaño de la caja englobante en el eje Y
		float tam_z; // Tamaño de la caja englobante en el eje Z
		float radio_esfera_englobante; // Radio de la esfera englobante
		
	public:
		_Modelo3D();
		
		/**
			@brief Dibuja el modelo
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el modelo en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		virtual void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false) = 0;
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje X
			@return El tamaño obtenido
		*/
		float getTamX();
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje Y
			@return El tamaño obtenido
		*/
		float getTamY();
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje Z
			@return El tamaño obtenido
		*/
		float getTamZ();
		
		/**
			@brief Obtiene el radio de la esfera englobante
			@return El radio obtenido
		*/
		float getRadioEsferaEnglobante();
};

#endif // _MODELO3D_H_

