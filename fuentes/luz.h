#ifndef _LUZ_H_
#define _LUZ_H_

#include "auxiliar.h"
#include <cstdlib>
#include <GL/gl.h>
#include <cstdio>

class Luz
{
	private:
		Color reflexion_ambiental; // GL_AMBIENT
		Color reflexion_difusa;    // GL_DIFFUSE
		Color reflexion_especular; // GL_SPECULAR
		GLfloat posicion[4]; // x,y,z,w Si w = 0 La luz es direccional (está en el infinito)
		int nombre;
		
	public:
		/**
			Constructor por defecto, no hace nada, se requiere que se configuren los parámetros posteriormente
		*/
		Luz();
		
		/**
			@brief Enciende la luz
		*/
		void encender();
		
		/**
			@brief Apaga la luz
		*/
		void apagar();
		
		/**
			@brief Configura el nombre de la luz
			@param[in] n El nombre de la luz
		*/
		void setNombre(int n);
		
		/**
			@brief Obtiene el nombre de la luz
			@return El nombre de la luz
		*/
		int getNombre();
		
		/**
			@brief Configura la posición de la luz mediante los parámetros x,y,z,w
			@param[in] x Coordenada x de la posición de la luz
			@param[in] y Coordenada y de la posición de la luz
			@param[in] z Coordenada z de la posición de la luz
			@param[in] w Si vale 0 la luz será una fuente direccional
		*/
		void setPosition(float x,float y,float z,float w);
		
		/**
			@brief Obtiene la posición de la luz mediante los parámetros x,y,z,w
			@param[out] x Coordenada x de la posición de la luz
			@param[out] y Coordenada y de la posición de la luz
			@param[out] z Coordenada z de la posición de la luz
			@param[out] w Si vale 0 la luz será una fuente direccional
		*/
		void getPosition(float &x,float &y,float &z,float &w);
		
		/**
			@brief Configura la reflexión ambiental
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setAmbient  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión difusa
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setDiffuse  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión especular
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setSpecular (float r,float g,float b);
		
		/**
			@brief Obtiene los valores de la reflexión ambiental
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getAmbient  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión difusa
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getDiffuse  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión especular
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getSpecular (float &r,float &g,float &b);
};

#endif // _LUZ_H_


