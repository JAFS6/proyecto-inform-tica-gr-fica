#include "capota.h"

Capota::Capota()
{
	ancho = 5;
	largo = 6;
	alto = 3;
	capota.setAmbient(0.0,0.0,0.0);
	capota.setDiffuse(0.1,0.35,0.1);
	capota.setSpecular(0.45,0.55,0.45);
	capota.setShininess(32);
}

Capota::Capota(float anch,float larg,float alt)
{
	ancho = anch;
	largo = larg;
	alto = alt;
	capota.setAmbient(0.0,0.0,0.0);
	capota.setDiffuse(0.1,0.35,0.1);
	capota.setSpecular(0.45,0.55,0.45);
	capota.setShininess(32);
}

void Capota::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	glPushMatrix();
		glScalef(ancho,alto,largo);
		capota.dibuja(iluminacion,ajedrez);
	glPopMatrix();
}

float Capota::getAncho()
{
	return ancho;
}

void Capota::setAncho(float a)
{
	if(a > 0)
		ancho = a;
}

float Capota::getLargo()
{
	return largo;
}

void Capota::setLargo(float l)
{
	if(l > 0)
		largo = l;
}

float Capota::getAlto()
{
	return alto;
}

void Capota::setAlto(float a)
{
	if(a > 0)
		alto = a;
}

void Capota::setAmbient (float r,float g,float b)
{
	capota.setAmbient(r,g,b);
}

void Capota::setDiffuse (float r,float g,float b)
{
	capota.setDiffuse(r,g,b);
}

void Capota::setSpecular(float r,float g,float b)
{
	capota.setSpecular(r,g,b);
}

void Capota::setShininess(float s)
{
	capota.setShininess(s);
}

void Capota::getAmbient (float &r,float &g,float &b)
{
	capota.getAmbient(r,g,b);
}

void Capota::getDiffuse (float &r,float &g,float &b)
{
	capota.getDiffuse(r,g,b);
}

void Capota::getSpecular(float &r,float &g,float &b)
{
	capota.getSpecular(r,g,b);
}

void Capota::getShininess(float &s)
{
	capota.getShininess(s);
}


