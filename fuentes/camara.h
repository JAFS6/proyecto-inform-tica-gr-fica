#ifndef _CAMARA_H_
#define _CAMARA_H_

#include "auxiliar.h"
#include <GL/gl.h>
#include <cmath>

class Camara
{
	private:
		Punto3D vrp; // View Reference Point
		Punto3D vpn; // View Plane Normal
		Punto3D vup; // View Up
		Punto3D u;   // X en SC de vista
		Punto3D v;   // Y en SC de vista
		Punto3D n;   // Z en SC de vista
		float alpha; // Ángulo de rotación respecto del eje X
		float beta;  // Ángulo de rotación respecto del eje Y
		float gamma; // Ángulo de rotación respecto del eje Z
		float vel_mov; // Velocidad de movimiento
		
	public:
		/**
			Constructor por defecto, coloca la cámara igual que la de openGL
		*/
		Camara();
		
		/**
			@brief Establece los parámetros de configuraciónde la cámara
			@param[in] nuevo_vrp El nuevo VRP de la cámara
			@param[in] nuevo_vpn El nuevo VPN de la cámara
			@param[in] nuevo_vup El nuevo VUP de la cámara
		*/
		void setCamara(Punto3D nuevo_vrp,Punto3D nuevo_vpn,Punto3D nuevo_vup);
		
		/**
			@brief Establece los parámetros de configuraciónde la cámara
			@param[in] nuevo_vrp_x Coordenada x del VRP
			@param[in] nuevo_vrp_y Coordenada y del VRP
			@param[in] nuevo_vrp_z Coordenada z del VRP
			@param[in] nuevo_vpn_x Coordenada x del VPN
			@param[in] nuevo_vpn_y Coordenada y del VPN
			@param[in] nuevo_vpn_z Coordenada z del VPN
			@param[in] nuevo_vup_x Coordenada x del VUP
			@param[in] nuevo_vup_y Coordenada y del VUP
			@param[in] nuevo_vup_z Coordenada z del VUP
		*/
		void setCamara(float nuevo_vrp_x,float nuevo_vrp_y,float nuevo_vrp_z,
					   float nuevo_vpn_x,float nuevo_vpn_y,float nuevo_vpn_z,
					   float nuevo_vup_x,float nuevo_vup_y,float nuevo_vup_z);
		
		/**
			@brief Aplica la transformación de vista
		*/
		void situarCamara();
		
		/**
			@brief Consulta el VRP
			@return El VRP
		*/
		Punto3D getVRP();
		
		/**
			@brief Consulta el VPN
			@return El VPN
		*/
		Punto3D getVPN();
		
		/**
			@brief Consulta el VUP
			@return El VUP
		*/
		Punto3D getVUP();
		
		/**
			@brief Mueve la cámara en la dirección que mira una cantidad c (positivo para avanzar,negativo para retroceder)
			@param[in] c Cantidad a mover
		*/
		void desplazamientoFrontal(float c);
		
		/**
			@brief Mueve la cámara lateralmente una cantidad c (positivo a la derecha,negativo a la izquierda)
			@param[in] c Cantidad a mover
		*/
		void desplazamientoLateral(float c);
		
		/**
			@brief Mueve la cámara verticalmente una cantidad c (positivo hacia arriba,negativo hacia abajo)
			@param[in] c Cantidad a mover
		*/
		void desplazamientoVertical(float c);
		
		/**
			@brief Gira la cámara verticalmente un ángulo ang (en grados) (positivo hacia arriba,negativo hacia abajo)
			@param[in] ang El ángulo a girar
		*/
		void giroVertical(float ang);
		
		/**
			@brief Gira la cámara horizontalmente un ángulo ang (en grados) (positivo a la izquierda,negativo a la derecha)
			@param[in] ang El ángulo a girar
		*/
		void giroHorizontal(float ang);
		
		/**
			@brief Rota la cámara un ángulo ang (en grados) (positivo en sentido horario, negativo en sentido antihorario)
			@param[in] ang El ángulo a girar
		*/
		void rotacion(float ang);
	
	private:
		/**
			@brief Crea el Sistema de Coordenadas de Vista
		*/
		void inicializa();
		
		/**
			@brief Calcula la transformación de vista
		*/
		void calcularTransformacion();
};

#endif // _CAMARA_H_

