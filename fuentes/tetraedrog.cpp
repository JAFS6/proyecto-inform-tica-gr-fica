#include "tetraedrog.h"

TetraedroG::TetraedroG()
{
    lado = 1;
	creaGeometria();
}

void TetraedroG::dibuja()
{
    int indice[6] = {1,0,3,2,1,0};
    glColor3f(color.r,color.g,color.b);
    glBegin(GL_TRIANGLE_STRIP);
        for(int i=0; i<6; ++i){
            glVertex3f(vertices[(indice[i])].x,vertices[(indice[i])].y,vertices[(indice[i])].z);
        }
    glEnd();
}

float TetraedroG::getLado()
{
    return lado;
}

void TetraedroG::setLado(float l)
{
    if(l > 0){
        lado = l;
        creaGeometria();
    }
}

void TetraedroG::creaGeometria()
{
    vertices.clear();
    Punto3D p;

    // V�rtice superior
    p.x = 0;
    p.y = ((sqrt(6)/3.0)*lado)/2;
    p.z = 0;
    vertices.push_back(p);

    // Para los dem�s v�rtices, la altura ser�
    p.y = -p.y;

    // V�rtice alineado con el semieje X positivo
    p.x = (sqrt(pow(lado,2)-(pow(lado/2.0,2))))/2.0;
    p.z = 0;
    vertices.push_back(p);

    float radio = distanciaEuclidea2D(p.x,p.z,0,0);
    p.x =  cos(grados2radianes(120))*radio;
    p.z = -sin(grados2radianes(120))*radio;
    vertices.push_back(p);

    p.x =  cos(grados2radianes(-120))*radio;
    p.z = -sin(grados2radianes(-120))*radio;
    vertices.push_back(p);
}

