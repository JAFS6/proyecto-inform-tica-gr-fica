#include "_o3d_modelo_animado.h"

_O3D_Modelo_Animado::_O3D_Modelo_Animado()
{
	objeto1 = NULL;
	objeto2 = NULL;
}

_O3D_Modelo_Animado::_O3D_Modelo_Animado(_Modelo3D * o)
{
	objeto1 = o;
}

void _O3D_Modelo_Animado::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	float escalar = this->getEscalado();
	
	if(objeto1 != NULL && objeto2 != NULL){
		glPushMatrix();
			glTranslatef(posicion_actual.x,posicion_actual.y,posicion_actual.z);
			glRotatef(rotacion,orientacion_actual.x,orientacion_actual.y,orientacion_actual.z);
			glRotatef(orientacion(),0,1,0);
			glScalef(escalar,escalar,escalar);
			if(!tiene_lana && contador_lana < t_crecer_lana*0.5 && adulto)
				objeto2->dibuja(iluminacion,ajedrez);
			else
				objeto1->dibuja(iluminacion,ajedrez);
		glPopMatrix();
	}
	else{
		std::cerr << "No hay objetos para dibujar" << std::endl;
	}
}

void _O3D_Modelo_Animado::setObjeto(_Modelo3D * o)
{
	objeto1 = o;
}

void _O3D_Modelo_Animado::setObjeto2(_Modelo3D * o)
{
	objeto2 = o;
}

float _O3D_Modelo_Animado::getRadioEsferaEnglobante()
{
	return (this->getEscalado() * objeto1->getRadioEsferaEnglobante());
}

float _O3D_Modelo_Animado::getTamX()
{
	if(objeto1 != NULL){
		return objeto1->getTamX();
	}
	else{
		std::cerr << "No hay objeto" << std::endl;
		return 0;
	}
}

float _O3D_Modelo_Animado::getTamY()
{
	if(objeto1 != NULL){
		return objeto1->getTamY();
	}
	else{
		std::cerr << "No hay objeto" << std::endl;
		return 0;
	}
}

float _O3D_Modelo_Animado::getTamZ()
{
	if(objeto1 != NULL){
		return objeto1->getTamZ();
	}
	else{
		std::cerr << "No hay objeto" << std::endl;
		return 0;
	}
}

void _O3D_Modelo_Animado::hacerAdulto()
{
	posicion_actual.y = (objeto1->getTamY()/2.0)*getEscalado();
	posicion_inicial.y = posicion_actual.y;
}

