#include "escena.h"

Escena::Escena()
{
	listas_creadas = false;
	ids = 0;
	// VARIABLES DE CONTROL
	
	mostrar_ejes = false;
	modo = solido;
	iluminacion = gouraud;
	luces_estaticas = true;
	camara_seleccionada = 0;
	estado_animacion = detenida;
	t_vel = lineal;
	texturas_visibles = true;
	
	// Inicializo los anteriores a un valor distinto para que al arrancar el programa se muestre la configuración
	anterior_mostrar_ejes = true;
	anterior_modo = puntos;
	anterior_iluminacion = plano;
	anterior_luces_estaticas = false;
	anterior_camara_seleccionada = 1;
	anterior_estado_animacion = activada;
	anterior_t_vel = senoidal;
	anterior_texturas_visibles = false;
	
	// OBJETOS
	
	tablero.setLado(0.8);
	tablero.setGrosor(0.02);
	tablero.setDimension(8,8);
	
	char ruta[1024];
	
	strcpy(ruta,"./modelos/trashcan.ply");
	cubo_basura.cargarModelo(ruta);
	
	strcpy(ruta,"./modelos/balance.ply");
	balancin.cargarModelo(ruta);
	
	balancin.setAmbient(0.2,0.2,0.2);
	balancin.setDiffuse(0.316,0.234,0.023);
	balancin.setSpecular(0,0,0);
	balancin.setShininess(0);
	
	cubo_basura.setAmbient(0.1,0.1,0.1);
	cubo_basura.setDiffuse(0.1,0.1,0.1);
	cubo_basura.setSpecular(0.5,0.5,0.5);
	cubo_basura.setShininess(5);
	
	_random aleatoriox;
	_random aleatorioz;
	aleatoriox.init(E1X_PASTO,E2X_PASTO);
	aleatoriox.seed();
	aleatorioz.init(E1Y_PASTO,E2Y_PASTO);
	aleatorioz.seed();
	Punto3D pos_aleatoria;
	bool continuar;
	
	oveja.clear();
	grupo.clear();
	
	_O3D_Modelo_Animado * oveja_aux;
	
	for(int i=0; i<NUM_MIN_OVEJAS; ++i){
		oveja_aux = 0;
		oveja_aux = new _O3D_Modelo_Animado();
		
		if(oveja_aux == 0){
			cerr << "Error en la reserva de _O3D_Modelo_Animado " << i << endl;
		}
		
		oveja_aux->setID(ids++);
		oveja_aux->setObjeto(&oveja_modelo1);
		oveja_aux->setObjeto2(&oveja_modelo2);
		oveja_aux->setTipoVel(t_vel);
		oveja_aux->setEscalado(ESCALADO_OVEJA_ADULTA);
		
		do{
			continuar = false;
			pos_aleatoria.x = aleatoriox.value();
			pos_aleatoria.z = aleatorioz.value();
			
			for(int k=0; !continuar && k<i; ++k){
				if(distanciaEuclidea2D(pos_aleatoria.x,pos_aleatoria.z, oveja[k]->posX(), oveja[k]->posZ()) 
					< oveja_aux->getRadioEsferaEnglobante() + oveja[k]->getRadioEsferaEnglobante()){
					continuar = true;
				}
			}
		}while(continuar);
		
		oveja_aux->setPos(pos_aleatoria.x,(oveja_modelo1.getTamY()/2.0)*oveja_aux->getEscalado(),pos_aleatoria.z);
		oveja_aux->setOrientacionModelo(0,0,1);
		oveja_aux->setOrientacion(aleatoriox.value(),0,aleatorioz.value());
		oveja_aux->setAdulto();
		
		oveja.push_back(oveja_aux);
		grupo.push_back(oveja_aux);
	}
	
	cout << "\nNumero de ovejas: " << oveja.size() << endl;
	
	// LUCES
	
	for(int i=0; i<NUM_LUCES; ++i){
		luces[i].setNombre(i+1);
		luces[i].setAmbient (0,0,0);
		luces[i].setDiffuse (1,1,1);
		luces[i].setSpecular(1,1,1);
	}
	luces[0].setPosition(0,1,0,0);
	luces[1].setPosition(0,1,1,0);
	luces[2].setPosition(1,1,0,0);
	luces[3].setPosition(0,1,-1,0);
	luces[4].setPosition(-1,1,0,0);
	interruptor[0] = true;
	interruptor[1] = true;
	interruptor[2] = true;
	interruptor[3] = true;
	interruptor[4] = true;
	
	texturas_cargadas = false;
	
	// CÁMARAS
	camara_sobre_oveja = 0;
	for(int i=0; i<NUM_CAMARAS; ++i)
		reiniciarCamara(i);
}

void Escena::liberarRecursos()
{
	int tam = oveja.size();
	
	for(int i=0; i<tam; ++i)
		delete oveja[i];
	oveja.clear();
	grupo.clear();
}

void Escena::draw()
{
	// CARGA DE TEXTURAS Y CREACION DE DISPLAYLISTS
	if(!texturas_cargadas){
		cesped.setImagen("./texturas/GrassTexture.png");
		
		montana.setImagen("./texturas/fondo_valle.png");
		
		paja.setImagen("./texturas/straw_texture.png");
		
		arbol.setImagen("./texturas/tree1.png");
		
		// POSICIÓN ÁRBOLES
	
		float aux_y_arbol = (arbol.getAlto()/2.0)*0.25;
	
		translaciones_arboles[0].x = 150;
		translaciones_arboles[0].y = aux_y_arbol;
		translaciones_arboles[0].z = 150;
	
		translaciones_arboles[1].x = -150;
		translaciones_arboles[1].y = aux_y_arbol;
		translaciones_arboles[1].z = 150;
	
		translaciones_arboles[2].x = 150;
		translaciones_arboles[2].y = aux_y_arbol;
		translaciones_arboles[2].z = -150;
	
		translaciones_arboles[3].x = -150;
		translaciones_arboles[3].y = aux_y_arbol;
		translaciones_arboles[3].z = -150;

		translaciones_arboles[4].x = 150;
		translaciones_arboles[4].y = aux_y_arbol;
		translaciones_arboles[4].z = 0;
	
		translaciones_arboles[5].x = 0;
		translaciones_arboles[5].y = aux_y_arbol;
		translaciones_arboles[5].z = 150;
	
		translaciones_arboles[6].x = -150;
		translaciones_arboles[6].y = aux_y_arbol;
		translaciones_arboles[6].z = 0;
	
		translaciones_arboles[7].x = 0;
		translaciones_arboles[7].y = aux_y_arbol;
		translaciones_arboles[7].z = -150;
		
		texturas_cargadas = true;
	}
	
	glEnable(GL_CULL_FACE);
	
	if(camara_seleccionada == INDICE_CAMARA_OVEJAS){ // Cámara de las ovejas
		reiniciarCamara(INDICE_CAMARA_OVEJAS);
	}
	
	camara[camara_seleccionada].situarCamara();
	
	glDisable(GL_LIGHTING);
	
	if(mostrar_ejes){
		ejes.draw();
	}
	
	if(iluminacion != ninguno && (modo == ajedrez || modo == solido) && !(modo == puntos || modo == lineas)){
		glEnable(GL_LIGHTING);
		GLfloat Componente_ambiente[4]={1,1,1,1};
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT,Componente_ambiente);
		glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE);
		
		if(luces_estaticas){
			for(int l=0; l<NUM_LUCES; ++l){
				if(interruptor[l])
					luces[l].encender();
				else
					luces[l].apagar();
			}
		}
		else{
			glPushMatrix();
				glLoadIdentity();
				for(int l=0; l<NUM_LUCES; ++l){
					if(interruptor[l])
						luces[l].encender();
					else
						luces[l].apagar();
				}
			glPopMatrix();
		}
		
		if(iluminacion == plano){
			glShadeModel(GL_FLAT);
		}
		else if(iluminacion == gouraud){
			glShadeModel(GL_SMOOTH);
		}
	}

	// Ponemos el grosor de los puntos
	glPointSize(4);

	if(modo == puntos){
		glDisable(GL_CULL_FACE);
		glPolygonMode(GL_FRONT_AND_BACK,GL_POINT);
	}
	else if(modo == lineas){
		glDisable(GL_CULL_FACE);
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	}
	else
		glPolygonMode(GL_FRONT,GL_FILL);
	
	bool ajedrez_activo = false;
	if(modo == ajedrez)
		ajedrez_activo = true;
	
	
	if(!listas_creadas || modo != anterior_modo || iluminacion != anterior_iluminacion){
		if(!listas_creadas)
			listas_creadas = true;
		else
			glDeleteLists(lista_estatico, 1);
		lista_estatico = CreaListaEstatico(iluminacion,ajedrez_activo);
	}
	
	// Muestro el estado del programa
	mostrarOpciones();
	
	glCallList(lista_estatico);
	
	int tam = oveja.size();
	
	for(int i=0; i<tam; ++i){
		oveja[i]->dibuja(iluminacion,ajedrez_activo);
	}

	glColor3f(1,1,1);
	
	pintarCesped(iluminacion,ajedrez_activo);
	
	pintarFondo(iluminacion,ajedrez_activo);
	
	pintarArboles(iluminacion,ajedrez_activo);
	
	// Restauramos el grosor de los puntos
	glPointSize(1);
}

void Escena::actualizar_escena()
{
	if(estado_animacion == activada){
		
		// Actualizar los objetos de la escena
		
		int tam = oveja.size();
		
		// Actualizar estados y posiciones
		
		for(int i=0; i<tam; ++i){
			oveja[i]->actualizar(grupo);
		}
		
		// Nacimientos
		
		for(int i=0; i<tam; ++i){
			nuevaCria(oveja[i]);
		}
		
		// Muertes
		for(int i=0; i<(int)oveja.size();){
			if(!oveja[i]->estaVivo()){
				cout << "\nLa oveja " << oveja[i]->getID() << " ha muerto :(" << endl;
				oveja.erase(oveja.begin()+i);
				grupo.erase(grupo.begin()+i);
				cout << "Numero de ovejas: " << oveja.size() << endl;
			}
			else{
				++i;
			}
		}
	}
}

void Escena::nuevaCria(_O3D_Modelo_Animado * madre)
{
	_O3D_Modelo_Animado * cria = NULL;
	
	if(madre->esHembra() && madre->vaAParir() && madre->dentroPasto()){
		_random aleatoriox;
		_random aleatorioz;
		aleatoriox.init(madre->posX()-4,madre->posX()+4);
		aleatoriox.seed();
		aleatorioz.init(madre->posZ()-4,madre->posZ()+4);
		aleatorioz.seed();
		
		cria = 0;
		cria = new _O3D_Modelo_Animado();
		
		if(cria == 0){
			cerr << "Error en la reserva de cria" << endl;
		}
		
		cria->setID(ids++);
		cria->setObjeto(&oveja_modelo1);
		cria->setObjeto2(&oveja_modelo2);
		cria->setTipoVel(t_vel);
		cria->setEscalado(ESCALADO_OVEJA_ADULTA);
	
		int num_ovejas = oveja.size();
		bool probar_otra;
		Punto3D pos_aleatoria;
		int num_intentos = 0;
		
		do{
			probar_otra = false;
			pos_aleatoria.x = aleatoriox.value();
			pos_aleatoria.z = aleatorioz.value();
			
			if(pos_aleatoria.x >= E1X_PASTO && pos_aleatoria.x <= E2X_PASTO && pos_aleatoria.z >= E1Y_PASTO && pos_aleatoria.z <= E2Y_PASTO){ //Condicion de nacer dentro del pasto
				for(int k=0; !probar_otra && k<num_ovejas; ++k){
					if(distanciaEuclidea2D(pos_aleatoria.x,pos_aleatoria.z, oveja[k]->posX(), oveja[k]->posZ())
							 < cria->getRadioEsferaEnglobante() + oveja[k]->getRadioEsferaEnglobante()){ //Condicion de colision
						probar_otra = true;
					}
				}
			}
			else
				probar_otra = true;
			++num_intentos;
		}while(probar_otra && num_intentos<50);
		
		if(!probar_otra){
			cria->setPos(pos_aleatoria.x,(oveja_modelo1.getTamY()/2.0)*cria->getEscalado(),pos_aleatoria.z);
			cria->setOrientacionModelo(0,0,1);
			cria->setOrientacion(aleatoriox.value(),0,aleatorioz.value());
			oveja.push_back(cria);
			grupo.push_back(cria);
			cria->iniciar();
			madre->setHaParido();
			
			cout << "\n¡¡Ha nacido una oveja!! :)\nNumero de ovejas: " << oveja.size() << endl;
		}
		else{
			delete cria;
			--ids;
			cout << "\nAborto" << endl;
		}
	}
}

void Escena::iniciarAnimacion()
{
	// Nota: No se pone en común estado_animacion = activada; para que no se ejecute si se llama al método cuando no se debe
	int tam = oveja.size();
	
	if(estado_animacion == detenida){
		estado_animacion = activada;
		for(int i=0; i<tam; ++i)
			oveja[i]->iniciar();
	}
	else if(estado_animacion == pausada){
		estado_animacion = activada;
		for(int i=0; i<tam; ++i)
			oveja[i]->reanudar();
	}
}

void Escena::pausarAnimacion()
{
	if(estado_animacion == activada){
	
		int tam = oveja.size();
		estado_animacion = pausada;
		
		for(int i=0; i<tam; ++i)
			oveja[i]->pausar();
	}
}

void Escena::detenerAnimacion()
{
	if(estado_animacion == activada || estado_animacion == pausada){
		int tam = oveja.size();
		estado_animacion = detenida;
		
		for(int i=0; i<tam; ++i)
			oveja[i]->detener();
	}
}

void Escena::altEjes()
{
    if(mostrar_ejes){
        mostrar_ejes = false;
    }
    else{
        mostrar_ejes = true;
    }
}

void Escena::altLucesEstaticas()
{
	if(luces_estaticas){
        luces_estaticas = false;
    }
    else{
        luces_estaticas = true;
    }
}

void Escena::setModo(modo_vis m)
{
    modo = m;
}

void Escena::setIluminacion(modo_ilu m)
{
	iluminacion = m;
}

void Escena::pulsarInterruptor(int n)
{
	if(interruptor[n]){
        interruptor[n] = false;
    }
    else{
        interruptor[n] = true;
    }
}

void Escena::encenderTodas()
{
	for(int i=0; i<NUM_LUCES; ++i)
		interruptor[i] = true;
}

void Escena::apagarTodas()
{
	for(int i=0; i<NUM_LUCES; ++i)
		interruptor[i] = false;
}

void Escena::setCamara(int n)
{
	if(n >= 0 && n < NUM_CAMARAS){
		if(n != INDICE_CAMARA_OVEJAS || (n == INDICE_CAMARA_OVEJAS && oveja.size() > 0))
			camara_seleccionada = n;
		
		if(camara_seleccionada == INDICE_CAMARA_OVEJAS)
			if(camara_sobre_oveja >= (int)oveja.size())
				camara_sobre_oveja = 0;
	}
}

void Escena::colocarCamara()
{
	Punto3D vrp,vpn,vup;
	
	cout << "\nIntroduzca el VRP: " << endl;
	cin >> vrp.x >> vrp.y >> vrp.z;
	cout << "\nIntroduzca el VPN: " << endl;
	cin >> vpn.x >> vpn.y >> vpn.z;
	cout << "\nIntroduzca el VUP: " << endl;
	cin >> vup.x >> vup.y >> vup.z;
	
	while(abs(productoEscalar(vpn,vup) - (moduloVector(vpn)*moduloVector(vup))) < 0.0001){
		cout << "\n¡¡¡ Los vectores VPN y VUP no pueden ser paralelos !!!" << endl;
		cout << "\nIntroduzca el VPN: " << endl;
		cin >> vpn.x >> vpn.y >> vpn.z;
		cout << "\nIntroduzca el VUP: " << endl;
		cin >> vup.x >> vup.y >> vup.z;
	}
	
	camara[camara_seleccionada].setCamara(vrp,vpn,vup);
	cout << "\nCamara colocada" << endl;
}

void Escena::desplazamientoFrontal(float c)
{
	camara[camara_seleccionada].desplazamientoFrontal(c);
}

void Escena::desplazamientoLateral(float c)
{
	camara[camara_seleccionada].desplazamientoLateral(c);
}

void Escena::desplazamientoVertical(float c)
{
	camara[camara_seleccionada].desplazamientoVertical(c);
}

void Escena::giroVertical(float ang)
{
	camara[camara_seleccionada].giroVertical(ang);
}

void Escena::giroHorizontal(float ang)
{
	camara[camara_seleccionada].giroHorizontal(ang);
}

void Escena::rotacion(float ang)
{
	camara[camara_seleccionada].rotacion(ang);
}

void Escena::reiniciarCamara(int i)
{
	if(i == 0)
		camara[0].setCamara(150,120,150,   1, 0.7, 1,   0, 1, 0);
	else if(i == 1)
		camara[1].setCamara(-150, 50,  0,  -1, 0.5, 0,   0, 1, 0);
	else if(i == 2)
		camara[2].setCamara(-75, 5,-120,   -0.25, 0, -1,   0, 1, 0);
	else if(i == 3)
		camara[3].setCamara(-40, 15,-10,   2, 1, 0,   0, 1, 0); // Cámara del comedero
	else if(i == 4)
		camara[4].setCamara(-75, 5,120,   -0.25, 0, 1,   0, 1, 0); // Cámara de las casetas
	else if(i == 5)
		camara[5].setCamara(-22, 15,-80,   0,0.25,1,  0, 1, 0); // Cámara de las primitivas
	else if(i == 6)
		camara[6].setCamara(40, 15,-95,   0,0.4,1,  0, 1, 0); // Cámara del tablero y el balancín
	else if(i == INDICE_CAMARA_OVEJAS){
		Punto3D orientacion_aux;
	
		oveja[camara_sobre_oveja]->getOrientacion(orientacion_aux.x,orientacion_aux.y,orientacion_aux.z);
	
		camara[INDICE_CAMARA_OVEJAS].setCamara(oveja[camara_sobre_oveja]->posX(),
							oveja[camara_sobre_oveja]->posY()+oveja_modelo1.getTamY()*oveja[camara_sobre_oveja]->getEscalado(),
							oveja[camara_sobre_oveja]->posZ(),  
							-orientacion_aux.x,-orientacion_aux.y,-orientacion_aux.z,   0, 1, 0); // Cámara de las ovejas
		
	}
}

void Escena::camaraOvejaSiguiente()
{
	if(camara_seleccionada == INDICE_CAMARA_OVEJAS){
		if(camara_sobre_oveja == (int)oveja.size()-1)
			camara_sobre_oveja = 0;
		else
			++camara_sobre_oveja;
	}
}
		
void Escena::camaraOvejaAnterior()
{
	if(camara_seleccionada == INDICE_CAMARA_OVEJAS){
		if(camara_sobre_oveja == 0)
			camara_sobre_oveja = (int)oveja.size()-1;
		else
			--camara_sobre_oveja;
	}
}

int Escena::camaraSeleccionada()
{
	return camara_seleccionada;
}

void Escena::incrementarVelocidad()
{
	int tam = oveja.size();
	
	for(int i=0; i<tam; ++i)
		oveja[i]->setVel(oveja[i]->getVel()+0.05);
}

void Escena::decrementarVelocidad()
{
	int tam = oveja.size();
	
	for(int i=0; i<tam; ++i)
		oveja[i]->setVel(oveja[i]->getVel()-0.05);
}

void Escena::incrementarVelocidadAngular()
{
	int tam = oveja.size();
	
	for(int i=0; i<tam; ++i)
		oveja[i]->setVelAng(oveja[i]->getVelAng()+0.005);
}
		
void Escena::decrementarVelocidadAngular()
{
	int tam = oveja.size();
	
	for(int i=0; i<tam; ++i)
		oveja[i]->setVelAng(oveja[i]->getVelAng()-0.005);
}

void Escena::setTipoVelocidad(tipo_velocidad t)
{
	int tam = oveja.size();
	
	t_vel = t;
	for(int i=0; i<tam; ++i)
		oveja[i]->setTipoVel(t_vel);
}

void Escena::altTexturas()
{
	if(texturas_visibles){
        texturas_visibles = false;
    }
    else{
        texturas_visibles = true;
    }
}

void Escena::anadirOveja()
{
	_random aleatoriox;
	_random aleatorioz;
	aleatoriox.init(E1X_PASTO,E2X_PASTO);
	aleatoriox.seed();
	aleatorioz.init(E1Y_PASTO,E2Y_PASTO);
	aleatorioz.seed();
	Punto3D pos_aleatoria;
	bool continuar;
	int i = oveja.size();
	
	_O3D_Modelo_Animado * oveja_aux;
	
	oveja_aux = 0;
	oveja_aux = new _O3D_Modelo_Animado();
	
	if(oveja_aux == 0){
		cerr << "Error en la reserva de _O3D_Modelo_Animado " << i << endl;
	}
	
	oveja_aux->setID(ids++);
	oveja_aux->setObjeto(&oveja_modelo1);
	oveja_aux->setObjeto2(&oveja_modelo2);
	oveja_aux->setTipoVel(t_vel);
	oveja_aux->setEscalado(ESCALADO_OVEJA_ADULTA);
	
	int num_intentos = 0;
	
	do{
		continuar = false;
		pos_aleatoria.x = aleatoriox.value();
		pos_aleatoria.z = aleatorioz.value();
		
		for(int k=0; !continuar && k<i; ++k){
			if(distanciaEuclidea2D(pos_aleatoria.x,pos_aleatoria.z, oveja[k]->posX(), oveja[k]->posZ()) 
				< oveja_aux->getRadioEsferaEnglobante() + oveja[k]->getRadioEsferaEnglobante()){
				continuar = true;
			}
		}
		++num_intentos;
	}while(continuar && num_intentos<50);
	
	if(!continuar){
		oveja_aux->setPos(pos_aleatoria.x,(oveja_modelo1.getTamY()/2.0)*oveja_aux->getEscalado(),pos_aleatoria.z);
		oveja_aux->setOrientacionModelo(0,0,1);
		oveja_aux->setOrientacion(aleatoriox.value(),0,aleatorioz.value());
	
		oveja.push_back(oveja_aux);
		grupo.push_back(oveja_aux);
		if(estado_animacion == activada){
			oveja_aux->iniciar();
		}
		cout << "\nOveja añadida, número de ovejas: " << oveja.size() << endl;
	}
	else{
		delete oveja_aux;
		--ids;
		cout << "\nNo se pudo añadir la oveja" << endl;
	}
}

void Escena::quitarOveja()
{
	int tam = oveja.size();
	int pos = -1;
	int mayor_vida = 0;
	int aux;
	
	for(int i=0; i<tam; ++i){ // Buscar la oveja más vieja
		aux = oveja[i]->edad();
		if(aux > mayor_vida && aux < oveja[i]->getEsperanzaVida()){
			mayor_vida = aux;
			pos = i;
		}
	}
	
	if(pos != -1){ // Quitar oveja
		/*oveja.erase(oveja.begin()+pos);
		grupo.erase(grupo.begin()+pos);
		cout << "\nOveja eliminada, número de ovejas: " << oveja.size() << endl;*/
		oveja[pos]->setEdad(oveja[pos]->getEsperanzaVida()+1);
	}
}

void Escena::mostrarNumeroOvejas()
{
	cout << "\nNumero de ovejas: " << oveja.size() << endl;
}

void Escena::mostrarOpciones()
{
	if(mostrar_ejes != anterior_mostrar_ejes){
		cout << endl;
		anterior_mostrar_ejes = mostrar_ejes;
		cout << "Mostrando ejes: ";
		if(anterior_mostrar_ejes) cout << "Si";
		else cout << "No";
		cout << endl;
	}
	
	if(modo != anterior_modo){
		cout << endl;
		anterior_modo = modo;
		cout << "Modo: ";
			 if(modo == puntos) cout << "puntos";
		else if(modo == lineas) cout << "lineas";
		else if(modo == ajedrez) cout << "ajedrez";
		else if(modo == solido) cout << "solido";
		cout << endl;
	}
	
	if(iluminacion != anterior_iluminacion){
		cout << endl;
		anterior_iluminacion = iluminacion;
		cout << "Modo iluminación: ";
			 if(iluminacion == ninguno) cout << "ninguno";
		else if(iluminacion == plano) cout << "plano";
		else if(iluminacion == gouraud) cout << "gouraud";
		cout << endl;
	}
	
	if(luces_estaticas != anterior_luces_estaticas){
		cout << endl;
		anterior_luces_estaticas = luces_estaticas;
		cout << "Luces estáticas: ";
		if(luces_estaticas) cout << "Si";
		else cout << "No";
		cout << endl;
	}
	
	if(camara_seleccionada != anterior_camara_seleccionada){
		cout << endl;
		anterior_camara_seleccionada = camara_seleccionada;
		cout << "Camara seleccionada: " << camara_seleccionada+1 << endl;
	}
	
	if(estado_animacion != anterior_estado_animacion){
		cout << endl;
		anterior_estado_animacion = estado_animacion;
		cout << "Estado animacion: ";
		if(estado_animacion == activada) cout << "activada";
		else if(estado_animacion == detenida) cout << "detenida";
		else if(estado_animacion == pausada) cout << "pausada";
		cout << endl;
	}
	
	if(t_vel != anterior_t_vel){
		cout << endl;
		anterior_t_vel = t_vel;
		cout << "Tipo de velocidad: ";
		if(t_vel == lineal) cout << "lineal";
		else if(t_vel == senoidal) cout << "senoidal";
		cout << endl;
	}
	
	if(texturas_visibles != anterior_texturas_visibles){
		cout << endl;
		anterior_texturas_visibles = texturas_visibles;
		cout << "Texturas visibles: ";
		if(texturas_visibles) cout << "Si";
		else cout << "No";
		cout << endl;
	}
}

void Escena::pintarCesped(modo_ilu iluminacion,bool ajedrez)
{
	float parametro_escalado = 1.0/16.0;
	int tam_campo = 25;
	
	glPushMatrix();
	
		glTranslatef(-(tam_campo/2.0)*cesped.getAncho()*parametro_escalado,0.0,-(tam_campo/2.0)*cesped.getAlto()*parametro_escalado);
	
		for(int i=0; i<tam_campo; ++i){
			for(int j=0; j<tam_campo; ++j){
				glPushMatrix();
					glTranslatef(cesped.getAncho()*parametro_escalado*i,0.0,cesped.getAlto()*parametro_escalado*j);
					glTranslatef((cesped.getAncho()/2.0)*parametro_escalado,0.0,(cesped.getAlto()/2.0)*parametro_escalado);
					glRotatef(-90,1,0,0);
					glScalef(parametro_escalado,parametro_escalado,1.0);
					cesped.dibuja(iluminacion,false,ajedrez,texturas_visibles);
				glPopMatrix();
			}
		}
	
	glPopMatrix();
}

void Escena::pintarValla(modo_ilu iluminacion,bool ajedrez)
{	
	cubo.setAmbient(0.1,0.1,0.1);
	cubo.setDiffuse(0.60,0.40,0.12);
	cubo.setSpecular(0.5,0.5,0.5);
	cubo.setShininess(32);
	
	glPushMatrix();
		glTranslatef(0,(cubo.getTamY()*1.5)/2.0,-100);
		glScalef(200,1.5,0.1);
		cubo.dibuja(iluminacion,ajedrez);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(0,(cubo.getTamY()*1.5)/2.0,100);
		glScalef(200,1.5,0.1);
		cubo.dibuja(iluminacion,ajedrez);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-100,(cubo.getTamY()*1.5)/2.0,0);
		glRotatef(90,0,1,0);
		glScalef(200,1.5,0.1);
		cubo.dibuja(iluminacion,ajedrez);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(100,(cubo.getTamY()*1.5)/2.0,0);
		glRotatef(90,0,1,0);
		glScalef(200,1.5,0.1);
		cubo.dibuja(iluminacion,ajedrez);
	glPopMatrix();
}

void Escena::pintarFondo(modo_ilu iluminacion,bool ajedrez)
{
	glPushMatrix();
		glTranslatef(0,(montana.getAlto()/2.0)-10,-200);
		glScalef(0.8,1.0,1.0);
		montana.dibuja(iluminacion,false,ajedrez,texturas_visibles);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-200,(montana.getAlto()/2.0)-10,0);
		glRotatef(90,0,1,0);
		glScalef(0.8,1.0,1.0);
		montana.dibuja(iluminacion,false,ajedrez,texturas_visibles);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(200,(montana.getAlto()/2.0)-10,0);
		glRotatef(-90,0,1,0);
		glScalef(0.8,1.0,1.0);
		montana.dibuja(iluminacion,false,ajedrez,texturas_visibles);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(0,(montana.getAlto()/2.0)-10,200);
		glRotatef(180,0,1,0);
		glScalef(0.8,1.0,1.0);
		montana.dibuja(iluminacion,false,ajedrez,texturas_visibles);
	glPopMatrix();
}

void Escena::pintarArboles(modo_ilu iluminacion,bool ajedrez)
{
	bool transparencia = true;
	
	if(modo == puntos || modo == lineas)
		transparencia = false;
	
	Punto3D pos_camara = camara[camara_seleccionada].getVRP(); // VRP de la cámara
	
	pair< int,float > orden[NUM_ARBOLES];
	
	for(int i=0; i<NUM_ARBOLES; ++i){
		orden[i].first = i;
		orden[i].second = distanciaEuclidea2D(pos_camara.x, pos_camara.z, translaciones_arboles[i].x, translaciones_arboles[i].z);
	}
	
	ordenacionInsercionParIntFloat(orden, NUM_ARBOLES);
	
	Punto3D inicial; // Orientación de los impostores
	inicial.x = 0;
	inicial.y = 0;
	inicial.z = 1;
	
	Punto3D direccion; // Vector dirección de la posición del impostor a la posición de la cámara proyectada en el plano XZ
	direccion.y = 0; // Los vectores los trataremos en el plano XZ
	
	float angulo;
	
	for(int i=NUM_ARBOLES-1; i>=0; --i){
		direccion.x = pos_camara.x - translaciones_arboles[orden[i].first].x;
		direccion.z = pos_camara.z - translaciones_arboles[orden[i].first].z;
		
		normalizaVector(direccion);
		
		angulo = radianes2grados( acos( productoEscalar(inicial,direccion) ) );
		if(pos_camara.x < translaciones_arboles[orden[i].first].x)
			angulo = 360 - angulo;
		
		glPushMatrix();
			glTranslatef(translaciones_arboles[orden[i].first].x,translaciones_arboles[orden[i].first].y,translaciones_arboles[orden[i].first].z);
			glRotatef(angulo,0,1,0);
			glScalef(0.25,0.25,0.25);
			arbol.dibuja(iluminacion,transparencia,ajedrez,texturas_visibles);
		glPopMatrix();
	}
}

void Escena::pintarEstanque(modo_ilu iluminacion,bool ajedrez)
{
	cilindro.setAmbient(0.1,0.1,0.1);
	cilindro.setDiffuse(0.043,0.41,0.9335);
	cilindro.setSpecular(0.5,0.5,0.5);
	cilindro.setShininess(32);
	
	glPushMatrix();
		glTranslatef(-75,(cilindro.getTamY()*0.05)/2.0,-60);
		glScalef(35,0.05,75);
		cilindro.dibuja(iluminacion,ajedrez);
	glPopMatrix();
}

void Escena::pintarPrimitivas(modo_ilu iluminacion,bool ajedrez)
{
	esfera.setAmbient(0.19225,0.19225,0.19225);
	esfera.setDiffuse(0.50754,0.50754,0.50754);
	esfera.setSpecular(0.508273,0.508273,0.508273);
	esfera.setShininess(51.2);
	
	cilindro.setAmbient(0.19225,0.19225,0.19225);
	cilindro.setDiffuse(0.50754,0.50754,0.50754);
	cilindro.setSpecular(0.508273,0.508273,0.508273);
	cilindro.setShininess(51.2);
	
	cono.setAmbient(0.19225,0.19225,0.19225);
	cono.setDiffuse(0.50754,0.50754,0.50754);
	cono.setSpecular(0.508273,0.508273,0.508273);
	cono.setShininess(51.2);
	
	cubo.setAmbient(0.19225,0.19225,0.19225);
	cubo.setDiffuse(0.50754,0.50754,0.50754);
	cubo.setSpecular(0.508273,0.508273,0.508273);
	cubo.setShininess(51.2);
	
	tetraedro.setAmbient(0.19225,0.19225,0.19225);
	tetraedro.setDiffuse(0.50754,0.50754,0.50754);
	tetraedro.setSpecular(0.508273,0.508273,0.508273);
	tetraedro.setShininess(51.2);
	
	tetraedroG.setColor(0.50754,0.50754,0.50754);
	cuboG.setColor(0.50754,0.50754,0.50754);
	conoG.setColor(0.50754,0.50754,0.50754);
	cilindroG.setColor(0.50754,0.50754,0.50754);
	
	glPushMatrix();
		glTranslatef(-10,esfera.getTamY()/2,-120);
		esfera.dibuja(iluminacion,ajedrez);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-20,esfera.getTamY()/2,-120);
		cilindro.dibuja(iluminacion,ajedrez);
		glTranslatef(0,0,-10);
		cilindroG.dibuja();
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-30,esfera.getTamY()/2,-120);
		cono.dibuja(iluminacion,ajedrez);
		glTranslatef(0,0,-10);
		conoG.dibuja();
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-40,esfera.getTamY()/2,-120);
		cubo.dibuja(iluminacion,ajedrez);
		glTranslatef(0,0,-10);
		cuboG.dibuja();
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-50,esfera.getTamY()/2,-120);
		tetraedro.dibuja(iluminacion,ajedrez);
		glTranslatef(0,0,-10);
		tetraedroG.dibuja();
	glPopMatrix();
}

void Escena::pintarComedero(modo_ilu iluminacion,bool ajedrez)
{
	glPushMatrix();
		glTranslatef(-65,0,-10);
		glRotatef(90,0,1,0);
		
		for(int i=0; i<6; ++i){
			glPushMatrix();
				glTranslatef((i*3)-7.5,0.601,0);
				glRotatef(-90,1,0,0);
				glScalef(0.0025,0.0025,0.0025);
				paja.dibuja(iluminacion,false,ajedrez);
			glPopMatrix();
		}
	
		glPushMatrix();
			glTranslatef(0,0.3,0);
			glScalef(20,0.6,2);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(0,0.75,-1.25);
			glScalef(20,1.5,0.5);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	glPopMatrix();
}

void Escena::pintarCobertizo(modo_ilu iluminacion,bool ajedrez)
{
	cubo.setAmbient(0.1,0.1,0.1);
	cubo.setDiffuse(0.1,0.1,0.1);
	cubo.setSpecular(0.5,0.5,0.5);
	cubo.setShininess(5);
	
	glPushMatrix();
		glTranslatef(-75,0,35);
	// Patas
		glPushMatrix();
			glTranslatef(-10,4,-30);
			glScalef(1,8,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(-10,4,30);
			glScalef(1,8,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(10,4,-30);
			glScalef(1,8,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(10,4,30);
			glScalef(1,8,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
	// Armazón
		glTranslatef(0,8,0);
		
		glPushMatrix();
			glTranslatef(10,0.5,0);
			glScalef(1,1,61);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-10,0.5,0);
			glScalef(1,1,61);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(0,0.5,30);
			glScalef(19,1,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(0,0.5,-30);
			glScalef(19,1,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glTranslatef(0,1,0);
	// Techo
	
		cubo.setAmbient(0.1,0.1,0.1);
		cubo.setDiffuse(0.1,0.1,0.1);
		cubo.setSpecular(0.1,0.1,0.1);
		cubo.setShininess(5);
		
		glPushMatrix();
			glTranslatef(6.25,3,0);
			glRotatef(-30,0,0,1);
			glScalef(15,1,65);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-6.25,3,0);
			glRotatef(30,0,0,1);
			glScalef(15,1,65);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
	glPopMatrix();
}

void Escena::pintarCaseta(modo_ilu iluminacion,bool ajedrez)
{
	cubo.setAmbient(0.1,0.1,0.1);
	cubo.setDiffuse(0.60,0.40,0.12);
	cubo.setSpecular(0.5,0.5,0.5);
	cubo.setShininess(32);
	
	glPushMatrix();
		glTranslatef(-71,0,85);
	// Patas
		glPushMatrix();
			glTranslatef(-6,3,-10);
			glScalef(1,6,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(-6,3,10);
			glScalef(1,6,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(6,5.5,-10);
			glScalef(1,11,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(6,5.5,10);
			glScalef(1,11,1);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
		
		glTranslatef(0,6.5,0);
	// Techo
		glPushMatrix();
			glTranslatef(0,2,0);
			glRotatef(25,0,0,1);
			glScalef(20,1,25);
			cubo.dibuja(iluminacion,ajedrez);
		glPopMatrix();
	glPopMatrix();
}

GLint Escena::CreaListaEstatico(modo_ilu iluminacion,bool ajedrez_activo)
{
	GLint id_lista = glGenLists(1);
	
	glNewList(id_lista,GL_COMPILE);
	
		glPushMatrix();
			glTranslatef(50,(tablero.getGrosor())/2.0,-120);
			tablero.dibuja(iluminacion,ajedrez_activo);
		glPopMatrix();
	
		pintarValla(iluminacion,ajedrez_activo);
		pintarEstanque(iluminacion,ajedrez_activo);
		pintarPrimitivas(iluminacion,ajedrez_activo);
		pintarComedero(iluminacion,ajedrez_activo);
		pintarCobertizo(iluminacion,ajedrez_activo);
		pintarCaseta(iluminacion,ajedrez_activo);
	
		glPushMatrix();
			glTranslatef(30,(balancin.getTamZ()*0.08)/2.0,-120);
			glScalef(0.08,0.08,0.08);
			glRotatef(-90,1,0,0);
			balancin.dibuja(iluminacion,ajedrez_activo);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(-90,(cubo_basura.getTamZ()*0.05)/2.0,95);
			glScalef(0.05,0.05,0.05);
			glRotatef(-90,1,0,0);
			cubo_basura.dibuja(iluminacion,ajedrez_activo);
		glPopMatrix();
	
		glPushMatrix();
			glTranslatef(0,coche.getTamY()/2,-120);
			coche.dibuja(iluminacion,ajedrez_activo);
		glPopMatrix();
		
	glEndList();
	
	return id_lista;
}
