#include "_o3d_animado.h"

using namespace std;

_O3D_Animado::_O3D_Animado()
{
	rotacion = 0;
	vivo = true;
	angulo_inclinacion = 0.0;
	adulto = false;
	aleatorio.seed();
	aleatorio.init(1,10);
	int sexo = rendondea(aleatorio.value());
	if(sexo <= 7){
		hembra = true;
		fertil = true;
		aleatorio.init(T_REC_FERTIL*0.9,T_REC_FERTIL);
		t_fertil = rendondea(aleatorio.value());
		parir = false;
	}
	else{
		hembra = false;
	}
	
	aleatorio.init(T_VIDA*0.9,T_VIDA);
	esperanza_vida = aleatorio.value();
	contador_vida = 0;
	
	t_vel = lineal;
	estado = idle;
	posicion_actual.x = 0;
	posicion_actual.y = 0;
	posicion_actual.z = 0;
	orientacion_actual.x = 0;
	orientacion_actual.y = 0;
	orientacion_actual.z = 1;
	escalado = 1;
	colisiono = false;
	num_turnos_colision = 0;
	aleatorio.init(0.08,0.25);
	velocidad = aleatorio.value();
	aleatorio.init(0.01,0.03);
	velocidad_angular = aleatorio.value();
	
	aleatorio.init(T_GASTO_AGUA*0.9,T_GASTO_AGUA);
	t_gastar_agua = rendondea(aleatorio.value());
	
	aleatorio.init(T_GASTO_COMIDA*0.9,T_GASTO_COMIDA);
	t_gastar_comida = rendondea(aleatorio.value());

	aleatorio.init(T_GASTO_ENERGIA*0.9,T_GASTO_ENERGIA);
	t_gastar_energia = rendondea(aleatorio.value());
	
	aleatorio.init(T_CRECER_LANA*0.9,T_CRECER_LANA);
	t_crecer_lana = rendondea(aleatorio.value());
	
	aleatorio.init(T_REC_AGUA*0.9,T_REC_AGUA);
	t_recuperar_agua = rendondea(aleatorio.value());
	
	aleatorio.init(T_REC_COMIDA*0.9,T_REC_COMIDA);
	t_recuperar_comida = rendondea(aleatorio.value());
	
	aleatorio.init(T_REC_ENERGIA*0.9,T_REC_ENERGIA);
	t_recuperar_energia = rendondea(aleatorio.value());
	
	aleatorio.init(T_CORTAR_LANA*0.9,T_CORTAR_LANA);
	t_cortar_lana = rendondea(aleatorio.value());
	
	aleatorio.init(0,INI_CONTADORES_MAX);
	contador_agua = rendondea(aleatorio.value());
	contador_comida = rendondea(aleatorio.value());
	contador_energia = rendondea(aleatorio.value());
	contador_lana = rendondea(aleatorio.value());
	
	accion = pastar;
	
	necesita_agua = false;
	necesita_comida = false;
	necesita_energia = false;
	tiene_lana = false;
	
	reabasteciendo = false;
	bebiendo = false;
	comiendo = false;
	durmiendo = false;
	cortando = false;
}

void _O3D_Animado::setID(int i)
{
	id = i;
}

int _O3D_Animado::getID()
{
	return id;
}

void _O3D_Animado::iniciar()
{	
	posicion_inicial = posicion_actual;
	
	if(!colisiono){
		seleccionarAccion();
		num_turnos_colision = 0;
	}
	else{
		++num_turnos_colision;
		if(num_turnos_colision > 20 && !reabasteciendo){
			accion = pastar;
			posicion_colision = posicion_actual;
		}
	}
	
	seleccionaDestino();
	
	orientacion_inicial = orientacion_actual;
	orientacion_final.x = posicion_final.x - posicion_inicial.x;
	orientacion_final.y = posicion_final.y - posicion_inicial.y;
	orientacion_final.z = posicion_final.z - posicion_inicial.z;
	
	normalizaVector(orientacion_final);
	
	ticks_necesarios_avanzar = rendondea(distanciaEuclidea2D(posicion_inicial.x, posicion_inicial.z, posicion_final.x, posicion_final.z) / velocidad);
	float angulo_total = acos( productoEscalar(orientacion_inicial,orientacion_final) / (moduloVector(orientacion_inicial)*moduloVector(orientacion_final)));
	ticks_necesarios_girar = rendondea((angulo_total / velocidad_angular));
	
	tick_actual = 0;
	estado = girando;
}

void _O3D_Animado::pausar()
{
	if(estado != idle){
		estado_guardado = estado;
		estado = idle;
	}
}

void _O3D_Animado::reanudar()
{
	if(estado == idle){
		estado = estado_guardado;
	}
}

void _O3D_Animado::detener()
{
	estado = idle;
}

void _O3D_Animado::actualizar(vector<_O3D_Animado *> &grupo)
{	
	if(estado != idle){
		/****************************************************************
		if(id == 0){
			cout << "*********************" << endl;
			cout << "Posicion: " << posicion_actual.x << " " << posicion_actual.z << endl;
			cout << "contador_agua: " << contador_agua << "\tt_gastar_agua: " << t_gastar_agua << endl;
			cout << "contador_comida: " << contador_comida << "\tt_gastar_comida: " << t_gastar_comida << endl;
			cout << "contador_energia: " << contador_energia << "\tt_gastar_energia: " << t_gastar_energia << endl;
			cout << "contador_lana: " << contador_lana << "\tt_crecer_lana: " << t_crecer_lana << endl;
			cout << "contador_vida: " << contador_vida << "\tesperanza_vida: " << esperanza_vida << endl;
			cout << "accion: ";
			if(accion == beber)
				cout << "beber";
			else if(accion == comer)
				cout << "comer";
			else if(accion == dormir)
				cout << "dormir";
			else if(accion == esquilar)
				cout << "esquilar";
			else if(accion == pastar)
				cout << "pastar";
			cout << endl;
	
			if(dentroPasto()) 		cout << "Dentro pasto" << endl;
			if(dentroAnteAgua()) 	cout << "Dentro ante agua" << endl;
			if(dentroAgua())		cout << "Dentro agua" << endl;
			if(dentroAnteComida())	cout << "Dentro ante comida" << endl;
			if(dentroComida()) 		cout << "Dentro comida" << endl;
			if(dentroAnteDormir()) 	cout << "Dentro ante dormir" << endl;
			if(dentroDormir()) 		cout << "Dentro dormir" << endl;
			if(dentroAnteEsquilar()) cout << "Dentro ante esquilar" << endl;
			if(dentroEsquilar()) 	cout << "Dentro esquilar" << endl;
	
			cout << "Reabasteciendo: " << reabasteciendo << endl;
			cout << "necesita_agua: " << necesita_agua << endl;
			cout << "necesita_comida: " << necesita_comida << endl;
			cout << "necesita_energia: " << necesita_energia << endl;
			cout << "tiene_lana: " << tiene_lana << endl;
			cout << "*********************" << endl;
		}
		****************************************************************/
		++contador_vida;
		
		int util = grupo.size();
		int posicion_mia = -1;
	
		for(int j=0; posicion_mia == -1 && j<util; ++j){
			if(grupo[j]->getID() == this->id){
				posicion_mia = j;
			}
		}
		
		if(contador_vida < esperanza_vida){ // Si está vivo
		
			actualizarVida(grupo,posicion_mia);
		
			if(estado == girando){
				if(tick_actual >= ticks_necesarios_girar){
					orientacion_actual = orientacion_final;
					estado = avanzando;
					tick_actual = 0;
				}
				else{
					float angulo_total = acos( productoEscalar(orientacion_inicial,orientacion_final) / (moduloVector(orientacion_inicial)*moduloVector(orientacion_final)));
					ticks_necesarios_girar = rendondea((angulo_total / velocidad_angular));
					if(ticks_necesarios_girar > 0){
						float t=(float) tick_actual++/(float) ticks_necesarios_girar;
						if(t_vel == senoidal) t = sin((PI/2.0)*t);
	//					[x*cos(a)+z*sin(a),y,-x*sin(a)+z*cos(a)]
						Punto3D prod_vec = productoVectorial(orientacion_inicial,orientacion_final);
				
						if(prod_vec.y < 0)
							angulo_total = -angulo_total;
				
						orientacion_actual.x = orientacion_inicial.x*cos(angulo_total*t)+orientacion_inicial.z*sin(angulo_total*t);
						orientacion_actual.z = -orientacion_inicial.x*sin(angulo_total*t)+orientacion_inicial.z*cos(angulo_total*t);
					}
					else{
						orientacion_actual = orientacion_final;
					}
				}
			}
			else if(estado == avanzando){
				if(tick_actual >= ticks_necesarios_avanzar){
					estado = buscandoObjetivo;
					tick_actual = 0;
					if(necesita_agua && dentroAgua()){
						reabasteciendo = true;
						bebiendo = true;
						contador_agua = 0;
						//cout << "Oveja " << id << " bebiendo" << endl;
					}
					else if(necesita_comida && dentroComida()){
						reabasteciendo = true;
						comiendo = true;
						contador_comida = 0;
						//cout << "Oveja " << id << " comiendo" << endl;
					}
					else if(necesita_energia && dentroDormir()){
						reabasteciendo = true;
						durmiendo = true;
						contador_energia = 0;
						//cout << "Oveja " << id << " durmiendo" << endl;
					}
					else if(tiene_lana && dentroEsquilar()){
						reabasteciendo = true;
						cortando = true;
						contador_lana = 0;
						//cout << "Oveja " << id << " cortando lana" << endl;
					}
				}
				else{
					ticks_necesarios_avanzar = rendondea(distanciaEuclidea2D(posicion_inicial.x, posicion_inicial.z, posicion_final.x, posicion_final.z) / velocidad);
					float t=(float) tick_actual++/(float) ticks_necesarios_avanzar;
					if(t_vel == senoidal) t = sin((PI/2.0)*t);
					Punto3D pos_aux;
					pos_aux.x = posicion_inicial.x * (1-t) + posicion_final.x * t;
					pos_aux.y = posicion_inicial.y;
					pos_aux.z = posicion_inicial.z * (1-t) + posicion_final.z * t;
			
					bool colision = false;
					Punto3D v; // Vector
					float modulo;
				
					for(int j=0; !colision && j<util; ++j){
						if(j != posicion_mia){
							v.x = grupo[j]->posX() - pos_aux.x;
							v.y = grupo[j]->posY() - pos_aux.y;
							v.z = grupo[j]->posZ() - pos_aux.z;
	
							modulo = moduloVector(v);
	
							if(modulo < grupo[posicion_mia]->getRadioEsferaEnglobante() + grupo[j]->getRadioEsferaEnglobante()){
								colision = true;
								if(this->adulto && grupo[j]->esAdulto()){
									if(this->hembra && this->fertil && !(grupo[j]->esHembra())){
										parir = true;
									}
									else if(!(this->hembra) && (grupo[j]->esHembra()) && grupo[j]->fertil){
										grupo[j]->parir = true;
										grupo[j]->contador_fertil = 0;
									}
								}
								punto_colision.x = grupo[j]->posX();
								punto_colision.y = grupo[j]->posY();
								punto_colision.z = grupo[j]->posZ();
								//cout << "Hay colision" << endl;
							}
						}
					}
			
					if(!colision){
						posicion_actual.x = pos_aux.x;
						posicion_actual.y = pos_aux.y;
						posicion_actual.z = pos_aux.z;
						colisiono = false;
					}
					else{
						estado = buscandoObjetivo;
						colisiono = true;
						tick_actual = 0;
					}
				
					if((necesita_agua || necesita_comida || necesita_energia || tiene_lana) && accion == pastar && dentroPasto()){
						estado = buscandoObjetivo;
						tick_actual = 0;
					}	
				}
			}
			else if(estado == buscandoObjetivo){
				if(!reabasteciendo)
					iniciar();
			}
		}
		else{ // Morir
			if(rotacion >= 90)
				vivo = false;
			else{
				rotacion += 0.5;
				float altura = (grupo[posicion_mia]->getTamY()/2.0)*getEscalado();
				posicion_actual.y = altura - (altura - cos(grados2radianes(rotacion)));
				posicion_inicial = posicion_actual;
				posicion_final = posicion_actual;
			}
		}
	}
}

void _O3D_Animado::setEscalado(float e)
{
	if(e > 0)
		escalado = e;
}

float _O3D_Animado::getEscalado()
{
	return (adulto) ? escalado : (escalado/2.0);
}

void _O3D_Animado::setTipoVel(tipo_velocidad t)
{
	t_vel = t;
	nuevaVel();
}

void _O3D_Animado::setVel(float v)
{
	if(v >= 0){
		velocidad = v;
		nuevaVel();
	}
}

float _O3D_Animado::getVel()
{
	return velocidad;
}

void _O3D_Animado::setVelAng(float v)
{
	if(v > 0){
		velocidad_angular = v;
		nuevaVel();
	}
}

float _O3D_Animado::getVelAng()
{
	return velocidad_angular;
}

void _O3D_Animado::setPos(float x,float y,float z)
{
	posicion_actual.x = x;
	posicion_actual.y = y;
	posicion_actual.z = z;
}

float _O3D_Animado::posX()
{
	return posicion_actual.x;
}

float _O3D_Animado::posY()
{
	return posicion_actual.y;
}

float _O3D_Animado::posZ()
{
	return posicion_actual.z;
}

float _O3D_Animado::getRotacion()
{
	return rotacion;
}

void _O3D_Animado::setOrientacionModelo(float x,float y,float z)
{
	orientacion_modelo.x = x;
	orientacion_modelo.y = y;
	orientacion_modelo.z = z;
	normalizaVector(orientacion_modelo);
}

void _O3D_Animado::setOrientacion(float x,float y,float z)
{
	orientacion_actual.x = x;
	orientacion_actual.y = y;
	orientacion_actual.z = z;
	normalizaVector(orientacion_actual);
}

void _O3D_Animado::getOrientacion(float &x,float &y,float &z)
{
	x = orientacion_actual.x;
	y = orientacion_actual.y;
	z = orientacion_actual.z;
}

float _O3D_Animado::orientacion()
{
	float angulo = acos( productoEscalar(orientacion_actual,orientacion_modelo) / (moduloVector(orientacion_actual)*moduloVector(orientacion_modelo)));	
	
	angulo = radianes2grados(angulo);
	angulo = normaliza_angulo(angulo);
	
	Punto3D aux = productoVectorial(orientacion_modelo,orientacion_actual);
	
	if(aux.y < 0) angulo = -angulo;

	return angulo;
}

bool _O3D_Animado::estaVivo()
{
	return vivo;
}

bool _O3D_Animado::esAdulto()
{
	return adulto;
}

bool _O3D_Animado::esHembra()
{
	return hembra;
}

bool _O3D_Animado::esFertil()
{
	return fertil;
}

bool _O3D_Animado::vaAParir()
{
	return parir;
}

void _O3D_Animado::setHaParido()
{
	parir = false;
	fertil = false;
	contador_fertil = 0;
}

void _O3D_Animado::setAdulto()
{
	adulto = true;
	contador_vida = esperanza_vida*0.25;
	hacerAdulto();
}

int _O3D_Animado::edad()
{
	return contador_vida;
}

int _O3D_Animado::getEsperanzaVida()
{
	return esperanza_vida;
}
		
void _O3D_Animado::setEdad(int e)
{
	if(e >= 0){
		contador_vida = e;
	}
}

void _O3D_Animado::nuevaVel()
{
	posicion_inicial = posicion_actual;
	
	orientacion_inicial = orientacion_actual;
	
	ticks_necesarios_avanzar = rendondea(distanciaEuclidea2D(posicion_inicial.x, posicion_inicial.z, posicion_final.x, posicion_final.z) / velocidad);
	float angulo_total = acos( productoEscalar(orientacion_inicial,orientacion_final) / (moduloVector(orientacion_inicial)*moduloVector(orientacion_final)));
	ticks_necesarios_girar = rendondea((angulo_total / velocidad_angular));
	
	tick_actual = 0;
}

void _O3D_Animado::seleccionaDestino()
{
	float e1x=0,e2x=0,e1y=0,e2y=0;
	bool especial = false;

	if(accion == pastar){
		e1x = E1X_PASTO;
		e2x = E2X_PASTO;
		e1y = E1Y_PASTO;
		e2y = E2Y_PASTO;
	}
	else if(accion == dormir){
		if(posicion_actual.z < E1Y_DORMIR){
			e1x = E1X_PASTO;
			e2x = E2X_PASTO;
		}
		else{
			e1x = E1X_DORMIR;
			e2x = E2X_DORMIR;
		}
		e1y = E1Y_DORMIR;
		e2y = E2Y_DORMIR;
		if(dentroDormir() || dentroAnteDormir()) especial = true;
	}
	else if(accion == comer){
		if(posicion_actual.z > E2Y_COMIDA){
			e1x = E1X_PASTO;
			e2x = E2X_PASTO;
		}
		else{
			e1x = E1X_COMIDA;
			e2x = E2X_COMIDA;
		}
		e1y = E1Y_COMIDA;
		e2y = E2Y_COMIDA;
		if(dentroComida() || dentroAnteComida()) especial = true;
	}
	else if(accion == beber){
		if(posicion_actual.z > E2Y_AGUA){
			e1x = E1X_PASTO;
			e2x = E2X_PASTO;
		}
		else{
			e1x = E1X_AGUA;
			e2x = E2X_AGUA;
		}
		e1y = E1Y_AGUA;
		e2y = E2Y_AGUA;
		if(dentroAgua() || dentroAnteAgua()) especial = true;
	}
	else if(accion == esquilar){
		if(posicion_actual.z < E1Y_ESQUILAR){
			e1x = E1X_PASTO;
			e2x = E2X_PASTO;
		}
		else{
			e1x = E1X_ESQUILAR;
			e2x = E2X_ESQUILAR;
		}
		e1y = E1Y_ESQUILAR;
		e2y = E2Y_ESQUILAR;
		if(dentroEsquilar() || dentroAnteEsquilar()) especial = true;
	}	
	
	bool continuar = true;
	int num_intentos = 0;
	
	do{
		++num_intentos;
		aleatorio.init(e1x,e2x); // Para X
		posicion_final.x = aleatorio.value();
		posicion_final.y = posicion_inicial.y;
		if(especial){
			aleatorio.init(posicion_actual.z-0.1,posicion_actual.z+0.1);
		}
		else{
			aleatorio.init(e1y,e2y); // Para Z
		}
		posicion_final.z = aleatorio.value();
	
		if(colisiono){
			if(aLaDerecha(posicion_actual.x, posicion_actual.z, punto_colision.x, punto_colision.z, posicion_final.x, posicion_final.z))
				continuar = false;
		}
		else{
			continuar = false;
		}
	}while(continuar && num_intentos<20);
}

//pastar,dormir,comer,beber,esquilar
void _O3D_Animado::seleccionarAccion()
{
	if(accion == pastar && dentroPasto()){ // Solo se cambia de acción si está pastando dentro del pasto
		if( (!colisiono) || 
			(colisiono && (distanciaEuclidea2D(posicion_colision.x,posicion_colision.z,posicion_actual.x,posicion_actual.z)>4) && posicion_actual.x > 0)){
			//enum tipo_accion {pastar,dormir,comer,beber,esquilar};
			vector<tipo_accion> acciones_posibles;
			acciones_posibles.clear();
		
			bool hay_necesidad = false;
		
			// Seleccionar nueva accion
			if(necesita_agua){
				acciones_posibles.push_back(beber);
				hay_necesidad = true;
			}
		
			if(necesita_comida){
				acciones_posibles.push_back(comer);
				hay_necesidad = true;
			}
		
			if(necesita_energia){
				acciones_posibles.push_back(dormir);
				hay_necesidad = true;
			}
		
			if(tiene_lana){
				acciones_posibles.push_back(esquilar);
				hay_necesidad = true;
			}
		
			if(hay_necesidad){
				aleatorio.init(0,100);
				int ale = rendondea(aleatorio.value());
				if(ale < 0) ale = -ale;
				int pos = ale%acciones_posibles.size();
				accion = acciones_posibles[pos];
			}
			else{
				accion = pastar;
			}
		}
	}
}

bool _O3D_Animado::dentroPasto()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_PASTO, E1Y_PASTO, E2X_PASTO, E2Y_PASTO);
}

bool _O3D_Animado::dentroAnteAgua()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_ANTEAGUA, E1Y_ANTEAGUA, E2X_ANTEAGUA, E2Y_ANTEAGUA);
}

bool _O3D_Animado::dentroAgua()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_AGUA, E1Y_AGUA, E2X_AGUA, E2Y_AGUA);
}

bool _O3D_Animado::dentroAnteComida()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_ANTECOMIDA, E1Y_ANTECOMIDA, E2X_ANTECOMIDA, E2Y_ANTECOMIDA);
}

bool _O3D_Animado::dentroComida()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_COMIDA, E1Y_COMIDA, E2X_COMIDA, E2Y_COMIDA);
}

bool _O3D_Animado::dentroAnteDormir()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_ANTEDORMIR, E1Y_ANTEDORMIR, E2X_ANTEDORMIR, E2Y_ANTEDORMIR);
}

bool _O3D_Animado::dentroDormir()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_DORMIR, E1Y_DORMIR, E2X_DORMIR, E2Y_DORMIR);
}

bool _O3D_Animado::dentroAnteEsquilar()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_ANTEESQUILAR, E1Y_ANTEESQUILAR, E2X_ANTEESQUILAR, E2Y_ANTEESQUILAR);
}

bool _O3D_Animado::dentroEsquilar()
{
	return dentroCuadrado(posicion_actual.x, posicion_actual.z, E1X_ESQUILAR, E1Y_ESQUILAR, E2X_ESQUILAR, E2Y_ESQUILAR);
}

void _O3D_Animado::actualizarVida(vector<_O3D_Animado *> &grupo,int posicion_mia)
{
	++contador_agua;
	++contador_comida;
	++contador_energia;
	if(adulto){
		++contador_lana;
		if(hembra){
			if(!fertil){
				++contador_fertil;
				if(contador_fertil >= t_fertil)
					fertil = true;
			}
		}
	}
	else if(contador_vida >= esperanza_vida*0.25){
		bool puede_crecer = true;
		int tam_grupo = grupo.size();
		Punto3D v;
		float modulo;
		adulto = true; // Trucar función getRadioEsferaEnglobante
		
		for(int i=0; puede_crecer && i<tam_grupo; ++i){
			if(i != posicion_mia){
				v.x = grupo[i]->posX() - posicion_actual.x;
				v.y = grupo[i]->posY() - posicion_actual.y;
				v.z = grupo[i]->posZ() - posicion_actual.z;

				modulo = moduloVector(v);

				if(modulo < grupo[posicion_mia]->getRadioEsferaEnglobante() + grupo[i]->getRadioEsferaEnglobante())
					puede_crecer = false;
			}
		}
		
		if(puede_crecer){
			//adulto = true; Se mantiene
			contador_lana = (t_crecer_lana*0.5) +1;
			grupo[posicion_mia]->hacerAdulto();
			cout << "\nLa oveja " << this->id << " ha alcanzado la edad adulta." << endl;
		}
		else{
			adulto = false; // Trucar función getRadioEsferaEnglobante
		}
	}
	
	if(!reabasteciendo){
		if(contador_agua >= t_gastar_agua && !necesita_agua){
			necesita_agua = true;
			//cout << "Necesita agua" << endl;
		}
		if(contador_comida >= t_gastar_comida && !necesita_comida){
			necesita_comida = true;
			//cout << "Necesita comida" << endl;
		}
		if(contador_energia >= t_gastar_energia && !necesita_energia){
			necesita_energia = true;
			//cout << "Necesita energia" << endl;
		}
		if(adulto && contador_lana >= t_crecer_lana && !tiene_lana){
			tiene_lana = true;
			//cout << "Tiene lana" << endl;
		}
	}
	else{
		if(necesita_agua && contador_agua >= t_recuperar_agua && bebiendo){
			necesita_agua = false;
			reabasteciendo = false;
			bebiendo = false;
			contador_agua = 0;
			accion = pastar;
			estado = buscandoObjetivo;
			//cout << "Oveja " << id << " termina de beber" << endl;
		}
		else if(necesita_comida && contador_comida >= t_recuperar_comida && comiendo){
			necesita_comida = false;
			reabasteciendo = false;
			comiendo = false;
			contador_comida = 0;
			accion = pastar;
			estado = buscandoObjetivo;
			//cout << "Oveja " << id << " termina de comer" << endl;
		}
		else if(necesita_energia && contador_energia >= t_recuperar_energia && durmiendo){
			necesita_energia = false;
			reabasteciendo = false;
			durmiendo = false;
			contador_energia = 0;
			accion = pastar;
			estado = buscandoObjetivo;
			//cout << "Oveja " << id << " termina de dormir" << endl;
		}
		else if(tiene_lana && contador_lana >= t_cortar_lana && cortando){
			tiene_lana = false;
			reabasteciendo = false;
			cortando = false;
			contador_lana = 0;
			accion = pastar;
			estado = buscandoObjetivo;
			//cout << "Oveja " << id << " termina de cortar lana" << endl;
		}
	}
}

