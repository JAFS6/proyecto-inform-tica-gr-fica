#include "_object3d.h"

_Object3D::_Object3D()
{
}

void _Object3D::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	int n = caras.size();
	
	GLfloat material_ambiente[]  = {reflexion_ambiental.r,reflexion_ambiental.g,reflexion_ambiental.b,1.0};
	GLfloat material_difuso[]    = {reflexion_difusa.r   ,reflexion_difusa.g   ,reflexion_difusa.b   ,1.0};
	GLfloat material_especular[] = {reflexion_especular.r,reflexion_especular.g,reflexion_especular.b,1.0};
	GLfloat exponente_brillo = exponente_especular;
	
	if(!ajedrez){
		if(iluminacion == ninguno){
			glColor3f(reflexion_difusa.r,reflexion_difusa.g,reflexion_difusa.b);
			for(int i=0; i<n; ++i){
				darCarasSinIluminado(i);
			}
		}
		else{
			glMaterialfv(GL_FRONT,GL_AMBIENT,material_ambiente);
			glMaterialfv(GL_FRONT,GL_DIFFUSE,material_difuso);
			glMaterialfv(GL_FRONT,GL_SPECULAR,material_especular);
			glMaterialf (GL_FRONT,GL_SHININESS,exponente_brillo);
			if(iluminacion == plano){
				for(int i=0; i<n; ++i){
					darCarasIluminadoPlano(i);
				}
			}
			else if(iluminacion == gouraud){
				for(int i=0; i<n; ++i){
					darCarasIluminadoSuave(i);
				}
			}
		}
    }
    else{
    	if(iluminacion == ninguno){
			glColor3f(1,0,0);
			for(int i=0; i<n; i+=2){
				darCarasSinIluminado(i);
			}
			glColor3f(0,1,0);
			for(int i=1; i<n; i+=2){
				darCarasSinIluminado(i);
			}
		}
		else{
			glMaterialfv(GL_FRONT,GL_AMBIENT,material_ambiente);
			glMaterialfv(GL_FRONT,GL_SPECULAR,material_especular);
			glMaterialf (GL_FRONT,GL_SHININESS,exponente_brillo);
			
			material_difuso[0] = 1.0;
			material_difuso[1] = 0.0;
			material_difuso[2] = 0.0;
			
			glMaterialfv(GL_FRONT,GL_DIFFUSE,material_difuso);
			
			if(iluminacion == plano){
				for(int i=0; i<n; i+=2){
					darCarasIluminadoPlano(i);
				}
			}
			else if(iluminacion == gouraud){
				for(int i=0; i<n; i+=2){
					darCarasIluminadoSuave(i);
				}
			}
			
			material_difuso[0] = 0.0;
			material_difuso[1] = 1.0;
			//material_difuso[2] = 0.0; No cambia
			
			glMaterialfv(GL_FRONT,GL_DIFFUSE,material_difuso);
			
			if(iluminacion == plano){
				for(int i=1; i<n; i+=2){
					darCarasIluminadoPlano(i);
				}
			}
			else if(iluminacion == gouraud){
				for(int i=1; i<n; i+=2){
					darCarasIluminadoSuave(i);
				}
			}
		}
    }
}

float _Object3D::getTamX()
{
	return tam_x;
}

float _Object3D::getTamY()
{
	return tam_y;
}

float _Object3D::getTamZ()
{
	return tam_z;
}

float _Object3D::getRadioEsferaEnglobante()
{
	return radio_esfera_englobante;
}

void _Object3D::setAmbient (float r,float g,float b)
{
	reflexion_ambiental.r = r;
	reflexion_ambiental.g = g;
	reflexion_ambiental.b = b;
}

void _Object3D::setDiffuse (float r,float g,float b)
{
	reflexion_difusa.r = r;
	reflexion_difusa.g = g;
	reflexion_difusa.b = b;
}

void _Object3D::setSpecular(float r,float g,float b)
{
	reflexion_especular.r = r;
	reflexion_especular.g = g;
	reflexion_especular.b = b;
}

void _Object3D::setShininess(float s)
{
	exponente_especular = s;
}

void _Object3D::getAmbient (float &r,float &g,float &b)
{
	r = reflexion_ambiental.r;
	g = reflexion_ambiental.g;
	b = reflexion_ambiental.b;
}

void _Object3D::getDiffuse (float &r,float &g,float &b)
{
	r = reflexion_difusa.r;
	g = reflexion_difusa.g;
	b = reflexion_difusa.b;
}

void _Object3D::getSpecular(float &r,float &g,float &b)
{
	r = reflexion_especular.r;
	g = reflexion_especular.g;
	b = reflexion_especular.b;
}

void _Object3D::getShininess(float &s)
{
	s = exponente_especular;
}

void _Object3D::calcularNormalesCaras()
{
	normales_caras.clear();
	Punto3D a,b,res,vert1,vert2,vert3;
	int n = caras.size();

	for(int i=0; i<n; ++i){
		// Cogemos los 3 vértices de la cara
		vert1 = vertices[caras[i].v1];
		vert2 = vertices[caras[i].v2];
		vert3 = vertices[caras[i].v3];
		// Calculamos dos vectores de la cara
		a.x = vert2.x - vert1.x;
		a.y = vert2.y - vert1.y;
		a.z = vert2.z - vert1.z;
		
		b.x = vert3.x - vert1.x;
		b.y = vert3.y - vert1.y;
		b.z = vert3.z - vert1.z;
		// Calculamos el producto vectorial
		res = productoVectorial(a,b);
		// Normalizamos el vector normal
		normalizaVector(res.x,res.y,res.z);
		// Guardamos el vector normal
		normales_caras.push_back(res);
	}
}

void _Object3D::calcularNormalesVertices()
{
	normales_vertices.clear();
	int num_vertices = vertices.size();
	int num_caras = caras.size();
	Punto3D res;
	res.x = 0;
	res.y = 0;
	res.z = 0;
	int i,contador_aux;
	Cara cara_aux;
	vector<int> contador;
	contador.clear();
	
	// Aprovechamos este bucle para calcular las dimensiones de la caja englobante
	float x_min=0,x_max=0,y_min=0,y_max=0,z_min=0,z_max=0;
	
	for(i=0; i<num_vertices; ++i){
		normales_vertices.push_back(res);
		contador.push_back(0);
		
		// Instrucciones del cálculo de la caja englobante
			 if(vertices[i].x < x_min) x_min = vertices[i].x;
		else if(vertices[i].x > x_max) x_max = vertices[i].x;
		
			 if(vertices[i].y < y_min) y_min = vertices[i].y;
		else if(vertices[i].y > y_max) y_max = vertices[i].y;
		
			 if(vertices[i].z < z_min) z_min = vertices[i].z;
		else if(vertices[i].z > z_max) z_max = vertices[i].z;
	}
	
	// Tamaño de la caja englobante
	tam_x = x_max - x_min;
	tam_y = y_max - y_min;
	tam_z = z_max - z_min;
	
	float mayor;
	
	if(tam_x > tam_y)
		mayor = tam_x;
	else
		mayor = tam_y;
		
	if(tam_z > mayor) mayor = tam_z;
	
	radio_esfera_englobante = mayor/2.0;
	
	for(i=0; i<num_caras; ++i){ // Recorrer todas las caras sumando las normales en cada uno de sus vertices
		cara_aux = caras[i];
		normales_vertices[cara_aux.v1].x += normales_caras[i].x;
		normales_vertices[cara_aux.v1].y += normales_caras[i].y;
		normales_vertices[cara_aux.v1].z += normales_caras[i].z;
		contador[cara_aux.v1]++;
		
		normales_vertices[cara_aux.v2].x += normales_caras[i].x;
		normales_vertices[cara_aux.v2].y += normales_caras[i].y;
		normales_vertices[cara_aux.v2].z += normales_caras[i].z;
		contador[cara_aux.v2]++;
		
		normales_vertices[cara_aux.v3].x += normales_caras[i].x;
		normales_vertices[cara_aux.v3].y += normales_caras[i].y;
		normales_vertices[cara_aux.v3].z += normales_caras[i].z;
		contador[cara_aux.v3]++;
	}
	
	for(i=0; i<num_vertices; ++i){
		contador_aux = contador[i];
		normales_vertices[i].x = normales_vertices[i].x / ((float)contador_aux);
		normales_vertices[i].y = normales_vertices[i].y / ((float)contador_aux);
		normales_vertices[i].z = normales_vertices[i].z / ((float)contador_aux);
		
		normalizaVector(normales_vertices[i].x,normales_vertices[i].y,normales_vertices[i].z);
	}
}

void _Object3D::darCarasSinIluminado(int i)
{
	int v1 = caras[i].v1;
	int v2 = caras[i].v2;
	int v3 = caras[i].v3;
	
	glBegin(GL_TRIANGLES);
        glVertex3f(vertices[v1].x,vertices[v1].y,vertices[v1].z);
        glVertex3f(vertices[v2].x,vertices[v2].y,vertices[v2].z);
        glVertex3f(vertices[v3].x,vertices[v3].y,vertices[v3].z);
    glEnd();
}

void _Object3D::darCarasIluminadoPlano(int i)
{
	int v1 = caras[i].v1;
	int v2 = caras[i].v2;
	int v3 = caras[i].v3;
	
	glBegin(GL_TRIANGLES);
		glNormal3f(normales_caras[i].x,normales_caras[i].y,normales_caras[i].z);
        glVertex3f(vertices[v1].x,vertices[v1].y,vertices[v1].z);
        glVertex3f(vertices[v2].x,vertices[v2].y,vertices[v2].z);
        glVertex3f(vertices[v3].x,vertices[v3].y,vertices[v3].z);
    glEnd();
}

void _Object3D::darCarasIluminadoSuave(int i)
{
	int v1 = caras[i].v1;
	int v2 = caras[i].v2;
	int v3 = caras[i].v3;
	
	glBegin(GL_TRIANGLES);
		glNormal3f(normales_vertices[v1].x,normales_vertices[v1].y,normales_vertices[v1].z);
        glVertex3f(vertices[v1].x,vertices[v1].y,vertices[v1].z);
        glNormal3f(normales_vertices[v2].x,normales_vertices[v2].y,normales_vertices[v2].z);
        glVertex3f(vertices[v2].x,vertices[v2].y,vertices[v2].z);
        glNormal3f(normales_vertices[v3].x,normales_vertices[v3].y,normales_vertices[v3].z);
        glVertex3f(vertices[v3].x,vertices[v3].y,vertices[v3].z);
    glEnd();
}


