#ifndef TETRAEDROG_H
#define TETRAEDROG_H

#include "_object3dg.h"

using namespace std;

class TetraedroG : public _Object3DG
{
private:
    float lado;
public:
    TetraedroG();
    void dibuja();
    float getLado();
    void setLado(float l);
private:
    void creaGeometria();
};

#endif // TETRAEDROG_H
