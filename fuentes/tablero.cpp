#include "tablero.h"

Tablero::Tablero()
{
	tam_lado = 5;
	num_fil = 4;
	num_col = 4;
	
	tipo_casilla[0].setLado(tam_lado);
	tipo_casilla[1].setLado(tam_lado);
	tipo_casilla[1].setAmbient(0.0,0.0,0.0);
	tipo_casilla[1].setDiffuse(0.01,0.01,0.01);
	tipo_casilla[1].setSpecular(0.5,0.5,0.5);
	tipo_casilla[1].setShininess(32);
}

Tablero::Tablero(int l,int f,int c)
{
	if(l <= 0) tam_lado = 1;
	else tam_lado = l;
	
	if(f <= 0) num_fil = 1;
	else num_fil = f;
	
	if(c <= 0) num_col = 1;
	else num_col = c;
	
	tipo_casilla[0].setLado(tam_lado);
	tipo_casilla[1].setLado(tam_lado);
	tipo_casilla[1].setAmbient(0.0,0.0,0.0);
	tipo_casilla[1].setDiffuse(0.01,0.01,0.01);
	tipo_casilla[1].setSpecular(0.5,0.5,0.5);
	tipo_casilla[1].setShininess(32);
}

void Tablero::dibuja(modo_ilu iluminacion,bool ajedrez)
{	
	float inicio_x = -((num_col/2.0)*tam_lado);
	float inicio_z = -((num_fil/2.0)*tam_lado);
	
	if(num_col % 2 == 0) inicio_x += tam_lado/2.0;
	
	if(num_fil % 2 == 0) inicio_z += tam_lado/2.0;

	glPushMatrix();
		glTranslatef(inicio_x,0,inicio_z);
		int i,j;
		int selector = 0;
		int cambio = 0;
		
		if(num_col % 2 == 0) cambio = 1;
		
		for(i=0; i<num_fil; ++i){
			glPushMatrix();
				glTranslatef(0,0,i*tam_lado);
				
				selector = (selector+cambio)%2;
				
				for(j=0; j<num_col; ++j){
					glPushMatrix();
						glTranslatef(j*tam_lado,0,0);
						tipo_casilla[selector].dibuja(iluminacion,ajedrez);
						selector = (selector+1)%2;
					glPopMatrix();
				}
			glPopMatrix();
		}
		
	glPopMatrix();
}

float Tablero::getLado()
{
	return tam_lado;
}

void Tablero::setLado(float l)
{
	if(l > 0){
		tam_lado = l;
		tipo_casilla[0].setLado(tam_lado);
		tipo_casilla[1].setLado(tam_lado);
	}
}

float Tablero::getGrosor()
{
	return tipo_casilla[0].getGrosor();
}

void Tablero::setGrosor(float g)
{
	if(g > 0){
		tipo_casilla[0].setGrosor(g);
		tipo_casilla[1].setGrosor(g);
	}
}

float Tablero::getNumFil()
{
	return num_fil;
}

void Tablero::setNumFil(int f)
{
	if(f > 0){
		num_fil = f;
	}
}

float Tablero::getNumCol()
{
	return num_col;
}

void Tablero::setNumCol(int c)
{
	if(c > 0){
		num_col = c;
	}
}

void Tablero::setDimension(int f,int c)
{
	if(f > 0){
		num_fil = f;
	}
	
	if(c > 0){
		num_col = c;
	}
}

void Tablero::setAmbient  (float r,float g,float b,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].setAmbient(r,g,b);
	}
}

void Tablero::setDiffuse  (float r,float g,float b,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].setDiffuse(r,g,b);
	}
}

void Tablero::setSpecular (float r,float g,float b,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].setSpecular(r,g,b);
	}
}

void Tablero::setShininess(float s,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].setShininess(s);
	}
}

void Tablero::getAmbient  (float &r,float &g,float &b,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].getAmbient(r,g,b);
	}
}

void Tablero::getDiffuse  (float &r,float &g,float &b,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].getDiffuse(r,g,b);
	}
}

void Tablero::getSpecular (float &r,float &g,float &b,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].getSpecular(r,g,b);
	}
}

void Tablero::getShininess(float &s,int t)
{
	if(t == 0 || t == 1){
		tipo_casilla[t].getShininess(s);
	}
}


