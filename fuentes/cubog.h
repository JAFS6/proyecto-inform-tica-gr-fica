#ifndef CUBOG_H
#define CUBOG_H

#include "_object3dg.h"

using namespace std;

class CuboG : public _Object3DG
{
private:
    float lado;
public:
    CuboG();
    void dibuja();
    float getLado();
    void setLado(float l);
private:
    void creaGeometria();
};

#endif // CUBOG_H
