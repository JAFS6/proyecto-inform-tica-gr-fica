#ifndef _TABLERO_H_
#define _TABLERO_H_

#include "casilla.h"

class Tablero
{
	private:
		float tam_lado;
		int num_fil;
		int num_col;
		Casilla tipo_casilla[2];
		
	public:
		/**
			@brief Crea un tablero de 4x4 casillas de lado 5 de plástico negro y plateadas
		*/
		Tablero();
		
		/**
			@brief Crea un tablero de fxc casillas de lado l de plástico negro y plateadas
			@param[in] l Tamaño del lado de las casillas del tablero
			@param[in] f Número de filas del tablero
			@param[in] c Número de columnas del tablero
		*/
		Tablero(int l,int f,int c);
		
		/**
			@brief Dibuja el tablero en posición horizontal centrado en los ejes X,Y,Z
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el coche en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
          @brief Obtiene la medida del lado de las casillas del tablero
          @return La medida del lado de las casillas del tablero
        */
        float getLado();
        
        /**
          @brief Asigna una nueva medida al lado de las casillas del tablero
          @param[in] l La nueva medida del lado de las casillas del tablero
        */
        void setLado(float l);
        
        /**
          @brief Obtiene la medida del grosor de las casillas del tablero
          @return La medida del grosor de las casillas del tablero
        */
        float getGrosor();
        
        /**
          @brief Asigna una nueva medida al grosor de las casillas del tablero
          @param[in] g La nueva medida del grosor de las casillas del tablero
        */
        void setGrosor(float g);
        
        /**
	        @brief Obtiene el número de filas del tablero
	        @return El número de filas del tablero
        */
        float getNumFil();
		
		/**
			@brief Establece el número de filas del tablero
			@param[in] f El número número de filas del tablero
		*/
		void setNumFil(int f);
		
		/**
	        @brief Obtiene el número de columnas del tablero
	        @return El número de columnas del tablero
        */
		float getNumCol();
		
		/**
			@brief Establece el número de columnas del tablero
			@param[in] c El número número de columnas del tablero
		*/
		void setNumCol(int c);
		
		/**
			@brief Establece el número de filas y columnas del tablero
			@param[in] f El número número de filas del tablero
			@param[in] c El número número de columnas del tablero
		*/
		void setDimension(int f,int c);
		
		/**
			@brief Configura la reflexión ambiental del tipo de casilla t
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
			@param[in] t El tipo de casilla 0,1
		*/
		void setAmbient  (float r,float g,float b,int t);
		
		/**
			@brief Configura la reflexión difusa del tipo de casilla t
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
			@param[in] t El tipo de casilla 0,1
		*/
		void setDiffuse  (float r,float g,float b,int t);
		
		/**
			@brief Configura la reflexión especular del tipo de casilla t
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
			@param[in] t El tipo de casilla 0,1
		*/
		void setSpecular (float r,float g,float b,int t);
		
		/**
			@brief Configura el exponente especular del tipo de casilla t
			@param[in] s Exponente especular
			@param[in] t El tipo de casilla 0,1
		*/
		void setShininess(float s,int t);
		
		/**
			@brief Obtiene los valores de la reflexión ambiental del tipo de casilla t
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
			@param[in] t El tipo de casilla 0,1
		*/
		void getAmbient  (float &r,float &g,float &b,int t);
		
		/**
			@brief Obtiene los valores de la reflexión difusa del tipo de casilla t
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
			@param[in] t El tipo de casilla 0,1
		*/
		void getDiffuse  (float &r,float &g,float &b,int t);
		
		/**
			@brief Obtiene los valores de la reflexión especular del tipo de casilla t
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
			@param[in] t El tipo de casilla 0,1
		*/
		void getSpecular (float &r,float &g,float &b,int t);
		
		/**
			@brief Obtiene el valor del exponente especular del tipo de casilla t
			@param[out] s Exponente especular
			@param[in] t El tipo de casilla 0,1
		*/
		void getShininess(float &s,int t);
};

#endif // _TABLERO_H_


