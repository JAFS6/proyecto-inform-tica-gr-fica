#ifndef FARO_H
#define FARO_H

#include "cono.h"

class Faro
{
	private:
		Cono foco;
		float profundidad;
		float diametro;
		
	public:
		/**
			@brief Crea un faro de plástico amarillo de profundidad 1 y diámetro 1
        */
		Faro();
		
		/**
			@brief Crea un faro de plástico amarillo de profundidad p, diámetro d
			@param[in] p La profundidad del faro
			@param[in] d El diámetro del faro
        */
		Faro(float p,float d);
		
		/**
			@brief Dibuja el faro en posición horizontal centrado en los ejes X,Y,Z orientado en el sentido positivo del eje Z
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el faro en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
		  @brief Obtiene la profundidad del faro
		  @return La profundidad del faro
		*/
		float getProfundidad();
		
		/**
		  @brief Establece la profundidad del faro
		  @param[in] p La nueva profundidad del faro
		*/
		void setProfundidad(float p);
		
		/**
		  @brief Obtiene el diámetro del faro
		  @return El diámetro del faro
		*/
		float getDiametro();
		
		/**
		  @brief Establece el diámetro del faro
		  @param[in] d El nuevo diámetro del faro
		*/
		void setDiametro(float d);
		
		/**
			@brief Configura la reflexión ambiental
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setAmbient  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión difusa
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setDiffuse  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión especular
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setSpecular (float r,float g,float b);
		
		/**
			@brief Configura el exponente especular
			@param[in] s Exponente especular
		*/
		void setShininess(float s);
		
		/**
			@brief Obtiene los valores de la reflexión ambiental
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getAmbient  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión difusa
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getDiffuse  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión especular
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getSpecular (float &r,float &g,float &b);
		
		/**
			@brief Obtiene el valor del exponente especular
			@param[out] s Exponente especular
		*/
		void getShininess(float &s);
};

#endif // FARO_H

