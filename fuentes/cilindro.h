#ifndef CILINDRO_H
#define CILINDRO_H

#include "_object3d.h"

class Cilindro : public _Object3D
{
	private:
		float altura;
		float radio;
		unsigned short num_div;
		
	public:
		/**
          @brief Crea un cilindro plateado de radio 0.5, altura 1 y 36 divisiones de resoluci�n
        */
		Cilindro();
		
		/**
          @brief Crea un cilindro plateado de altura h y radio rad, con d divisiones de resoluci�n
          @param[in] h La altura del cilindro
          @param[in] rad El radio del cilindro
          @param[in] d El n�mero de divisiones de resoluci�n del cilindro
        */
        Cilindro(float h,float rad,unsigned short d);
        
		/**
		  @brief Obtiene la altura del cilindro
		  @return La altura del cilindro
		*/
		float getAltura();
		
		/**
		  @brief Establece la altura del cilindro
		  @param[in] h La nueva altura del cilindro
		*/
		void setAltura(float h);
		
		/**
		  @brief Obtiene el radio del cilindro
		  @return El radio del cilindro
		*/
		float getRadio();
		
		/**
		  @brief Establece el radio del cilindro
		  @param[in] r El nuevo radio del cilindro
		*/
		void setRadio(float r);
		
		/**
		  @brief Obtiene el n�mero de divisiones de resoluci�n del cilindro
		  @return El n�mero de divisiones de resoluci�n del cilindro
		*/
		unsigned short getNumDiv();
		
		/**
		  @brief Establece el n�mero de divisiones de resoluci�n del cilindro
		  @param[in] d El nuevo n�mero de divisiones de resoluci�n del cilindro
		*/
		void setNumDiv(unsigned short d);
		
	private:
        /**
          @brief Crea la geometr�a del cilindro
        */
        void creaGeometria();
        
        /**
          @brief Crea la topolog�a del cilindro
        */
        void creaTopologia();
};

#endif // CILINDRO_H

