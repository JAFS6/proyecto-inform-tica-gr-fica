#ifndef CONO_H
#define CONO_H

#include "_object3d.h"

class Cono : public _Object3D
{
	private:
		float altura;
		float radio;
		unsigned short num_div;
		
	public:
		/**
          @brief Crea un cono plateado de radio 0.5, altura 1 y 36 divisiones de resoluci�n
        */
		Cono();
		
		/**
          @brief Crea un cono plateado de altura h y radio rad, con d divisiones de resoluci�n
          @param[in] h La altura del cono
          @param[in] rad El radio del cono
          @param[in] d El n�mero de divisiones de resoluci�n del cono
        */
        Cono(float h,float rad,unsigned short d);
        
		/**
		  @brief Obtiene la altura del cono
		  @return La altura del cono
		*/
		float getAltura();
		
		/**
		  @brief Establece la altura del cono
		  @param[in] h La nueva altura del cono
		*/
		void setAltura(float h);
		
		/**
		  @brief Obtiene el radio del cono
		  @return El radio del cono
		*/
		float getRadio();
		
		/**
		  @brief Establece el radio del cono
		  @param[in] r El nuevo radio del cono
		*/
		void setRadio(float r);
		
		/**
		  @brief Obtiene el n�mero de divisiones de resoluci�n del cono
		  @return El n�mero de divisiones de resoluci�n del cono
		*/
		unsigned short getNumDiv();
		
		/**
		  @brief Establece el n�mero de divisiones de resoluci�n del cono
		  @param[in] d El nuevo n�mero de divisiones de resoluci�n del cono
		*/
		void setNumDiv(unsigned short d);
		
	private:
        /**
          @brief Crea la geometr�a del cono
        */
        void creaGeometria();
        
        /**
          @brief Crea la topolog�a del cono
        */
        void creaTopologia();
};

#endif // CONO_H

