#include "_o3d_solido_animado.h"

_O3D_Solido_Animado::_O3D_Solido_Animado()
{
	objeto = 0;
}

_O3D_Solido_Animado::_O3D_Solido_Animado(_Object3D * o)
{
	objeto = o;
}

void _O3D_Solido_Animado::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	if(objeto != NULL){
		objeto->setAmbient (reflexion_ambiental.r,reflexion_ambiental.g,reflexion_ambiental.b);
		objeto->setDiffuse (reflexion_difusa.r,   reflexion_difusa.g,   reflexion_difusa.b);
		objeto->setSpecular(reflexion_especular.r,reflexion_especular.g,reflexion_especular.b);
		objeto->setShininess(exponente_especular);
		
		glPushMatrix();
			glTranslatef(posicion_actual.x,posicion_actual.y,posicion_actual.z);
			glRotatef(orientacion(),0,1,0);
			glScalef(escalado,escalado,escalado);
			objeto->dibuja(iluminacion,ajedrez);
		glPopMatrix();
	}
	else{
		std::cerr << "No hay objeto para dibujar" << std::endl;
	}
}

void _O3D_Solido_Animado::setObjeto(_Object3D * o)
{
	objeto = o;
}

float _O3D_Solido_Animado::getRadioEsferaEnglobante()
{
	return (escalado * objeto->getRadioEsferaEnglobante());
}

void _O3D_Solido_Animado::setAmbient (float r,float g,float b)
{
	reflexion_ambiental.r = r;
	reflexion_ambiental.g = g;
	reflexion_ambiental.b = b;
}

void _O3D_Solido_Animado::setDiffuse (float r,float g,float b)
{
	reflexion_difusa.r = r;
	reflexion_difusa.g = g;
	reflexion_difusa.b = b;
}

void _O3D_Solido_Animado::setSpecular(float r,float g,float b)
{
	reflexion_especular.r = r;
	reflexion_especular.g = g;
	reflexion_especular.b = b;
}

void _O3D_Solido_Animado::setShininess(float s)
{
	exponente_especular = s;
}

void _O3D_Solido_Animado::getAmbient (float &r,float &g,float &b)
{
	r = reflexion_ambiental.r;
	g = reflexion_ambiental.g;
	b = reflexion_ambiental.b;
}

void _O3D_Solido_Animado::getDiffuse (float &r,float &g,float &b)
{
	r = reflexion_difusa.r;
	g = reflexion_difusa.g;
	b = reflexion_difusa.b;
}

void _O3D_Solido_Animado::getSpecular(float &r,float &g,float &b)
{
	r = reflexion_especular.r;
	g = reflexion_especular.g;
	b = reflexion_especular.b;
}

void _O3D_Solido_Animado::getShininess(float &s)
{
	s = exponente_especular;
}

