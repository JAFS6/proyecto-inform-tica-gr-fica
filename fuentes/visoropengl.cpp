#include "visoropengl.h"
#include <QCoreApplication>

VisorOpenGL::VisorOpenGL(QWidget *parent) : QGLWidget(parent)
{
	// Inicializamos los �ngulos de rotaci�n
	observer_angle_x = 0;
	observer_angle_y = 0;
	observer_angle_z = 0;

	// Definimos el tama�o del volumen de visualizaci�n
	Window_width = 5;
	Window_height = 5;
	//Front_plane = 10;
	Front_plane = 5;
	Back_plane = 1000;

	// se inicia la posicion del observador, en el eje z
	Observer_distance = 2*Front_plane;

	// permitimos al componente tener el foco y aceptar eventos
	setFocusPolicy(Qt::StrongFocus);
	
	// Configuraci�n del timer
	temporizador = new QTimer(this);
	temporizador->setInterval(40);
	temporizador->setSingleShot(false);
	connect(temporizador, SIGNAL(timeout()), this, SLOT(actualizarEscena()));
	temporizador->start();
}

VisorOpenGL::~VisorOpenGL()
{
	makeCurrent();
}

QSize VisorOpenGL::minimumSizeHint() const
{
	return QSize(100,100);
}

QSize VisorOpenGL::sizeHint() const
{
	return QSize(800,600);
}


void VisorOpenGL::setXRotation(float angle)
{
	normalizeAngle(angle);
	
	if(angle != observer_angle_x){
		observer_angle_x = angle;
		emit xRotationChanged(angle);
		updateGL();
	}
}

void VisorOpenGL::setYRotation(float angle)
{
	normalizeAngle(angle);
	
	if(angle != observer_angle_y){
		observer_angle_y = angle;
		emit xRotationChanged(angle);
		updateGL();
	}
}

void VisorOpenGL::normalizeAngle(float angle)
{
	while (angle>360)
		angle-=360;
	while (angle<0)
		angle+=360;
}


void VisorOpenGL::initializeGL()
{
	glClearColor(0.043,0.41,0.9335,1.0);
	glShadeModel(GL_FLAT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_NORMALIZE);
}


void VisorOpenGL::resizeGL(int width, int height)
{
	glViewport(0,0,width,height);
	h = (double)height / (double)width;
	setProjection();
}

void VisorOpenGL::setProjection()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1.0f*Window_width,+1.0f*Window_width,-h*Window_height,h*Window_height,Front_plane,Back_plane);
	glMatrixMode(GL_MODELVIEW);
}

void VisorOpenGL::setObserver()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0,0,-Observer_distance);
	glRotatef(observer_angle_x,1,0,0);
	glRotatef(observer_angle_y,0,1,0);
}

void VisorOpenGL::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	setProjection();
	setObserver();
	dibujarEscena();
}

void VisorOpenGL::mousePressEvent(QMouseEvent *event)
{
	lastPos = event->pos();
}

void VisorOpenGL::mouseMoveEvent(QMouseEvent *event)
{
	int dx = event->x() - lastPos.x();
	int dy = event->y() - lastPos.y();

	if(event->buttons() & Qt::LeftButton){
		setXRotation(observer_angle_x + dy/3);
		setYRotation(observer_angle_y + dx/3);
	}
	lastPos = event->pos();
}

void VisorOpenGL::keyPressEvent(QKeyEvent *event)
{
	switch(event->key() | event->modifiers()){
	// Vista
		//Giro
		case Qt::Key_Right: escena.giroHorizontal(-0.8);break;
		case Qt::Key_Left:  escena.giroHorizontal(0.8);break;
		case Qt::Key_Up:    escena.giroVertical(-0.6);break;
		case Qt::Key_Down:  escena.giroVertical(0.6);break;
		case Qt::Key_Q:     escena.rotacion(-0.5);break;
		case Qt::Key_E:     escena.rotacion(0.5);break;
		//Desplazamiento
		case Qt::Key_W:     escena.desplazamientoFrontal(0.5);break;
		case Qt::Key_A:     escena.desplazamientoLateral(-0.6);break;
		case Qt::Key_S:     escena.desplazamientoFrontal(-0.5);break;
		case Qt::Key_D:     escena.desplazamientoLateral(0.6);break;
		
		case Qt::Key_W  | Qt::ControlModifier: escena.desplazamientoVertical(0.5);break;
		case Qt::Key_S  | Qt::ControlModifier: escena.desplazamientoVertical(-0.5);break;
		// Colocar vista
		case Qt::Key_C:escena.colocarCamara();break;
		case Qt::Key_R | Qt::ControlModifier:escena.reiniciarCamara(escena.camaraSeleccionada());break;
	// Vistas predefinidas
		case Qt::Key_1 | Qt::ControlModifier:escena.setCamara(0);break;
		case Qt::Key_2 | Qt::ControlModifier:escena.setCamara(1);break;
		case Qt::Key_3 | Qt::ControlModifier:escena.setCamara(2);break;
		case Qt::Key_4 | Qt::ControlModifier:escena.setCamara(3);break;
		case Qt::Key_5 | Qt::ControlModifier:escena.setCamara(4);break;
		case Qt::Key_6 | Qt::ControlModifier:escena.setCamara(5);break;
		case Qt::Key_7 | Qt::ControlModifier:escena.setCamara(6);break;
		case Qt::Key_8 | Qt::ControlModifier:escena.setCamara(7);break;
		case Qt::Key_Tab:	 escena.camaraOvejaSiguiente();break;
		case Qt::Key_Tab | Qt::ControlModifier:escena.camaraOvejaAnterior();break;
	// Modos de visualizacion
		case Qt::Key_F1:escena.setModo(puntos);break;
		case Qt::Key_F2:escena.setModo(lineas);break;
		case Qt::Key_F3:escena.setModo(ajedrez);break;
		case Qt::Key_F4:escena.setModo(solido);break;
		case Qt::Key_F5:escena.setIluminacion(ninguno);break;
		case Qt::Key_F6:escena.setIluminacion(plano);break;
		case Qt::Key_F7:escena.setIluminacion(gouraud);break;
	// Interruptores de las luces
		case Qt::Key_1:escena.pulsarInterruptor(0);break;
		case Qt::Key_2:escena.pulsarInterruptor(1);break;
		case Qt::Key_3:escena.pulsarInterruptor(2);break;
		case Qt::Key_4:escena.pulsarInterruptor(3);break;
		case Qt::Key_5:escena.pulsarInterruptor(4);break;
		case Qt::Key_9:escena.encenderTodas();break;
		case Qt::Key_0:escena.apagarTodas();break;
	// Animaci�n
		case Qt::Key_I:escena.iniciarAnimacion();break;
		case Qt::Key_P:escena.pausarAnimacion();break;
		case Qt::Key_U:escena.detenerAnimacion();break;
		case Qt::Key_Plus:escena.incrementarVelocidad();break;
		case Qt::Key_Minus:escena.decrementarVelocidad();break;
		case Qt::Key_M:escena.incrementarVelocidadAngular();break;
		case Qt::Key_N:escena.decrementarVelocidadAngular();break;
		case Qt::Key_L:escena.setTipoVelocidad(lineal);break;
		case Qt::Key_K:escena.setTipoVelocidad(senoidal);break;
	// Reba�o
		case Qt::Key_Home:escena.anadirOveja();break;
		case Qt::Key_End: escena.quitarOveja();break;
		case Qt::Key_F:escena.mostrarNumeroOvejas();break;
	// Otros
		case Qt::Key_E | Qt::AltModifier:escena.altEjes();break;
		case Qt::Key_T:escena.altTexturas();break;
		case Qt::Key_H:imprimirAyuda();break;
		case Qt::Key_G:escena.altLucesEstaticas();break;
		case Qt::Key_Escape:
			escena.liberarRecursos();
			QCoreApplication::exit(0);
			break;
		default:QGLWidget::keyPressEvent(event);
	}
	updateGL();
	//Ejemplo modificadores
	// case Qt::Key_E | Qt::ControlModifier: lo que sea; break;
	// case Qt::Key_E | Qt::AltModifier: lo que sea; break;
	// case Qt::Key_E | Qt::ShiftModifier: lo que sea; break;
}

void VisorOpenGL::dibujarEscena()
{
	this->escena.draw();
}

void VisorOpenGL::actualizarEscena()
{
	this->escena.actualizar_escena();
	updateGL();
}

