#include "rueda.h"

Rueda::Rueda()
{
	ancho = 1;
	diametro = 2;
	goma.setAmbient(0.0,0.1,0.06);
	goma.setDiffuse(0.0,0.50980392,0.50980392);
	goma.setSpecular(0.50196078,0.50196078,0.50196078);
	goma.setShininess(32);
}

Rueda::Rueda(float a,float d)
{
	ancho = a;
	diametro = d;
	goma.setAmbient(0.0,0.1,0.06);
	goma.setDiffuse(0.0,0.50980392,0.50980392);
	goma.setSpecular(0.50196078,0.50196078,0.50196078);
	goma.setShininess(32);
}

void Rueda::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	glPushMatrix();
		glRotatef(90,0,0,1);
		glScalef(diametro,ancho,diametro);
		goma.dibuja(iluminacion,ajedrez);
	glPopMatrix();
}

float Rueda::getAncho()
{
	return ancho;
}

void Rueda::setAncho(float a)
{
	ancho = a;
}

float Rueda::getDiametro()
{
	return diametro;
}

void Rueda::setDiametro(float d)
{
	diametro = d;
}

void Rueda::setAmbient (float r,float g,float b)
{
	goma.setAmbient(r,g,b);
}

void Rueda::setDiffuse (float r,float g,float b)
{
	goma.setDiffuse(r,g,b);
}

void Rueda::setSpecular(float r,float g,float b)
{
	goma.setSpecular(r,g,b);
}

void Rueda::setShininess(float s)
{
	goma.setShininess(s);
}

void Rueda::getAmbient (float &r,float &g,float &b)
{
	goma.getAmbient(r,g,b);
}

void Rueda::getDiffuse (float &r,float &g,float &b)
{
	goma.getDiffuse(r,g,b);
}

void Rueda::getSpecular(float &r,float &g,float &b)
{
	goma.getSpecular(r,g,b);
}

void Rueda::getShininess(float &s)
{
	goma.getShininess(s);
}


