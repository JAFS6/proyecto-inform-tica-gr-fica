#ifndef CHASIS_H
#define CHASIS_H

#include "cubo.h"

class Chasis
{
	private:
		Cubo chasis;
		float ancho;
		float largo;
		float alto;
		
	public:
		/**
			@brief Crea un chasis de plástico rojo de 6 de ancho por 10 de largo por 3 de alto
        */
		Chasis();
		
		/**
			@brief Crea un chasis de plástico rojo de ancho por largo por alto
			@param[in] ancho El ancho del chasis
			@param[in] largo El largo del chasis
			@param[in] alto El alto del chasis
        */
		Chasis(float ancho,float largo,float alto);
		
		/**
			@brief Dibuja el chasis en posición horizontal centrado en los ejes X,Y,Z orientado en el sentido positivo del eje Z
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el chasis en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
		  @brief Obtiene el ancho del chasis
		  @return El ancho del chasis
		*/
		float getAncho();
		
		/**
		  @brief Establece el ancho del chasis
		  @param[in] a El nuevo ancho del chasis
		*/
		void setAncho(float a);
		
		/**
		  @brief Obtiene el largo del chasis
		  @return El largo del chasis
		*/
		float getLargo();
		
		/**
		  @brief Establece el largo del chasis
		  @param[in] l El nuevo largo del chasis
		*/
		void setLargo(float l);
		
		/**
		  @brief Obtiene el alto del chasis
		  @return El alto del chasis
		*/
		float getAlto();
		
		/**
		  @brief Establece el alto del chasis
		  @param[in] a El nuevo alto del chasis
		*/
		void setAlto(float a);
		
		/**
			@brief Configura la reflexión ambiental
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setAmbient  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión difusa
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setDiffuse  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión especular
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setSpecular (float r,float g,float b);
		
		/**
			@brief Configura el exponente especular
			@param[in] s Exponente especular
		*/
		void setShininess(float s);
		
		/**
			@brief Obtiene los valores de la reflexión ambiental
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getAmbient  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión difusa
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getDiffuse  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión especular
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getSpecular (float &r,float &g,float &b);
		
		/**
			@brief Obtiene el valor del exponente especular
			@param[out] s Exponente especular
		*/
		void getShininess(float &s);
};

#endif // CHASIS_H

