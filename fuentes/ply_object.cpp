#include "ply_object.h"

PLY_Object::PLY_Object()
{
	vertices.clear();
	caras.clear();
}

PLY_Object::PLY_Object(vector<float> nvertices,vector<int> ncaras)
{
	cargarModelo(nvertices,ncaras);
}

PLY_Object::PLY_Object(char * fich)
{
	cargarModelo(fich);
}

void PLY_Object::cargarModelo(vector<float> nvertices,vector<int> ncaras)
{
	vertices.clear();
    caras.clear();
    Punto3D aux;
    int num_vert = nvertices.size();
    int num_caras = ncaras.size();

    for(int i=0; i<num_vert;i+=3){
        aux.x = nvertices[i];
        aux.y = nvertices[i+1];
        aux.z = nvertices[i+2];
        vertices.push_back(aux);
    }

    Cara cara_aux;

    for(int i=0; i<num_caras;i+=3){
        cara_aux.v1 = ncaras[i];
        cara_aux.v2 = ncaras[i+1];
        cara_aux.v3 = ncaras[i+2];
        caras.push_back(cara_aux);
    }
    
	centrarMalla();
    calcularNormalesCaras();
	calcularNormalesVertices();
}

void PLY_Object::cargarModelo(char * fich)
{
	vector<float> vertices_leidos;
    vector<int> caras_leidas;
    vertices_leidos.clear();
    caras_leidas.clear();
    
	ifstream in;

	in.open (fich, ifstream::in);
	
	if(in.fail()){
		cerr << "\nError al abrir el fichero: " << fich << endl;
		exit(1);
	}
	
	char buffer[1024];
	int num_vertices,num_caras,aux2;
	float aux;
	//cout << "Fichero: " << fich << endl;
	in.getline(buffer,1024);
	in.getline(buffer,1024);
	in.getline(buffer,1024,' ');
	in.getline(buffer,1024,' ');
	in >> num_vertices;
	in.ignore();
	//cout << "num vertices: " << num_vertices << endl;
	in.getline(buffer,1024);
	in.getline(buffer,1024);
	in.getline(buffer,1024);
	in.getline(buffer,1024,' ');
	in.getline(buffer,1024,' ');
	in >> num_caras;
	in.ignore();
	//cout << "num caras: " << num_caras << endl;
	in.getline(buffer,1024);
	in.getline(buffer,1024);
	
	for(int i=0; i<num_vertices; ++i){
		in >> aux;
		vertices_leidos.push_back(aux);
		in >> aux;
		vertices_leidos.push_back(aux);
		in >> aux;
		vertices_leidos.push_back(aux);
	}
	
	for(int i=0; i<num_caras; ++i){
		in >> aux2; // 3
		in >> aux2;
		caras_leidas.push_back(aux2);
		in >> aux2;
		caras_leidas.push_back(aux2);
		in >> aux2;
		caras_leidas.push_back(aux2);
	}
	
	in.close();

    cargarModelo(vertices_leidos,caras_leidas);
}

void PLY_Object::centrarMalla()
{
	int n = vertices.size();
	float max_x = vertices[0].x;
	float min_x = vertices[0].x;
	float max_y = vertices[0].y;
	float min_y = vertices[0].y;
	float max_z = vertices[0].z;
	float min_z = vertices[0].z;
	
	for(int i=1; i<n; ++i){
		     if(vertices[i].x > max_x) max_x = vertices[i].x;
		else if(vertices[i].x < min_x) min_x = vertices[i].x;
		
		     if(vertices[i].y > max_y) max_y = vertices[i].y;
		else if(vertices[i].y < min_y) min_y = vertices[i].y;
		
		     if(vertices[i].z > max_z) max_z = vertices[i].z;
		else if(vertices[i].z < min_z) min_z = vertices[i].z;
	}
	
	float dx = (max_x + min_x)/2.0;
	float dy = (max_y + min_y)/2.0;
	float dz = (max_z + min_z)/2.0;
	
	for(int i=0; i<n; ++i){
		vertices[i].x -= dx;
		vertices[i].y -= dy;
		vertices[i].z -= dz;
	}
}


