#ifndef CONOG_H
#define CONOG_H

#include "_object3dg.h"

class ConoG : public _Object3DG
{
private:
    float altura;
    float radio;
    short int num_divisiones;
public:
    ConoG();
    void dibuja();
    float getAltura();
    void setAltura(float a);
    float getRadio();
    void setRadio(float r);
    float getNumDiv();
    void setNumDiv(float nd);
private:
    void creaGeometria(); /* Almacenamiento de v�rtices
                            vertices[0] V�rtice superior
                            vertices[1..n-2] V�rtices de la base en sentido antihorario visto desde el semieje positivo Y
                            vertices[n-1] V�rtice central de la base
                            */
};

#endif // CONOG_H
