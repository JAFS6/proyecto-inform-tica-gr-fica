#include "_impostor.h"

_Impostor::_Impostor()
{
	alto = 0;
	ancho = 0;
	id = -1;
}

unsigned _Impostor::getAlto()
{
	return alto;
}

unsigned _Impostor::getAncho()
{
	return ancho;
}

void _Impostor::setImagen(const char * img)
{
	int id_aux = CargaTextura( img, ancho, alto);

	if(id_aux != -1){
		id = id_aux;
		//cout << "Imagen " << img << " cargada con id " << id << endl;
	}
	else{
		cerr << "Error en carga de textura " << img << endl;
		exit(1);
	}
}

void _Impostor::dibuja(modo_ilu iluminacion,bool transparente,bool ajedrez,bool textura_visible)
{
	if(id != -1){
		
		float anchomedio = ancho / 2.0;
		float altomedio = alto / 2.0;
		
		if(!ajedrez){
			GLfloat material_difuso[]    = {1.0,1.0,1.0,1.0};
			if(iluminacion == ninguno){
				glColor3f(material_difuso[0],material_difuso[1],material_difuso[2]);
			}
			else{
				GLfloat material_ambiente[]  = {0.1,0.1,0.1,1.0};
				GLfloat material_especular[] = {0.0,0.0,0.0,1.0};
				GLfloat exponente_brillo = 1;
			
				glMaterialfv(GL_FRONT,GL_AMBIENT,material_ambiente);
				glMaterialfv(GL_FRONT,GL_DIFFUSE,material_difuso);
				glMaterialfv(GL_FRONT,GL_SPECULAR,material_especular);
				glMaterialf (GL_FRONT,GL_SHININESS,exponente_brillo);
			}
			
			if(textura_visible){
				glEnable( GL_TEXTURE_2D );
				if(transparente){
					glEnable (GL_BLEND);
					glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				}
				glBindTexture( GL_TEXTURE_2D, id );
			
				glNormal3f(0,0,1);
				glBegin( GL_TRIANGLES );
					glTexCoord2d(0.0,0.0); glVertex3f(-anchomedio,altomedio,0.0);
					glTexCoord2d(1.0,1.0); glVertex3f(anchomedio,-altomedio,0.0);
					glTexCoord2d(1.0,0.0); glVertex3f(anchomedio,altomedio,0.0);
				glEnd();

				glBegin( GL_TRIANGLES );
					glTexCoord2d(0.0,0.0); glVertex3f(-anchomedio,altomedio,0.0);
					glTexCoord2d(0.0,1.0); glVertex3f(-anchomedio,-altomedio,0.0);
					glTexCoord2d(1.0,1.0); glVertex3f(anchomedio,-altomedio,0.0);
				glEnd();
		
				if(transparente)
					glDisable (GL_BLEND);
				glDisable( GL_TEXTURE_2D );
			}
			else{
				glNormal3f(0,0,1);
				glBegin( GL_TRIANGLES );
					glVertex3f(-anchomedio,altomedio,0.0);
					glVertex3f(anchomedio,-altomedio,0.0);
					glVertex3f(anchomedio,altomedio,0.0);
				glEnd();

				glBegin( GL_TRIANGLES );
					glVertex3f(-anchomedio,altomedio,0.0);
					glVertex3f(-anchomedio,-altomedio,0.0);
					glVertex3f(anchomedio,-altomedio,0.0);
				glEnd();
			}
		}
		else{
			if(iluminacion == ninguno){
				glColor3f(1,0,0);
				glBegin( GL_TRIANGLES );
					glVertex3f(-anchomedio,altomedio,0.0);
					glVertex3f(anchomedio,-altomedio,0.0);
					glVertex3f(anchomedio,altomedio,0.0);
				glEnd();
				glColor3f(0,1,0);
				glBegin( GL_TRIANGLES );
					glVertex3f(-anchomedio,altomedio,0.0);
					glVertex3f(-anchomedio,-altomedio,0.0);
					glVertex3f(anchomedio,-altomedio,0.0);
				glEnd();
			}
			else{
				GLfloat material_ambiente[]  = {0.0,0.0,0.0,1.0};
				GLfloat material_difuso[]    = {0.0,0.0,0.0,1.0};
				GLfloat material_especular[] = {0.0,0.0,0.0,1.0};
				GLfloat exponente_brillo = 1;
				
				glMaterialfv(GL_FRONT,GL_AMBIENT,material_ambiente);
				glMaterialfv(GL_FRONT,GL_SPECULAR,material_especular);
				glMaterialf (GL_FRONT,GL_SHININESS,exponente_brillo);
			
				material_difuso[0] = 1.0;
				material_difuso[1] = 0.0;
				material_difuso[2] = 0.0;
			
				glMaterialfv(GL_FRONT,GL_DIFFUSE,material_difuso);
			
				glNormal3f(0,0,1);
				glBegin( GL_TRIANGLES );
					glVertex3f(-anchomedio,altomedio,0.0);
					glVertex3f(anchomedio,-altomedio,0.0);
					glVertex3f(anchomedio,altomedio,0.0);
				glEnd();
			
				material_difuso[0] = 0.0;
				material_difuso[1] = 1.0;
				//material_difuso[2] = 0.0; No cambia
				
				glMaterialfv(GL_FRONT,GL_DIFFUSE,material_difuso);
				
				glNormal3f(0,0,1);
				glBegin( GL_TRIANGLES );
					glVertex3f(-anchomedio,altomedio,0.0);
					glVertex3f(-anchomedio,-altomedio,0.0);
					glVertex3f(anchomedio,-altomedio,0.0);
				glEnd();
			}
		}
	}
	else{
		cerr << "No hay textura cargada" << endl;
	}
}

