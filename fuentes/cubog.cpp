#include "cubog.h"

CuboG::CuboG()
{
    lado = 1;
	creaGeometria();
}

void CuboG::dibuja()
{
    int indice[16] = {1,0,2,3,6,7,5,4,7,3,4,0,5,1,6,2};
    glColor3f(color.r,color.g,color.b);
    glBegin(GL_QUAD_STRIP);
        for(int i=0; i<8; ++i)
            glVertex3f(vertices[(indice[i])].x,vertices[(indice[i])].y,vertices[(indice[i])].z);
    glEnd();
    glBegin(GL_QUAD_STRIP);
        for(int i=0; i<8; ++i)
            glVertex3f(vertices[(indice[i+8])].x,vertices[(indice[i+8])].y,vertices[(indice[i+8])].z);
    glEnd();
}

float CuboG::getLado()
{
    return lado;
}

void CuboG::setLado(float l)
{
    if(l > 0){
        lado = l;
        creaGeometria();
    }
}

void CuboG::creaGeometria()
{
	float medio_lado = lado/2;
    vertices.clear();
    Punto3D p;
    // Abajo
    p.y = -medio_lado;
    //
    p.x = medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
    p.x = medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
    // Arriba
    p.y = medio_lado;
    //
    p.x = medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
    p.x = medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = -medio_lado;
    vertices.push_back(p);
    p.x = -medio_lado;
    p.z = medio_lado;
    vertices.push_back(p);
}

