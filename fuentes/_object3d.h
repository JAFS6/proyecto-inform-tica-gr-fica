#ifndef _OBJECT3D_H
#define _OBJECT3D_H

#include "auxiliar.h"

class _Object3D
{
	protected:
		vector<Punto3D> vertices; // V�rtices del objeto
		vector<Cara> caras; // Caras del objeto
		vector<Punto3D> normales_vertices; // Normales de los v�rtices
		vector<Punto3D> normales_caras; // Normales de las caras
		Color reflexion_ambiental; // GL_AMBIENT
		Color reflexion_difusa;    // GL_DIFFUSE Color a usar cuando no haya iluminaci�n
		Color reflexion_especular; // GL_SPECULAR
		float exponente_especular; // GL_SHININESS
		float tam_x; // Tama�o de la caja englobante en el eje X
		float tam_y; // Tama�o de la caja englobante en el eje Y
		float tam_z; // Tama�o de la caja englobante en el eje Z
		float radio_esfera_englobante; // Radio de la esfera englobante
		
	public:
		_Object3D();
		
		/**
			@brief Dibuja el objeto
			@param[in] iluminaci�n Modo de iluminaci�n: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el objeto en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
			@brief Obtiene el tama�o de la caja englobante en el eje X
			@return El tama�o obtenido
		*/
		float getTamX();
		
		/**
			@brief Obtiene el tama�o de la caja englobante en el eje Y
			@return El tama�o obtenido
		*/
		float getTamY();
		
		/**
			@brief Obtiene el tama�o de la caja englobante en el eje Z
			@return El tama�o obtenido
		*/
		float getTamZ();
		
		/**
			@brief Obtiene el radio de la esfera englobante
			@return El radio obtenido
		*/
		float getRadioEsferaEnglobante();
		
		/**
			@brief Configura la reflexi�n ambiental
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setAmbient  (float r,float g,float b);
		
		/**
			@brief Configura la reflexi�n difusa
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setDiffuse  (float r,float g,float b);
		
		/**
			@brief Configura la reflexi�n especular
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setSpecular (float r,float g,float b);
		
		/**
			@brief Configura el exponente especular
			@param[in] s Exponente especular
		*/
		void setShininess(float s);
		
		/**
			@brief Obtiene los valores de la reflexi�n ambiental
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getAmbient  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexi�n difusa
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getDiffuse  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexi�n especular
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getSpecular (float &r,float &g,float &b);
		
		/**
			@brief Obtiene el valor del exponente especular
			@param[out] s Exponente especular
		*/
		void getShininess(float &s);
		
	protected:
		/**
			@brief Crea la geometr�a del objeto. Debe implementarse en todas las subclases
		*/
		virtual void creaGeometria() = 0;
		
		/**
			@brief Crea la topolog�a del objeto. Debe implementarse en todas las subclases
		*/
		virtual void creaTopologia() = 0;
		
		/**
			@brief Calcula las normales de las caras
		*/
		void calcularNormalesCaras();
		
		/**
			@brief Calcula las normales de los vertices
		*/
		void calcularNormalesVertices();
		
		/**
			@brief C�digo para dar caras sin normales
			@param[in] i �ndice de la cara
		*/
		inline void darCarasSinIluminado(int i);
		
		/**
			@brief C�digo para dar caras con normales de caras
			@param[in] i �ndice de la cara
		*/
		inline void darCarasIluminadoPlano(int i);
		
		/**
			@brief C�digo para dar caras con normales de vertices
			@param[in] i �ndice de la cara
		*/
		inline void darCarasIluminadoSuave(int i);
};

#endif // _OBJECT3D_H

