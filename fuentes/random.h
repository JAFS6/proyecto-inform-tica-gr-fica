//**************************************************************************
// Implementación de una clase que calcula valores aleatorios entre dos
// valores extremos
//
// Domingo Martin Perandres 2004
//
// GPL
//**************************************************************************

#ifndef _RANDOM
#define _RANDOM

#include <sys/time.h>

class _random
{
	public:
		  _random(){ seed();};
		  _random(float Minimum1,float Maximum1){seed();Minimum=Minimum1;Maximum=Maximum1;Factor=(Maximum-Minimum)/(float) RAND_MAX;};
		void init(float Minimum1,float Maximum1){seed();Minimum=Minimum1;Maximum=Maximum1;Factor=(Maximum-Minimum)/(float) RAND_MAX;};
		float value(){return(Factor*rand()+Minimum);};
		void seed(){
		struct timeval tv; gettimeofday(&tv,NULL);srand(tv.tv_usec);
		};

	private:
		float Minimum,Maximum;
		float Factor;
};




#endif
