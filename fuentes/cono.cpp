#include "cono.h"

Cono::Cono()
{
	reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
	altura = 1;
    radio = 0.5;
    num_div = 36;
    creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

Cono::Cono(float h,float rad,unsigned short d)
{
	reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
	if(h <= 0) altura = 1;
	else       altura = h;
	if(rad <= 0) radio = 1;
	else       radio = rad;
	if(d <= 3) num_div = 3;
	else       num_div = d;
	creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

float Cono::getAltura()
{
	return altura;
}

void Cono::setAltura(float h)
{
	if(h > 0){
		altura = h;
		creaGeometria();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

float Cono::getRadio()
{
	return radio;
}

void Cono::setRadio(float r)
{
	if(r > 0){
		radio = r;
		creaGeometria();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

unsigned short Cono::getNumDiv()
{
	return num_div;
}

void Cono::setNumDiv(unsigned short d)
{
	if(d >= 3){
		num_div = d;
		creaGeometria();
		creaTopologia();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

void Cono::creaGeometria()
{
	vertices.clear();
    Punto3D p;
	// V�rtice superior
    p.x = 0;
    p.y = altura/2;
    p.z = 0;
    vertices.push_back(p);

    float angulo = (2*PI)/num_div;
	p.y = -p.y; // Todos los puntos de la base tienen esta altura

	for(int i=0; i<num_div; ++i){
		p.x =  cos(angulo * i) * radio;
		p.z = -sin(angulo * i) * radio;
		vertices.push_back(p);
	}

    // V�rtice central de la base
    p.x = 0;
    p.z = 0;
    vertices.push_back(p);
}

void Cono::creaTopologia()
{
	caras.clear();
	Cara c;
	// Parte superior
    c.v1 = 0; // V�rtice com�n a todas las caras de la parte superior
    for(int i=1; i<num_div; ++i){
        c.v2 = i;
        c.v3 = i+1;
        caras.push_back(c);
    }
    c.v2 = num_div;
    c.v3 = 1;
    caras.push_back(c);
    // Parte inferior (base)
    c.v3 = vertices.size()-1; // V�rtice com�n a todas las caras de la parte inferior
    int n = c.v3-1;
    for(int i=c.v3 - num_div; i<n; ++i){
        c.v2 = i;
        c.v1 = i+1;
        caras.push_back(c);
    }
    c.v2 = num_div;
    c.v1 = 1;
    caras.push_back(c);
}

