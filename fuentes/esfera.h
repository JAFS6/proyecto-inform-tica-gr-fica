#ifndef ESFERA_H
#define ESFERA_H

#include "_object3d.h"
#include "auxiliar.h"

class Esfera : public _Object3D
{
	private:
		float radio;
		unsigned short num_div_hor;
		unsigned short num_div_vert;

	public:
		/**
          @brief Crea una esfera plateada de radio 0.5 con 36 divisiones de resoluci�n horizontales y verticales
        */
		Esfera();
		
		/**
          @brief Crea una esfera plateada de radio r, con dh divisiones de resoluci�n horizontales y dv divisiones de resoluci�n verticales
          @param[in] r El radio de la esfera
          @param[in] dh El n�mero de divisiones de resoluci�n horizontales de la esfera
          @param[in] dv El n�mero de divisiones de resoluci�n verticales de la esfera
        */
        Esfera(float r,unsigned short dh,unsigned short dv);

		/**
		  @brief Obtiene el radio de la esfera
		  @return El radio de la esfera
		*/
		float getRadio();
		
		/**
		  @brief Establece el radio de la esfera
		  @param[in] r El nuevo radio de la esfera
		*/
		void setRadio(float r);
		
		/**
		  @brief Obtiene el n�mero de divisiones de resoluci�n horizontales de la esfera
		  @return El n�mero de divisiones de resoluci�n horizontales de la esfera
		*/
		unsigned short getNumDivHor();
		
		/**
		  @brief Establece el n�mero de divisiones de resoluci�n horizontales de la esfera
		  @param[in] d El nuevo n�mero de divisiones de resoluci�n horizontales de la esfera
		*/
		void setNumDivHor(unsigned short d);
		
		/**
		  @brief Obtiene el n�mero de divisiones de resoluci�n verticales de la esfera
		  @return El n�mero de divisiones de resoluci�n verticales de la esfera
		*/
		unsigned short getNumDivVert();
		
		/**
		  @brief Establece el n�mero de divisiones de resoluci�n verticales de la esfera
		  @param[in] d El nuevo n�mero de divisiones de resoluci�n verticales de la esfera
		*/
		void setNumDivVert(unsigned short d);
		
		/**
		  @brief Establece el n�mero de divisiones de resoluci�n verticales y horizontales de la esfera
		  @param[in] v El nuevo n�mero de divisiones de resoluci�n verticales de la esfera
		  @param[in] h El nuevo n�mero de divisiones de resoluci�n horizontales de la esfera
		*/
		void setNumDiv(unsigned short v,unsigned short h);

	private:
		/**
		@brief Crea la geometr�a de la esfera
		*/
		void creaGeometria();

		/**
		@brief Crea la topolog�a de la esfera
		*/
		void creaTopologia();
};

#endif // ESFERA_H

