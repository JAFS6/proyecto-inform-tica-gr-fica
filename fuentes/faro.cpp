#include "faro.h"

Faro::Faro()
{
	profundidad = 1;
	diametro = 1;
	foco.setAmbient(0.0,0.0,0.0);
	foco.setDiffuse(0.5,0.5,0.0);
	foco.setSpecular(0.60,0.60,0.50);
	foco.setShininess(32);
}

Faro::Faro(float p,float d)
{
	profundidad = p;
	diametro = d;
	foco.setAmbient(0.0,0.0,0.0);
	foco.setDiffuse(0.5,0.5,0.0);
	foco.setSpecular(0.60,0.60,0.50);
	foco.setShininess(32);
}

void Faro::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	glPushMatrix();
		glRotatef(-90,1,0,0);
		glScalef(diametro,profundidad,diametro);
		foco.dibuja(iluminacion,ajedrez);
	glPopMatrix();
}

float Faro::getProfundidad()
{
	return profundidad;
}

void Faro::setProfundidad(float p)
{
	profundidad = p;
}

float Faro::getDiametro()
{
	return diametro;
}

void Faro::setDiametro(float d)
{
	diametro = d;
}

void Faro::setAmbient (float r,float g,float b)
{
	foco.setAmbient(r,g,b);
}

void Faro::setDiffuse (float r,float g,float b)
{
	foco.setDiffuse(r,g,b);
}

void Faro::setSpecular(float r,float g,float b)
{
	foco.setSpecular(r,g,b);
}

void Faro::setShininess(float s)
{
	foco.setShininess(s);
}

void Faro::getAmbient (float &r,float &g,float &b)
{
	foco.getAmbient(r,g,b);
}

void Faro::getDiffuse (float &r,float &g,float &b)
{
	foco.getDiffuse(r,g,b);
}

void Faro::getSpecular(float &r,float &g,float &b)
{
	foco.getSpecular(r,g,b);
}

void Faro::getShininess(float &s)
{
	foco.getShininess(s);
}


