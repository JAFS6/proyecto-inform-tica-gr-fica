#include "chasis.h"

Chasis::Chasis()
{
	ancho = 6;
	largo = 10;
	alto = 3;
	chasis.setAmbient(0.0,0.0,0.0);
	chasis.setDiffuse(0.5,0.0,0.0);
	chasis.setSpecular(0.7,0.6,0.6);
	chasis.setShininess(32);
}

Chasis::Chasis(float anch,float larg,float alt)
{
	ancho = anch;
	largo = larg;
	alto = alt;
	chasis.setAmbient(0.0,0.0,0.0);
	chasis.setDiffuse(0.5,0.0,0.0);
	chasis.setSpecular(0.7,0.6,0.6);
	chasis.setShininess(32);
}

void Chasis::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	glPushMatrix();
		glScalef(ancho,alto,largo);
		chasis.dibuja(iluminacion,ajedrez);
	glPopMatrix();
}

float Chasis::getAncho()
{
	return ancho;
}

void Chasis::setAncho(float a)
{
	if(a > 0)
		ancho = a;
}

float Chasis::getLargo()
{
	return largo;
}

void Chasis::setLargo(float l)
{
	if(l > 0)
		largo = l;
}

float Chasis::getAlto()
{
	return alto;
}

void Chasis::setAlto(float a)
{
	if(a > 0)
		alto = a;
}

void Chasis::setAmbient (float r,float g,float b)
{
	chasis.setAmbient(r,g,b);
}

void Chasis::setDiffuse (float r,float g,float b)
{
	chasis.setDiffuse(r,g,b);
}

void Chasis::setSpecular(float r,float g,float b)
{
	chasis.setSpecular(r,g,b);
}

void Chasis::setShininess(float s)
{
	chasis.setShininess(s);
}

void Chasis::getAmbient (float &r,float &g,float &b)
{
	chasis.getAmbient(r,g,b);
}

void Chasis::getDiffuse (float &r,float &g,float &b)
{
	chasis.getDiffuse(r,g,b);
}

void Chasis::getSpecular(float &r,float &g,float &b)
{
	chasis.getSpecular(r,g,b);
}

void Chasis::getShininess(float &s)
{
	chasis.getShininess(s);
}


