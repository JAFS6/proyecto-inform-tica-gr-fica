#include "conog.h"

ConoG::ConoG()
{
    altura = 1;
    radio = 0.5;
    num_divisiones = 36;
    creaGeometria();
}

void ConoG::dibuja()
{
    int n = num_divisiones+1;
    glColor3f(color.r,color.g,color.b);
    glBegin(GL_TRIANGLE_FAN); // Parte superior
        for(int i=0; i<n; ++i)
            glVertex3f(vertices[i].x,vertices[i].y,vertices[i].z);

        glVertex3f(vertices[1].x,vertices[1].y,vertices[1].z);
    glEnd();
    glBegin(GL_TRIANGLE_FAN); // Base
        for(int i=vertices.size()-1; i>=1; --i)
            glVertex3f(vertices[i].x,vertices[i].y,vertices[i].z);

        n = vertices.size()-2;
        glVertex3f(vertices[n].x,vertices[n].y,vertices[n].z);
    glEnd();
}

float ConoG::getAltura()
{
    return altura;
}

void ConoG::setAltura(float a)
{
    if(a > 0){
        altura = a;
        creaGeometria();
    }
}

float ConoG::getRadio()
{
    return radio;
}

void ConoG::setRadio(float r)
{
    if(r > 0){
        radio = r;
        creaGeometria();
    }
}

float ConoG::getNumDiv()
{
    return num_divisiones;
}

void ConoG::setNumDiv(float nd)
{
    if(nd >= 3){
        num_divisiones = nd;
        creaGeometria();
    }
}

void ConoG::creaGeometria()
{
    vertices.clear();
    Punto3D p;
    // V�rtice superior
    p.x = 0;
    p.y = altura/2.0;
    p.z = 0;
    vertices.push_back(p);

    p.y = -(altura/2.0); // Para toda la base igual

    float angulo = (2*PI)/num_divisiones;

    for(int i=0; i<num_divisiones; ++i){
        p.x = cos(angulo*i)*radio;
        p.z = -sin(angulo*i)*radio;
        vertices.push_back(p);
    }

    // Centro de la base
    p.x = 0;
    p.z = 0;
    vertices.push_back(p);
}
