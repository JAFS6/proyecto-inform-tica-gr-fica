#ifndef _CASILLA_H_
#define _CASILLA_H_

#include "_object3d.h"

class Casilla : public _Object3D
{
	private:
		float lado;
		float grosor;
		
	public:
		/**
			@brief Crea una casilla plateada de lado 1 y grosor 0.1
		*/
		Casilla();
		
        /**
          @brief Crea una casilla plateada de lado l y grosor g
          @param[in] l El tamaño del lado de la casilla
          @param[in] g El grosor de la casilla
        */
        Casilla(float l,float g);
        
        /**
          @brief Obtiene la medida del lado de la casilla
          @return La medida del lado
        */
        float getLado();
        
        /**
          @brief Asigna una nueva medida al lado de la casilla y recalcula la geometría
          @param[in] l La nueva medida del lado
        */
        void setLado(float l);
        
        /**
          @brief Obtiene la medida del grosor de la casilla
          @return La medida del grosor
        */
        float getGrosor();
        
        /**
          @brief Asigna una nueva medida al grosor de la casilla y recalcula la geometría
          @param[in] g La nueva medida del grosor
        */
        void setGrosor(float g);
		
	private:
        /**
          @brief Crea la geometría de la casilla
        */
        void creaGeometria();
        
        /**
          @brief Crea la topología de la casilla
        */
        void creaTopologia();
};

#endif // _CASILLA_H_


