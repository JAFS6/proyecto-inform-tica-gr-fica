#ifndef FUNCIONESAUXILIARES_H
#define FUNCIONESAUXILIARES_H

#include <GL/glu.h>
#include <GL/gl.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <utility>
#include "lodepng.h"

using namespace std;

/***************************************************
***************** CONSTANTES ***********************
***************************************************/

#define PI 3.141592 // Constante matem�tica PI
#define NUM_LUCES 5
#define NUM_CAMARAS 8
#define INDICE_CAMARA_OVEJAS 7
#define NUM_MIN_OVEJAS 25
#define ESCALADO_OVEJA_ADULTA 1.5
#define NUM_ARBOLES 8

// TIEMPOS

#define T_VIDA 15000
#define T_REC_FERTIL 10000

#define T_GASTO_ENERGIA 6000
#define T_GASTO_AGUA 6000
#define T_GASTO_COMIDA 6000
#define T_CRECER_LANA 6000

#define T_REC_AGUA 250
#define T_REC_COMIDA 250
#define T_REC_ENERGIA 2500
#define T_CORTAR_LANA 300

#define INI_CONTADORES_MAX 6000

// ZONAS

#define E1X_PASTO -35
#define E1Y_PASTO -97
#define E2X_PASTO 97
#define E2Y_PASTO 97

#define E1X_ANTEAGUA -58
#define E1Y_ANTEAGUA -80
#define E2X_ANTEAGUA -35
#define E2Y_ANTEAGUA -55

#define E1X_AGUA -59
#define E1Y_AGUA -80
#define E2X_AGUA -58
#define E2Y_AGUA -55

#define E1X_ANTECOMIDA -62.3
#define E1Y_ANTECOMIDA -18
#define E2X_ANTECOMIDA -35
#define E2Y_ANTECOMIDA -2

#define E1X_COMIDA -62.7
#define E1Y_COMIDA -18
#define E2X_COMIDA -62.3
#define E2Y_COMIDA -2

#define E1X_ANTEDORMIR -66
#define E1Y_ANTEDORMIR 7
#define E2X_ANTEDORMIR -35
#define E2Y_ANTEDORMIR 63

#define E1X_DORMIR -84
#define E1Y_DORMIR 7
#define E2X_DORMIR -66
#define E2Y_DORMIR 63

#define E1X_ANTEESQUILAR -67
#define E1Y_ANTEESQUILAR 77
#define E2X_ANTEESQUILAR -35
#define E2Y_ANTEESQUILAR 93

#define E1X_ESQUILAR -75
#define E1Y_ESQUILAR 77
#define E2X_ESQUILAR -67
#define E2Y_ESQUILAR 93

typedef unsigned char BYTE;   // 8-bit unsigned entity.

/***************************************************
***************** ESTRUCTURAS **********************
***************************************************/

struct Punto3D{
    float x;
    float y;
    float z;
};

struct Punto2D{
    float x;
    float z;
};

struct Cara{
    int v1;
    int v2;
    int v3;
};

struct Color{
    float r;
    float g;
    float b;
};
/*
struct Info_Colision{
	int id;
	Punto3D pos;
	float radio;
	//_O3D_Modelo_Animado * objeto;
};*/

/***************************************************
***************** ENUMERADOS ***********************
***************************************************/

enum modo_vis {puntos,lineas,ajedrez,solido};

enum modo_ilu {ninguno,plano,gouraud};

enum modelo_elegido {COCHE_MALO,COCHE_BUENO,BEETHOVEN,ESFERA};

enum estado_objeto {idle,girando,avanzando,buscandoObjetivo};

enum tipo_velocidad {lineal,senoidal};

enum animacion_estado {activada,pausada,detenida};

enum tipo_accion {pastar,dormir,comer,beber,esquilar};

enum tipo_zona {zona_pasto,ante_agua,zona_agua,ante_comida,zona_comida,ante_dormir,zona_dormir,ante_esquilar,zona_esquilar,zona_erronea};

/**************************************************************
***************** FUNCIONES DE ORDENACI�N ***********************
***************************************************************/

/**
	@brief Ordena de menor a mayor el vector v seg�n el segundo par�metro del par
	@param[in] v El vector a ordenar
	@param[in] util El n�mero de componentes del vector
*/
void ordenacionInsercionParIntFloat(pair< int,float > * v, int util);

/**************************************************************
***************** FUNCIONES MATEM�TICAS ***********************
***************************************************************/

/**
    @brief Calcula la distancia eucl�dea entre 2 puntos del plano
    @param[in] x1 Coordenada x del punto 1
    @param[in] y1 Coordenada y del punto 1
    @param[in] x2 Coordenada x del punto 2
    @param[in] y2 Coordenada y del punto 2
    @return La distancia entre los 2 puntos del plano
  */
float distanciaEuclidea2D(float x1, float y1, float x2, float y2);

/**
    @brief Normaliza un �ngulo en grados en el intervalo ]-360,360[
    @param[in] angulo El �ngulo a normalizar
    @return El �ngulo normalizado
  */
float normaliza_angulo(float angulo);

/**
    @brief Calcula el valor en radianes del angulo (en grados) introducido
    @param[in] angulo El �ngulo a transformar
    @return El valor calculado
  */
float grados2radianes(float angulo);

/**
    @brief Calcula el valor en grados del angulo (en radianes) introducido
    @param[in] angulo El �ngulo a transformar
    @return El valor calculado
  */
float radianes2grados(float angulo);

/**
	@brief Normaliza el vector x,y,z
	@param[in,out] x Componente x del vector
	@param[in,out] y Componente y del vector
	@param[in,out] z Componente z del vector
*/
void normalizaVector(float &x,float &y,float &z);

/**
	@brief Normaliza el vector v
	@param[in,out] v El vector
*/
void normalizaVector(Punto3D &v);

/**
	@brief Calcula el m�dulo del vector x,y,z
	@param[in] x Componente x del vector
	@param[in] y Componente y del vector
	@param[in] z Componente z del vector
	@return El m�dulo del vector x,y,z
*/
float moduloVector(float x,float y,float z);

/**
	@brief Calcula el m�dulo del vector v
	@param[in] v El vector
	@return El m�dulo del vector v
*/
float moduloVector(Punto3D v);

/**
	@brief Calcula el producto vectorial a x b
	@param[in] a Vector a
	@param[in] b Vector b
	@return El vector soluci�n
*/
Punto3D productoVectorial(Punto3D a,Punto3D b);

/**
	@brief Calcula el producto escalar a � b
	@param[in] a Vector a
	@param[in] b Vector b
	@return El producto escalar
*/
float productoEscalar(Punto3D a,Punto3D b);

/**
	@brief Calcula el vector resultante de rotar respecto al eje X un �ngulo ang el vector v
	@param[in] ang �ngulo a rotar en radianes
	@param[in] v El vector a rotar
	@return El vector soluci�n
*/
Punto3D rotX(float ang,Punto3D v);

/**
	@brief Calcula el vector resultante de rotar respecto al eje Y un �ngulo ang el vector v
	@param[in] ang �ngulo a rotar en radianes
	@param[in] v El vector a rotar
	@return El vector soluci�n
*/
Punto3D rotY(float ang,Punto3D v);

/**
	@brief Calcula el vector resultante de rotar respecto al eje Z un �ngulo ang el vector v
	@param[in] ang �ngulo a rotar en radianes
	@param[in] v El vector a rotar
	@return El vector soluci�n
*/
Punto3D rotZ(float ang,Punto3D v);

/**
	@brief Redondea al entero m�s cercano
	@param[in] n El n�mero real a aproximar
	@return El entero m�s cercano a n
*/
int rendondea(float n);

/**
	@brief Calcula el m�nimo �ngulo (en grados) que forman 2 vectores 1 y 2 cuyo origen est� en el 0,0
	@param[in] X1 Componente X del vector 1
	@param[in] Z1 Componente Z del vector 1
	@param[in] X2 Componente X del vector 2
	@param[in] Z2 Componente Z del vector 2
	@return El �ngulo calculado
*/
float angulo2Vectores(float X1, float Z1, float X2, float Z2);

/**************************************************************
***************** FUNCIONES DE TEXTURAS ***********************
***************************************************************/

/**
	@brief Carga una textura en la memoria de la GPU
	@param[in] filename Nombre del archivo de textura
	@return El identificador de la textura cargada, o -1 si hubo error
*/
int CargaTextura( const char * filename);

/**
	@brief Carga una textura en la memoria de la GPU
	@param[in] filename Nombre del archivo de textura
	@param[out] width Ancho de la imagen
	@param[out] height Alto de la imagen
	@return El identificador de la textura cargada, o -1 si hubo error
*/
int CargaTextura( const char * filename,unsigned & width, unsigned & height);

/***********************************************************
***************** FUNCIONES DE AYUDA ***********************
************************************************************/

/**
	@brief Comprueba si el punto Xpunto,Zpunto est� a la derecha del vector origen->destino
	@param[in] Xorigen La componente X del punto de origen del vector
	@param[in] Zorigen La componente Z del punto de origen del vector
	@param[in] Xdestino La componente X del punto de destino del vector
	@param[in] Zdestino La componente Z del punto de destino del vector
	@param[in] Xpunto La componente X del punto a comprobar
	@param[in] Zpunto La componente Z del punto a comprobar
	@return True si el punto Xpunto,Zpunto est� a la derecha del vector origen->destino, false en caso contrario
*/
bool aLaDerecha(float Xorigen, float Zorigen, float Xdestino, float Zdestino, float Xpunto, float Zpunto);

/**
	@brief Comprueba si un punto x,z se encuetra dentro de un cuadrado dados su esquina superior izquierda y su esquina inferior derecha
	@param[in] x Coordenada x del punto a comprobar
	@param[in] z Coordenada z del punto a comprobar
	@param[in] e1x Coordenada x de la esquina superior izquierda del cuadrado
	@param[in] e1z Coordenada z de la esquina superior izquierda del cuadrado
	@param[in] e2x Coordenada x de la esquina inferior derecha del cuadrado
	@param[in] e2z Coordenada z de la esquina inferior derecha del cuadrado
	@return True si el punto x,z est� dentro del cuadrado (incluidas las aristas), false en caso contrario
	@pre e1x < e2x, e1z < e2z
*/
bool dentroCuadrado(float x, float z, float e1x, float e1z, float e2x, float e2z);

/**
    @brief Imprime en consola la ayuda del programa
*/
void imprimirAyuda();

#endif // FUNCIONESAUXILIARES_H

