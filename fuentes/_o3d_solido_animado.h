#ifndef _O3D_SOLIDO_ANIMADO_H_
#define _O3D_SOLIDO_ANIMADO_H_

#include "auxiliar.h"
#include "_object3d.h"
#include "_o3d_animado.h"

class _O3D_Solido_Animado : public _O3D_Animado
{
	private:
		_Object3D * objeto;
		Color reflexion_ambiental; // GL_AMBIENT
		Color reflexion_difusa;    // GL_DIFFUSE Color a usar cuando no haya iluminación
		Color reflexion_especular; // GL_SPECULAR
		float exponente_especular; // GL_SHININESS
		
	public:
		/**
			@brief Crea un controlador de objeto sólido animado por defecto asignándole velocidad lineal igual a 1, con estado idle, posición en el origen de coordenadas, y orientación hacia el lado positivo del eje Z
		*/
		_O3D_Solido_Animado();
		
		/**
			@brief Crea un controlador de objeto sólido animado para el objeto indicado, asignándole por defecto velocidad lineal igual a 1, con estado idle, posición en el origen de coordenadas, y orientación hacia el lado positivo del eje Z
		*/
		_O3D_Solido_Animado(_Object3D * o);
		
		/**
			@brief Dibuja el objeto animado
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el objeto en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
			@brief Establece el objeto que se animará
			@param[in] o El objeto
		*/
		void setObjeto(_Object3D * o);
		
		/**
			@brief Obtiene el radio de la esfera englobante
			@return El radio obtenido
		*/
		float getRadioEsferaEnglobante();
		
		/**
			@brief Configura la reflexión ambiental
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setAmbient  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión difusa
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setDiffuse  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión especular
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setSpecular (float r,float g,float b);
		
		/**
			@brief Configura el exponente especular
			@param[in] s Exponente especular
		*/
		void setShininess(float s);
		
		/**
			@brief Obtiene los valores de la reflexión ambiental
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getAmbient  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión difusa
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getDiffuse  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión especular
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getSpecular (float &r,float &g,float &b);
		
		/**
			@brief Obtiene el valor del exponente especular
			@param[out] s Exponente especular
		*/
		void getShininess(float &s);
};

#endif // _O3D_SOLIDO_ANIMADO_H

