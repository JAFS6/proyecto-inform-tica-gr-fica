#include "cilindro.h"

Cilindro::Cilindro()
{
	reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
	altura = 1;
    radio = 0.5;
    num_div = 36;
    creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

Cilindro::Cilindro(float h,float rad,unsigned short d)
{
	reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
	if(h <= 0) altura = 1;
	else       altura = h;
	if(rad <= 0) radio = 1;
	else       radio = rad;
	if(d <= 3) num_div = 3;
	else       num_div = d;
	creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

float Cilindro::getAltura()
{
	return altura;
}

void Cilindro::setAltura(float h)
{
	if(h > 0){
		altura = h;
		creaGeometria();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

float Cilindro::getRadio()
{
	return radio;
}

void Cilindro::setRadio(float r)
{
	if(r > 0){
		radio = r;
		creaGeometria();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

unsigned short Cilindro::getNumDiv()
{
	return num_div;
}

void Cilindro::setNumDiv(unsigned short d)
{
	if(d >= 3){
		num_div = d;
		creaGeometria();
		creaTopologia();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

void Cilindro::creaGeometria()
{
	vertices.clear();
    Punto3D p;
	// V�rtice central de la base superior
    p.x = 0;
    p.y = altura/2.0; // Altura de los v�rtices de la base superior
    p.z = 0;
    vertices.push_back(p);

    float angulo = (2*PI)/num_div;
	
	for(int i=0; i<num_div; ++i){
		p.x =  cos(angulo * i) * radio;
		p.z = -sin(angulo * i) * radio;
		vertices.push_back(p);
	}

	p.y = -p.y; // Altura de los v�rtices de la base inferior
	
	for(int i=1; i<=num_div; ++i){
		p.x = vertices[i].x;
		p.z = vertices[i].z;
		vertices.push_back(p);
	}
	
    // V�rtice central de la base inferior
    p.x = 0;
    p.z = 0;
    vertices.push_back(p);
}

void Cilindro::creaTopologia()
{
	caras.clear();
	Cara c;
	// Base superior
    c.v1 = 0; // V�rtice com�n a todas las caras de la base superior
    for(int i=1; i<num_div; ++i){
        c.v2 = i;
        c.v3 = i+1;
        caras.push_back(c);
    }
    c.v2 = num_div;
    c.v3 = 1;
    caras.push_back(c);
	// Base inferior
    c.v3 = vertices.size()-1; // V�rtice com�n a todas las caras de la base inferior
    int n = c.v3-1;
    for(int i=c.v3 - num_div; i<n; ++i){
        c.v2 = i;
        c.v1 = i+1;
        caras.push_back(c);
    }
    c.v2 = c.v3-1;
    c.v1 = c.v3 - num_div;
    caras.push_back(c);
	// Tira lateral
	for(int i=1; i<num_div; ++i){
		c.v1 = i+1;
		c.v2 = i;
		c.v3 = i+num_div;
		caras.push_back(c);
		
		c.v1 = i+1+num_div;
		c.v2 = i+1;
		//c.v3 = i+num_div;
		caras.push_back(c);
	}
	c.v1 = 1;
	c.v2 = num_div;
	c.v3 = 2*num_div;
	caras.push_back(c);
	
	c.v1 = 1+num_div;
	c.v2 = 1;
	//c.v3 = 2*num_div;
	caras.push_back(c);
}

