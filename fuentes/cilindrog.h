#ifndef CILINDROG_H
#define CILINDROG_H

#include "_object3dg.h"

class CilindroG : public _Object3DG
{
private:
    float altura;
    float radio;
    short int num_divisiones;
public:
    CilindroG();
    void dibuja();
    float getAltura();
    void setAltura(float a);
    float getRadio();
    void setRadio(float r);
    float getNumDiv();
    void setNumDiv(float nd);
private:
    void creaGeometria(); /* Almacenamiento de v�rtices
                            V�rtice central de la base inferior
                            V�rtices de la base inferior en sentido antihorario visto desde el semieje positivo Y
                            V�rtice central de la base superior
                            V�rtices de la base superior en sentido antihorario visto desde el semieje positivo Y
                            */
};

#endif // CILINDROG_H
