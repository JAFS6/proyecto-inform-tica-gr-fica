#ifndef RUEDA_H
#define RUEDA_H

#include "cilindro.h"

class Rueda
{
	private:
		Cilindro goma;
		float ancho;
		float diametro;
		
	public:
		/**
			@brief Crea una rueda de plastico azul de ancho 1 y diámetro 2
        */
		Rueda();
		
		/**
			@brief Crea una rueda de plastico azul de ancho a y diámetro d
			@param[in] a El ancho de la rueda
			@param[in] d El diámetro de la rueda
        */
		Rueda(float a,float d);
		
		/**
			@brief Dibuja la rueda en posición vertical centrada en los ejes X,Y,Z
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar la rueda en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
		  @brief Obtiene el ancho de la rueda
		  @return El ancho de la rueda
		*/
		float getAncho();
		
		/**
		  @brief Establece el ancho de la rueda
		  @param[in] a El nuevo ancho de la rueda
		*/
		void setAncho(float a);
		
		/**
		  @brief Obtiene el diámetro de la rueda
		  @return El diámetro de la rueda
		*/
		float getDiametro();
		
		/**
		  @brief Establece el diámetro de la rueda
		  @param[in] d El nuevo diámetro de la rueda
		*/
		void setDiametro(float d);
		
		/**
			@brief Configura la reflexión ambiental
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setAmbient  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión difusa
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setDiffuse  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión especular
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setSpecular (float r,float g,float b);
		
		/**
			@brief Configura el exponente especular
			@param[in] s Exponente especular
		*/
		void setShininess(float s);
		
		/**
			@brief Obtiene los valores de la reflexión ambiental
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getAmbient  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión difusa
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getDiffuse  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión especular
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getSpecular (float &r,float &g,float &b);
		
		/**
			@brief Obtiene el valor del exponente especular
			@param[out] s Exponente especular
		*/
		void getShininess(float &s);
};

#endif // RUEDA_H

