#ifndef _O3D_ANIMADO_H
#define _O3D_ANIMADO_H

#include "auxiliar.h"
#include "_object3d.h"
#include "random.h"

class _O3D_Animado
{
	protected:
		// Controlador animación
		int id;
		
		Punto3D posicion_inicial;
		Punto3D posicion_actual;
		Punto3D posicion_final;
		Punto3D posicion_colision;
		
		Punto3D orientacion_modelo;
		
		Punto3D orientacion_inicial;
		Punto3D orientacion_actual;
		Punto3D orientacion_final;
		float angulo_necesario;
		
		float velocidad;
		float velocidad_angular;
		tipo_velocidad t_vel;
		int ticks_necesarios_avanzar;
		int ticks_necesarios_girar;
		int tick_actual;
		
		estado_objeto estado;
		estado_objeto estado_guardado;
		
		bool colisiono;
		Punto3D punto_colision; // Posición de la oveja con la que colisionó
		int num_turnos_colision;
		
		float angulo_inclinacion;
		
		_random aleatorio;
		
		// Visualizacion
		float escalado; // Escalado para poner el tamaño de la oveja adulta
		
		// Controlador vida
		tipo_accion accion;
		
		int t_gastar_agua;
		int t_recuperar_agua;
		int t_gastar_comida;
		int t_recuperar_comida;
		int t_gastar_energia;
		int t_recuperar_energia;
		int t_crecer_lana;
		int t_cortar_lana;
		
		bool necesita_agua;
		bool necesita_comida;
		bool necesita_energia;
		bool tiene_lana;
		
		bool reabasteciendo;
		
		bool bebiendo;
		bool comiendo;
		bool durmiendo;
		bool cortando;
		
		int contador_agua;
		int contador_comida;
		int contador_energia;
		int contador_lana;
		
		bool hembra; //True si es hembra, false si es macho
		bool fertil; // Solo aplicable a las hembras
		bool parir; // Solo aplicable a las hembras
		int contador_fertil;
		int t_fertil; // Tiempo que tardará en volver a ser fértil
		int esperanza_vida;
		int contador_vida;
		bool vivo;
		bool adulto;
		float rotacion;
		
	public:
		/**
			@brief Crea un controlador de objeto animado por defecto asignándole velocidad lineal igual a 1, con estado idle, posición en el origen de coordenadas, y orientación hacia el lado positivo del eje Z
		*/
		_O3D_Animado();
		
		/**
			@brief Establece el ID del objeto
			@param[in] i El nuevo ID
		*/
		void setID(int i);
		
		/**
			@brief Consulta el ID del objeto
			@return El ID del objeto
		*/
		int getID();
		
		/**
			@brief Inicia la animación, buscará un nuevo objetivo
		*/
		void iniciar();
		
		/**
			@brief Pausa la animación, mantendrá el objetivo actual
		*/
		void pausar();
		
		/**
			@brief Reanuda la animación, continuará con el objetivo actual
		*/
		void reanudar();
		
		/**
			@brief Detiene la animación, olvidará el objetivo actual
		*/
		void detener();
		
		/**
			@brief Actualiza la posición del objeto
		*/
		void actualizar(vector<_O3D_Animado *> &grupo);
		
		/**
			@brief Dibuja el objeto animado
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el objeto en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		virtual void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false) = 0;
		
		/**
			@brief Obtiene el radio de la esfera englobante
			@return El radio obtenido
		*/
		virtual float getRadioEsferaEnglobante()=0;
		
		virtual void hacerAdulto()=0;
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje X
			@return El tamaño obtenido
		*/
		virtual float getTamX()=0;
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje Y
			@return El tamaño obtenido
		*/
		virtual float getTamY()=0;
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje Z
			@return El tamaño obtenido
		*/
		virtual float getTamZ()=0;
		
		/**
			@brief Establece el factor de escalado a aplicar al modelo en los tres ejes
			@param[in] e El factor de escala
		*/
		void setEscalado(float e);
		
		/**
			@brief Consulta el factor de escalado
			@return El valor del factor de escalado
		*/
		float getEscalado();
		
		/**
			@brief Establece el tipo de velocidad
			@param[in] t Tipo de velocidad: lineal o senoidal
		*/
		void setTipoVel(tipo_velocidad t);
		
		/**
			@brief Establece la velocidad
			@param[in] v La nueva velocidad medida en unidades por tick
		*/
		void setVel(float v);
		
		/**
			@brief Consulta la velocidad
			@return La velocidad del objeto medida en unidades por tick
		*/
		float getVel();
		
		/**
			@brief Establece la velocidad angular
			@param[in] v La nueva velocidad medida en radianes por tick
		*/
		void setVelAng(float v);
		
		/**
			@brief Consulta la velocidad angular
			@return La velocidad angular del objeto medida en radianes por tick
		*/
		float getVelAng();
		
		/**
			@brief Establece la posición del objeto
			@param[in] x La coordenada x de la nueva posición del objeto
			@param[in] y La coordenada y de la nueva posición del objeto
			@param[in] z La coordenada z de la nueva posición del objeto
		*/
		void setPos(float x,float y,float z);
		
		/**
			@brief Consulta la coordenada x de la posición del objeto
			@return El valor de la coordenada x de la posición del objeto
		*/
		float posX();
		
		/**
			@brief Consulta la coordenada y de la posición del objeto
			@return El valor de la coordenada y de la posición del objeto
		*/
		float posY();
		
		/**
			@brief Consulta la coordenada z de la posición del objeto
			@return El valor de la coordenada z de la posición del objeto
		*/
		float posZ();
		
		float getRotacion();
		
		/**
			@brief Establece la orientación del modelo
			@param[in] x La componente x del vector orientación del modelo
			@param[in] y La componente y del vector orientación del modelo
			@param[in] z La componente z del vector orientación del modelo
		*/
		void setOrientacionModelo(float x,float y,float z);
		
		/**
			@brief Establece la orientación del objeto
			@param[in] x La componente x del vector orientación del objeto
			@param[in] y La componente y del vector orientación del objeto
			@param[in] z La componente z del vector orientación del objeto
		*/
		void setOrientacion(float x,float y,float z);
		
		/**
			@brief Obtiene la orientación del objeto
			@param[out] x La componente x del vector orientación del objeto
			@param[out] y La componente y del vector orientación del objeto
			@param[out] z La componente z del vector orientación del objeto
		*/
		void getOrientacion(float &x,float &y,float &z);
		
		/**
			@brief Calcula la rotación, en grados, necesaria para orientar el modelo en la dirección del objeto
		*/
		float orientacion();
		
		// Métodos que devuelven true si está dentro de la zona
		
		bool dentroPasto();
		bool dentroAnteAgua();
		bool dentroAgua();
		bool dentroAnteComida();
		bool dentroComida();
		bool dentroAnteDormir();
		bool dentroDormir();
		bool dentroAnteEsquilar();
		bool dentroEsquilar();
		
		bool estaVivo();
		
		bool esAdulto();
		
		bool esHembra();
		
		bool esFertil();
		
		bool vaAParir();
		
		void setHaParido();
		
		void setAdulto();
		
		int edad();
		
		int getEsperanzaVida();
		
		void setEdad(int e);
	
	private:
		/**
			@brief Permite el cambio de velocidad y tipo de velocidad
		*/
		void nuevaVel();
		
		/**
			@brief Selecciona un destino para el objeto
		*/
		void seleccionaDestino();
		
		/**
			@brief Selecciona la acción a realizar: pastar,dormir,comer,beber,esquilar
		*/
		void seleccionarAccion();
		
		void actualizarVida(vector<_O3D_Animado *> &grupo,int posicion_mia);
};

#endif // _O3D_ANIMADO_H

