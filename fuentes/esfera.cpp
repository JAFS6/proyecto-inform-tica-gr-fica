#include "esfera.h"

Esfera::Esfera()
{
	reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
    radio = 0.5;
    num_div_hor = 36;
    num_div_vert = 36;
    creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

Esfera::Esfera(float r,unsigned short dh,unsigned short dv)
{
	reflexion_ambiental.r = 0.19225;
	reflexion_ambiental.g = 0.19225;
	reflexion_ambiental.b = 0.19225;
	reflexion_difusa.r = 0.50754;
	reflexion_difusa.g = 0.50754;
	reflexion_difusa.b = 0.50754;
	reflexion_especular.r = 0.508273;
	reflexion_especular.g = 0.508273;
	reflexion_especular.b = 0.508273;
	exponente_especular = 51.2;
	if(r <= 0) radio = 0.5;
	else       radio = r;
	if(dh <= 3) num_div_hor = 3;
	else        num_div_hor = dh;
	if(dv <= 3) num_div_vert = 3;
	else        num_div_vert = dv;
	creaGeometria();
	creaTopologia();
	calcularNormalesCaras();
	calcularNormalesVertices();
}

float Esfera::getRadio()
{
	return radio;
}

void Esfera::setRadio(float r)
{
	if(r > 0){
		radio = r;
		creaGeometria();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

unsigned short Esfera::getNumDivHor()
{
	return num_div_hor;
}

void Esfera::setNumDivHor(unsigned short d)
{
	if(d >= 3){
		num_div_hor = d;
		creaGeometria();
		creaTopologia();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

unsigned short Esfera::getNumDivVert()
{
	return num_div_vert;
}

void Esfera::setNumDivVert(unsigned short d)
{
	if(d >= 3){
		num_div_vert = d;
		creaGeometria();
		creaTopologia();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

void Esfera::setNumDiv(unsigned short v,unsigned short h)
{
	bool hay_cambio = false;
	if(v >= 3){
		num_div_vert = v;
		hay_cambio = true;
	}
	if(h >= 3){
		num_div_hor = h;
		hay_cambio = true;
	}
	if(hay_cambio){
		creaGeometria();
		creaTopologia();
		calcularNormalesCaras();
		calcularNormalesVertices();
	}
}

void Esfera::creaGeometria()
{
	// Generatriz de la esfera z=0, y>=0
	Punto3D aux,aux2;
	vector<Punto3D> generatriz;
	generatriz.clear();
	
	aux.z = 0; // Todos los puntos de la generatriz tienen z = 0
	
	/*
		La generatriz será el arco superior, comenzando en x = radio, y acabando en x = -radio, y>=0, z=0
		Constará de num_div_hor+2 puntos
	*/
	
	// Primer punto
	aux.x = radio;
	aux.y = 0;
	generatriz.push_back(aux);
	
	float angulo = PI/(num_div_hor+1);
	
	for(int i=1; i<=num_div_hor; ++i){
		aux.x = cos(angulo * i) * radio;
		aux.y = sin(angulo * i) * radio;
		generatriz.push_back(aux);
	}
	
	// Último punto
	aux.x = -radio;
	aux.y = 0;
	generatriz.push_back(aux);
	
	// Girar la generatriz respecto del eje X
	vertices.clear();
	
	aux = generatriz[0];
	vertices.push_back(aux); // El primer vértice será el primero de la generatriz
	
	// Calcular los puntos intermedios
	int n = generatriz.size()-2; // Número de vértices de la generatriz menos el primero y el último
	angulo = (2.0*PI)/num_div_vert;
	float angulo_aux;
	
	for(int i=1; i<=n; ++i){ // Para cada vértice "interior" de la generatriz
		aux = generatriz[i];
		aux2.x = aux.x;
		for(int j=0; j<num_div_vert; ++j){
			angulo_aux = (angulo * j);
			aux2.y = aux.y * cos(angulo_aux) - aux.z * sin(angulo_aux);
			aux2.z = aux.y * sin(angulo_aux) + aux.z * cos(angulo_aux);
			vertices.push_back(aux2);
		}
	}
	
	aux = generatriz[generatriz.size()-1];
	vertices.push_back(aux); // El último vértice será el último de la generatriz
}

void Esfera::creaTopologia()
{
	caras.clear();
	Cara aux;
	// Primera tapa
	aux.v1 = 0;
	for(int i=1; i<num_div_vert; ++i){
        aux.v2 = i;
        aux.v3 = i+1;
        caras.push_back(aux);
    }
    aux.v2 = num_div_vert;
    aux.v3 = 1;
    caras.push_back(aux);
	
	// Cuerpo
	int n = num_div_vert*(num_div_hor-1);
	for(int i=1; i<=n; i += num_div_vert){
		int fin = i+num_div_vert-1;
		for(int j=i; j<fin; ++j){
			aux.v1 = j;
			aux.v2 = j+num_div_vert;
			aux.v3 = j+1;
			caras.push_back(aux);
			aux.v1 = aux.v3;
			// v2 se mantiene
			aux.v3 = aux.v1+num_div_vert;
			caras.push_back(aux);
		}
		aux.v1 = fin;
		aux.v2 = fin + num_div_vert;
		aux.v3 = i;
		caras.push_back(aux);
		aux.v1 = aux.v3;
		// v2 se mantiene
		aux.v3 = aux.v1+num_div_vert;
		caras.push_back(aux);
	}
	
	// Segunda tapa
	aux.v3 = vertices.size()-1;
   	n = aux.v3-1;
    for(int i=aux.v3 - num_div_vert; i<n; ++i){
        aux.v2 = i;
        aux.v1 = i+1;
        caras.push_back(aux);
    }
    aux.v2 = aux.v3-1;
    aux.v1 = aux.v3 - num_div_vert;
    caras.push_back(aux);
}

