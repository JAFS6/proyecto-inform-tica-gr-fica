#ifndef ESCENA_H
#define ESCENA_H

#include <QTimer>
#include "axis.h"
#include "auxiliar.h"
#include "coche.h"
#include "tetraedro.h"
#include "cubo.h"
#include "cono.h"
#include "cilindro.h"
#include "tetraedrog.h"
#include "cubog.h"
#include "conog.h"
#include "cilindrog.h"
#include "esfera.h"
#include "ply_object.h"
#include "luz.h"
#include "camara.h"
#include "tablero.h"
#include "_o3d_solido_animado.h"
#include "_o3d_modelo_animado.h"
#include "_o3d_animado.h"
#include "random.h"
#include "_impostor.h"
#include "oveja.h"
#include "oveja_sin.h"

class Escena
{
	private:
		Axis ejes;
		Coche coche;
		PLY_Object cubo_basura;
		PLY_Object balancin;
		Oveja oveja_modelo1;
		Oveja_Sin oveja_modelo2;
		vector<_O3D_Modelo_Animado *> oveja;
		vector<_O3D_Animado *> grupo;
		Luz luces[NUM_LUCES];
		bool interruptor[NUM_LUCES];
		Camara camara[NUM_CAMARAS];
		int camara_sobre_oveja;
		Tablero tablero;
		_Impostor cesped;
		_Impostor montana;
		_Impostor paja;
		_Impostor arbol;
		Punto3D translaciones_arboles[NUM_ARBOLES];
		Cubo cubo;
		Cilindro cilindro;
		Tetraedro tetraedro;
		Cono cono;
		CuboG cuboG;
		CilindroG cilindroG;
		TetraedroG tetraedroG;
		ConoG conoG;
		Esfera esfera;
		int ids;
		
		/*VARIABLES DE CONTROL DEL PROGRAMA*/
		bool mostrar_ejes;
		modo_vis modo; // Modo de visualizaci�n: puntos,lineas,ajedrez,solido
		modo_ilu iluminacion; // Modo de iluminaci�n: ninguna,plana,gouraud
		bool luces_estaticas;
		int camara_seleccionada;
		animacion_estado estado_animacion;
		tipo_velocidad t_vel;
		bool texturas_cargadas;
		bool listas_creadas;
		bool texturas_visibles;
		
		bool anterior_mostrar_ejes;
		modo_vis anterior_modo;
		modo_ilu anterior_iluminacion;
		bool anterior_luces_estaticas;
		int anterior_camara_seleccionada;
		animacion_estado anterior_estado_animacion;
		tipo_velocidad anterior_t_vel;
		bool anterior_texturas_visibles;
		
		GLint lista_estatico; // Identificador lista objetos est�ticos

	public:
		Escena();
		
		/**
			@brief Libera los recursos reservados
		*/
		void liberarRecursos();
		
		/**
			@brief Funci�n de dibujado
		*/
		void draw();
		
		/**
			@brief Actualiza las animaciones de la escena
		*/
		void actualizar_escena();
		
		/**
			@brief Inicia la animaci�n o la reanuda en caso de que estuviese pausada
		*/
		void iniciarAnimacion();
		
		/**
			@brief Pausa la animaci�n
		*/
		void pausarAnimacion();
		
		/**
			@brief Detiene la animaci�n
		*/
		void detenerAnimacion();
		
		/**
			@brief Alterna el pintado de los ejes
		*/
		void altEjes();
		
		/**
			@brief Alterna entre luces est�ticas y luces que giran con la escena
		*/
		void altLucesEstaticas();
		
		/**
			@brief Configura el modo de visualizaci�n
			@param[in] m El modo de visualizaci�n: puntos,lineas,ajedrez,solido
		*/
		void setModo(modo_vis m);
		
		/**
			@brief Configura el modo de iluminaci�n
			@param[in] m El modo de iluminaci�n: ninguno,plano,gouraud
		*/
		void setIluminacion(modo_ilu m);
		
		/**
			@brief Conmuta entre encendido/apagado la luz n
			@param[in] n La luz a conmutar
		*/
		void pulsarInterruptor(int n);
		
		/**
			@brief Enciende todas las luces
		*/
		void encenderTodas();
		
		/**
			@brief Apaga todas las luces
		*/
		void apagarTodas();
		
		/**
			@brief Selecciona la c�mara
		*/
		void setCamara(int n);
		
		/**
			@brief Coloca la c�mara actualmente seleccionada en el lugar que le indique el usuario a trav�s de teclado
		*/
		void colocarCamara();
		
		/**
			@brief Mueve la c�mara actualmente seleccionada en la direcci�n que mira una cantidad c (positivo para avanzar,negativo para retroceder)
			@param[in] c Cantidad a mover
		*/
		void desplazamientoFrontal(float c);
		
		/**
			@brief Mueve la c�mara actualmente seleccionada lateralmente una cantidad c (positivo a la derecha,negativo a la izquierda)
			@param[in] c Cantidad a mover
		*/
		void desplazamientoLateral(float c);
		
		/**
			@brief Mueve la c�mara actualmente seleccionada verticalmente una cantidad c (positivo hacia arriba,negativo hacia abajo)
			@param[in] c Cantidad a mover
		*/
		void desplazamientoVertical(float c);
		
		/**
			@brief Gira la c�mara actualmente seleccionada verticalmente un �ngulo ang (en grados) (positivo hacia arriba,negativo hacia abajo)
			@param[in] ang El �ngulo a girar
		*/
		void giroVertical(float ang);
		
		/**
			@brief Gira la c�mara actualmente seleccionada horizontalmente un �ngulo ang (en grados) (positivo a la izquierda,negativo a la derecha)
			@param[in] ang El �ngulo a girar
		*/
		void giroHorizontal(float ang);
		
		/**
			@brief Rota la c�mara actualmente seleccionada un �ngulo ang (en grados) (positivo en sentido horario, negativo en sentido antihorario)
			@param[in] ang El �ngulo a girar
		*/
		void rotacion(float ang);
		
		/**
			@brief Coloca la c�mara en la posici�n que ten�a al inicio del programa
			@param[in] i El �ndice en el vector de c�maras
		*/
		void reiniciarCamara(int i);
		
		/**
			@brief Consulta el �ndice en el vector de c�maras de la c�mara seleccionada
			@return El �ndice de la c�mara seleccionada
		*/
		int camaraSeleccionada();
		
		void camaraOvejaSiguiente();
		
		void camaraOvejaAnterior();
		
		void incrementarVelocidad();
		
		void decrementarVelocidad();
		
		void incrementarVelocidadAngular();
		
		void decrementarVelocidadAngular();
		
		void setTipoVelocidad(tipo_velocidad t);
		
		/**
			@brief Alterna el uso de textura
		*/
		void altTexturas();
		
		/**
			@brief A�ade una nueva cr�a al reba�o
		*/
		void anadirOveja();
		
		/**
			@brief Elimina la oveja m�s vieja del reba�o
		*/
		void quitarOveja();
		
		void mostrarNumeroOvejas();
		
	private:
		/**
			@brief Se muestra por consola el estado del programa
		*/
		void mostrarOpciones();
		
		/**
			@brief Pinta el cesped
			@param[in] iluminaci�n Modo de iluminaci�n: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar en modo ajedrez (caras pares de un color e impares de otro)
		*/
		void pintarCesped(modo_ilu iluminacion,bool ajedrez);
		
		/**
			@brief Pinta la valla
			@param[in] iluminaci�n Modo de iluminaci�n: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar en modo ajedrez (caras pares de un color e impares de otro)
		*/
		void pintarValla(modo_ilu iluminacion,bool ajedrez);
		
		/**
			@brief Pinta el fondo
			@param[in] iluminaci�n Modo de iluminaci�n: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar en modo ajedrez (caras pares de un color e impares de otro)
		*/
		void pintarFondo(modo_ilu iluminacion,bool ajedrez);
		
		/**
			@brief Pinta los �rboles
			@param[in] iluminaci�n Modo de iluminaci�n: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar en modo ajedrez (caras pares de un color e impares de otro)
		*/
		void pintarArboles(modo_ilu iluminacion,bool ajedrez);
		
		void pintarEstanque(modo_ilu iluminacion,bool ajedrez);
		
		void pintarPrimitivas(modo_ilu iluminacion,bool ajedrez);
		
		void pintarComedero(modo_ilu iluminacion,bool ajedrez);
		
		void pintarCobertizo(modo_ilu iluminacion,bool ajedrez);
		
		void pintarCaseta(modo_ilu iluminacion,bool ajedrez);
		
		void nuevaCria(_O3D_Modelo_Animado * madre);
		
		GLint CreaListaEstatico(modo_ilu iluminacion,bool ajedrez_activo);
};

#endif // ESCENA_H

