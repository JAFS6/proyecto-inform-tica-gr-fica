#ifndef _OVEJA_SIN_H
#define _OVEJA_SIN_H

#include "_modelo3d.h"
#include "cilindro.h"
#include "esfera.h"

class Oveja_Sin : public _Modelo3D
{
	private:
		Cilindro cilindro;
		Esfera esfera;
		GLint display_list;
		bool lista_hecha;
		modo_ilu anterior_iluminacion;
		bool anterior_ajedrez;
		
	public:
		/**
			@brief Crea una oveja
        */
		Oveja_Sin();
		
		/**
			@brief Dibuja la oveja centrada en los ejes X,Y,Z orientada en el sentido positivo del eje Z
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el coche en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
};

#endif // _OVEJA_SIN_H

