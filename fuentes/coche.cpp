#include "coche.h"

Coche::Coche()
{
	tam_x = chasis.getAncho() + rueda.getAncho();
	tam_y = chasis.getAlto() + capota.getAlto() + (rueda.getDiametro()/2.0);
	tam_z = chasis.getLargo() + (faro.getProfundidad()/2.0);
	
	float mayor;
	
	if(tam_x > tam_y)
		mayor = tam_x;
	else
		mayor = tam_y;
		
	if(tam_z > mayor) mayor = tam_z;
	
	radio_esfera_englobante = mayor/2.0;
}

void Coche::dibuja(modo_ilu iluminacion,bool ajedrez)
{
	float aux,aux2;
	glPushMatrix();
		glTranslatef(0,-1,-0.25); // Para centrar el coche en los 3 ejes;
		chasis.dibuja(iluminacion,ajedrez);
		glPushMatrix();
			aux = (chasis.getAlto()/2.0)+(capota.getAlto()/2.0);
			glTranslatef(0,aux,-0.75);
			capota.dibuja(iluminacion,ajedrez);
		glPopMatrix();
		glPushMatrix();
			aux = chasis.getLargo()/2.0;
			glTranslatef(0,0,aux);
			aux = (chasis.getAncho()/2.0) *0.75;
			glPushMatrix();
				glTranslatef(aux,0,0);
				faro.dibuja(iluminacion,ajedrez);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(-aux,0,0);
				faro.dibuja(iluminacion,ajedrez);
			glPopMatrix();
		glPopMatrix();
		glPushMatrix();
			aux = (chasis.getAlto()/2.0);
			glTranslatef(0,-aux,0);
			aux = (chasis.getAncho()/2.0);
			aux2 = (chasis.getLargo()/2.0) *0.6;
			glPushMatrix();
				glTranslatef(-aux,0,0);
				glPushMatrix();
					glTranslatef(0,0,aux2);
					rueda.dibuja(iluminacion,ajedrez);
				glPopMatrix();
				glPushMatrix();
					glTranslatef(0,0,-aux2);
					rueda.dibuja(iluminacion,ajedrez);
				glPopMatrix();
			glPopMatrix();
			glPushMatrix();
				glTranslatef(aux,0,0);
				glPushMatrix();
					glTranslatef(0,0,aux2);
					rueda.dibuja(iluminacion,ajedrez);
				glPopMatrix();
				glPushMatrix();
					glTranslatef(0,0,-aux2);
					rueda.dibuja(iluminacion,ajedrez);
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
}

