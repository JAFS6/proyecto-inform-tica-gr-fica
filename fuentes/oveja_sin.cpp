#include "oveja_sin.h"

Oveja_Sin::Oveja_Sin()
{
	lista_hecha = false;
	tam_x = 1.4;
	tam_y = 2.1;
	tam_z = 2.6;
	
	float mayor;
	
	if(tam_x > tam_y)
		mayor = tam_x;
	else
		mayor = tam_y;
		
	if(tam_z > mayor) mayor = tam_z;
	
	radio_esfera_englobante = mayor/2.0;
	
	cilindro.setNumDiv(12);
	esfera.setNumDiv(20,20);
}

void Oveja_Sin::dibuja(modo_ilu iluminacion,bool ajedrez)
{	
	if(!lista_hecha || iluminacion != anterior_iluminacion || ajedrez != anterior_ajedrez){
		if(!lista_hecha)
			lista_hecha = true;
		else
			glDeleteLists(display_list, 1);
			
		display_list = glGenLists(1);
	
		glNewList(display_list,GL_COMPILE);
		
			cilindro.setAmbient(0.1,0.1,0.1);
			cilindro.setDiffuse(0.316,0.234,0.023);
			cilindro.setSpecular(0,0,0);
			cilindro.setShininess(0);

			glPushMatrix();
		
				glTranslatef(0,-1.05,-0.05);
		
				// Patas
		
				glPushMatrix();
					glTranslatef(0.25,0.25,0.75);
					glScalef(0.2,0.6,0.2);
					cilindro.dibuja(iluminacion,ajedrez);
				glPopMatrix();
		
				glPushMatrix();
					glTranslatef(-0.25,0.25,0.75);
					glScalef(0.2,0.6,0.2);
					cilindro.dibuja(iluminacion,ajedrez);
				glPopMatrix();
		
				glPushMatrix();
					glTranslatef(0.25,0.25,-0.75);
					glScalef(0.2,0.6,0.2);
					cilindro.dibuja(iluminacion,ajedrez);
				glPopMatrix();
		
				glPushMatrix();
					glTranslatef(-0.25,0.25,-0.75);
					glScalef(0.2,0.6,0.2);
					cilindro.dibuja(iluminacion,ajedrez);
				glPopMatrix();
	
				glTranslatef(0,0.8,0);
		
				esfera.setAmbient(0,0,0);
				esfera.setDiffuse(0.2,0.2,0.2);
				esfera.setSpecular(0,0,0);
				esfera.setShininess(0);
		
				// Cuerpo
		
				glPushMatrix();
					glScalef(1.5,1.2,2.5);
					esfera.dibuja(iluminacion,ajedrez);
				glPopMatrix();
		
				// Cuello
		
				glTranslatef(0,0.5,1);
		
				glPushMatrix();
					glRotatef(45,1,0,0);
					glScalef(0.3,0.3,0.3);
					cilindro.dibuja(iluminacion,ajedrez);
				glPopMatrix();
		
				// Cabeza
				glPushMatrix();
					glTranslatef(0,0.1,0.2);
					glRotatef(45,1,0,0);
					glScalef(0.5,0.4,0.6);
					esfera.dibuja(iluminacion,ajedrez);
				glPopMatrix();
		
				// Ojos
		
				esfera.setDiffuse(0,0,0);
				esfera.setSpecular(0.5,0.5,0.5);
				esfera.setShininess(32);
		
				glPushMatrix();
					glTranslatef(0.18,0.2,0.25);
					glScalef(0.1,0.1,0.1);
					esfera.dibuja(iluminacion,ajedrez);
				glPopMatrix();
		
				glPushMatrix();
					glTranslatef(-0.18,0.2,0.25);
					glScalef(0.1,0.1,0.1);
					esfera.dibuja(iluminacion,ajedrez);
				glPopMatrix();
	
			glPopMatrix();
		
		glEndList();
		
		lista_hecha = true;
	}
	anterior_iluminacion = iluminacion;
	anterior_ajedrez = ajedrez;
	
	glCallList(display_list);
}

