#include "camara.h"

Camara::Camara()
{
	vrp.x = 0;
	vrp.y = 0;
	vrp.z = 0;
	
	vpn.x = 0;
	vpn.y = 0;
	vpn.z = 1;
	
	vup.x = 0;
	vup.y = 1;
	vup.z = 0;
	
	vel_mov = 0.3;
	
	inicializa();
	calcularTransformacion();
}

void Camara::setCamara(Punto3D nuevo_vrp,Punto3D nuevo_vpn,Punto3D nuevo_vup)
{
	vrp = nuevo_vrp;
	vpn = nuevo_vpn;
	vup = nuevo_vup;
	
	inicializa();
	calcularTransformacion();
}

void Camara::setCamara(float nuevo_vrp_x,float nuevo_vrp_y,float nuevo_vrp_z,
					   float nuevo_vpn_x,float nuevo_vpn_y,float nuevo_vpn_z,
					   float nuevo_vup_x,float nuevo_vup_y,float nuevo_vup_z)
{
	vrp.x = nuevo_vrp_x;
	vrp.y = nuevo_vrp_y;
	vrp.z = nuevo_vrp_z;
	
	vpn.x = nuevo_vpn_x;
	vpn.y = nuevo_vpn_y;
	vpn.z = nuevo_vpn_z;
	
	vup.x = nuevo_vup_x;
	vup.y = nuevo_vup_y;
	vup.z = nuevo_vup_z;
	
	inicializa();
	calcularTransformacion();
}

void Camara::situarCamara()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(-gamma,0,0,1);
	glRotatef(-beta,0,1,0);
	glRotatef(alpha,1,0,0);
	glTranslatef(-vrp.x,-vrp.y,-vrp.z);
}

Punto3D Camara::getVRP()
{
	return vrp;
}

Punto3D Camara::getVPN()
{
	return vpn;
}

Punto3D Camara::getVUP()
{
	return vup;
}

void Camara::desplazamientoFrontal(float c)
{
	vrp.x -= c * n.x;
	vrp.y -= c * n.y;
	vrp.z -= c * n.z;
}

void Camara::desplazamientoLateral(float c)
{
	vrp.x += c * u.x;
	vrp.y += c * u.y;
	vrp.z += c * u.z;
}

void Camara::desplazamientoVertical(float c)
{
	vrp.y += c * v.y;
	/*vrp.x += c * v.x;
	vrp.y += c * v.y;
	vrp.z += c * v.z;*/
}

void Camara::giroVertical(float ang)
{   
    Punto3D vector_giro = productoVectorial(vpn,vup); // Vector sobre el que se hará el giro, será perpendicular al VPN y al VUP
    
    normaliza_angulo(ang);
    float ang_rad = grados2radianes(ang);

	// Agrupamiento de cálculos comunes
	float seno = sin(ang_rad);
	float coseno = cos(ang_rad);
	
    float producto_escalar = productoEscalar(vector_giro,vpn);
    
    Punto3D producto_vectorial = productoVectorial(vector_giro,vpn);
    
    Punto3D primera_componente;
    
    primera_componente.x = producto_escalar*vector_giro.x;
    primera_componente.y = producto_escalar*vector_giro.y;
    primera_componente.z = producto_escalar*vector_giro.z;
    
    Punto3D tercera_componente;
    
    tercera_componente.x = seno * producto_vectorial.x;
    tercera_componente.y = seno * producto_vectorial.y;
    tercera_componente.z = seno * producto_vectorial.z;
    
    vpn.x = (primera_componente.x) + (coseno * (vpn.x - primera_componente.x)) - (tercera_componente.x);
    vpn.y = (primera_componente.y) + (coseno * (vpn.y - primera_componente.y)) - (tercera_componente.y);
    vpn.z = (primera_componente.z) + (coseno * (vpn.z - primera_componente.z)) - (tercera_componente.z);
    
    inicializa();
	calcularTransformacion();
}

void Camara::giroHorizontal(float ang)
{
    Punto3D vector_giro = vup; // Vector sobre el que se hará el giro (VUP)
    
    normaliza_angulo(ang);
    float ang_rad = grados2radianes(ang);

	// Agrupamiento de cálculos comunes
	float seno = sin(ang_rad);
	float coseno = cos(ang_rad);
	
    float producto_escalar = productoEscalar(vector_giro,vpn);
    
    Punto3D producto_vectorial = productoVectorial(vector_giro,vpn);
    
    Punto3D primera_componente;
    
    primera_componente.x = producto_escalar*vector_giro.x;
    primera_componente.y = producto_escalar*vector_giro.y;
    primera_componente.z = producto_escalar*vector_giro.z;
    
    Punto3D tercera_componente;
    
    tercera_componente.x = seno * producto_vectorial.x;
    tercera_componente.y = seno * producto_vectorial.y;
    tercera_componente.z = seno * producto_vectorial.z;
    
    vpn.x = (primera_componente.x) + (coseno * (vpn.x - primera_componente.x)) + (tercera_componente.x);
    vpn.y = (primera_componente.y) + (coseno * (vpn.y - primera_componente.y)) + (tercera_componente.y);
    vpn.z = (primera_componente.z) + (coseno * (vpn.z - primera_componente.z)) + (tercera_componente.z);
    
    inicializa();
	calcularTransformacion();
}

void Camara::rotacion(float ang)
{
	Punto3D vector_giro = vpn; // Vector sobre el que se hará el giro VPN
    
    normaliza_angulo(ang);
    float ang_rad = grados2radianes(ang);

	// Agrupamiento de cálculos comunes
	float seno = sin(ang_rad);
	float coseno = cos(ang_rad);
	
    float producto_escalar = productoEscalar(vector_giro,vup);
    
    Punto3D producto_vectorial = productoVectorial(vector_giro,vup);
    
    Punto3D primera_componente;
    
    primera_componente.x = producto_escalar*vector_giro.x;
    primera_componente.y = producto_escalar*vector_giro.y;
    primera_componente.z = producto_escalar*vector_giro.z;
    
    Punto3D tercera_componente;
    
    tercera_componente.x = seno * producto_vectorial.x;
    tercera_componente.y = seno * producto_vectorial.y;
    tercera_componente.z = seno * producto_vectorial.z;
    
    Punto3D comprobar;
    
    comprobar.x = (primera_componente.x) + (coseno * (vup.x - primera_componente.x)) - (tercera_componente.x);
    comprobar.y = (primera_componente.y) + (coseno * (vup.y - primera_componente.y)) - (tercera_componente.y);
    comprobar.z = (primera_componente.z) + (coseno * (vup.z - primera_componente.z)) - (tercera_componente.z);
    
    normalizaVector(comprobar);
    
    Punto3D normalY;
    
    normalY.x = 0;
    normalY.y = 1;
    normalY.z = 0;
    
    if(radianes2grados(acos(productoEscalar(normalY,comprobar))) < 10 && comprobar.y > 0){
	    vup = comprobar;
    
		inicializa();
		calcularTransformacion();
	}
}

void Camara::inicializa()
{
	n = vpn;
	normalizaVector(n.x,n.y,n.z);
	
	u = productoVectorial(vup,n);
	normalizaVector(u.x,u.y,u.z);
	
	v = productoVectorial(n,u);
	normalizaVector(v.x,v.y,v.z);
	
	alpha = 0;
	beta  = 0;
	gamma = 0;
}

void Camara::calcularTransformacion()
{	
	double alpha_rad,beta_rad,gamma_rad;
	Punto3D n1,u1,u2;//n2,v1,v2;
	
	// Ángulo alpha
	
	if(n.y == 0) alpha_rad = 0;
	else alpha_rad = atan2(n.y,n.z);
	
	alpha = radianes2grados(alpha_rad);
	
	n1 = rotX(alpha_rad,n);
	u1 = rotX(alpha_rad,u);
	//v1 = rotX(alpha_rad,v);
	
	normalizaVector(n1.x,n1.y,n1.z);
	normalizaVector(u1.x,u1.y,u1.z);
	//normalizaVector(v1.x,v1.y,v1.z);
	
	// Ángulo beta
	
	if(n1.x == 0) beta_rad = 0;
	else beta_rad = atan2(n1.x,n1.z);
	
	beta = radianes2grados(beta_rad);
	
	//n2 = rotY(-beta_rad,n1);
	u2 = rotY(-beta_rad,u1);
	//v2 = rotY(-beta_rad,v1);
	
	//normalizaVector(n2.x,n2.y,n2.z);
	normalizaVector(u2.x,u2.y,u2.z);
	//normalizaVector(v2.x,v2.y,v2.z);
	
	// Ángulo gamma
	if(u2.y == 0) gamma_rad = 0;
	else gamma_rad = atan2(u2.y,u2.x);
	
	gamma = radianes2grados(gamma_rad);
}

