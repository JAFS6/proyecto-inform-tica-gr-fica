#ifndef _IMPOSTOR_H_
#define _IMPOSTOR_H_

#include "auxiliar.h"

class _Impostor
{
	private:
		int id;
		unsigned alto;
		unsigned ancho;
		
	public:
		_Impostor();
		
		/**
			@brief Obtiene el alto de la imagen
			@return El el alto de la imagen
		*/
		unsigned getAlto();
		
		/**
			@brief Obtiene el ancho de la imagen
			@return El el ancho de la imagen
		*/
		unsigned getAncho();
		
		/**
			@brief Establece la imagen a mostrar
			@param[in] img El nombre del archivo de la imagen
			@pre Debe llamarse en la función de pintar escena
		*/
		void setImagen(const char * img);
		
		/**
			@brief Dibuja el impostor centrado en los ejes de coordenadas, paralelo al plano XY
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] transparente True para habilitar el uso de transparencia
			@param[in] ajedrez True para dibujar el impostor en modo ajedrez (caras pares de un color e impares de otro) False por defecto
			@param[in] textura_visible True para ver la textura, True por defecto
			Nota: Dibujar ante los elementos sin transparencias
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool transparente=true,bool ajedrez=false,bool textura_visible=true);
};

#endif // _IMPOSTOR_H_

