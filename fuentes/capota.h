#ifndef CAPOTA_H
#define CAPOTA_H

#include "cubo.h"

class Capota
{
	private:
		Cubo capota;
		float ancho;
		float largo;
		float alto;
		
	public:
		/**
			@brief Crea una capota de plástico verde de 5 de ancho por 6 de largo por 3 de alto
        */
		Capota();
		
		/**
			@brief Crea una capota de plástico verde de ancho por largo por alto
			@param[in] ancho El ancho de la capota
			@param[in] largo El largo de la capota
			@param[in] alto El alto de la capota
        */
		Capota(float ancho,float largo,float alto);
		
		/**
			@brief Dibuja la capota en posición horizontal centrada en los ejes X,Y,Z orientada en el sentido positivo del eje Z
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar la capota en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
		  @brief Obtiene el ancho de la capota
		  @return El ancho de la capota
		*/
		float getAncho();
		
		/**
		  @brief Establece el ancho de la capota
		  @param[in] a El nuevo ancho de la capota
		*/
		void setAncho(float a);
		
		/**
		  @brief Obtiene el largo de la capota
		  @return El largo de la capota
		*/
		float getLargo();
		
		/**
		  @brief Establece el largo de la capota
		  @param[in] l El nuevo largo de la capota
		*/
		void setLargo(float l);
		
		/**
		  @brief Obtiene el alto de la capota
		  @return El alto de la capota
		*/
		float getAlto();
		
		/**
		  @brief Establece el alto de la capota
		  @param[in] a El nuevo alto de la capota
		*/
		void setAlto(float a);
		
		/**
			@brief Configura la reflexión ambiental
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setAmbient  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión difusa
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setDiffuse  (float r,float g,float b);
		
		/**
			@brief Configura la reflexión especular
			@param[in] r Componente roja del color
			@param[in] g Componente verde del color
			@param[in] b Componente azul del color
		*/
		void setSpecular (float r,float g,float b);
		
		/**
			@brief Configura el exponente especular
			@param[in] s Exponente especular
		*/
		void setShininess(float s);
		
		/**
			@brief Obtiene los valores de la reflexión ambiental
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getAmbient  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión difusa
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getDiffuse  (float &r,float &g,float &b);
		
		/**
			@brief Obtiene los valores de la reflexión especular
			@param[out] r Componente roja del color
			@param[out] g Componente verde del color
			@param[out] b Componente azul del color
		*/
		void getSpecular (float &r,float &g,float &b);
		
		/**
			@brief Obtiene el valor del exponente especular
			@param[out] s Exponente especular
		*/
		void getShininess(float &s);
};

#endif // CAPOTA_H

