#ifndef _OBJECT3DG_H
#define _OBJECT3DG_H

#include <GL/gl.h>
#include "auxiliar.h"

using namespace std;

class _Object3DG
{
    protected:
        vector<Punto3D> vertices;
        Color color;
    public:
        _Object3DG();
        /**
          @brief Asigna un nuevo color al objeto mediante las tres componentes
          @param[in] r Componente roja del color del objeto
          @param[in] g Componente verde del color del objeto
          @param[in] b Componente azul del color del objeto
        */
        void setColor(float r,float g,float b);
};

#endif // _OBJECT3DG_H
