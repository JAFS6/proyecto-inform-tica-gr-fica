#include "luz.h"

Luz::Luz()
{
	reflexion_ambiental.r = reflexion_ambiental.g = reflexion_ambiental.b = 1;
	reflexion_difusa.r = reflexion_difusa.g = reflexion_difusa.b = 1;
	reflexion_especular.r = reflexion_especular.g = reflexion_especular.b = 1;
}

void Luz::encender()
{
	GLfloat ambiente[]  = {reflexion_ambiental.r,reflexion_ambiental.g,reflexion_ambiental.b,1.0};
	GLfloat difuso[]    = {reflexion_difusa.r   ,reflexion_difusa.g   ,reflexion_difusa.b   ,1.0};
	GLfloat especular[] = {reflexion_especular.r,reflexion_especular.g,reflexion_especular.b,1.0};
	
	glLightfv(GL_LIGHT0+nombre,GL_AMBIENT,ambiente);
	glLightfv(GL_LIGHT0+nombre,GL_DIFFUSE,difuso);
	glLightfv(GL_LIGHT0+nombre,GL_SPECULAR,especular);
	glLightfv(GL_LIGHT0+nombre,GL_POSITION,posicion);
	
	glEnable(GL_LIGHT0+nombre);
}

void Luz::apagar()
{
	glDisable(GL_LIGHT0+nombre);
}

void Luz::setNombre(int n)
{
	if(n >= 0 && n < GL_MAX_LIGHTS){
		nombre = n;
	}
	else{
		cerr << "La luz no puede tener el nombre " << n << endl;
		exit(1);
	}
}

int Luz::getNombre()
{
	return nombre;
}

void Luz::setPosition(float x,float y,float z,float w)
{
	posicion[0] = x;
	posicion[1] = y;
	posicion[2] = z;
	posicion[3] = w;
}

void Luz::getPosition(float &x,float &y,float &z,float &w)
{
	x = posicion[0];
	y = posicion[1];
	z = posicion[2];
	w = posicion[3];
}

void Luz::setAmbient (float r,float g,float b)
{
	reflexion_ambiental.r = r;
	reflexion_ambiental.g = g;
	reflexion_ambiental.b = b;
}

void Luz::setDiffuse (float r,float g,float b)
{
	reflexion_difusa.r = r;
	reflexion_difusa.g = g;
	reflexion_difusa.b = b;
}

void Luz::setSpecular(float r,float g,float b)
{
	reflexion_especular.r = r;
	reflexion_especular.g = g;
	reflexion_especular.b = b;
}

void Luz::getAmbient (float &r,float &g,float &b)
{
	r = reflexion_ambiental.r;
	g = reflexion_ambiental.g;
	b = reflexion_ambiental.b;
}

void Luz::getDiffuse (float &r,float &g,float &b)
{
	r = reflexion_difusa.r;
	g = reflexion_difusa.g;
	b = reflexion_difusa.b;
}

void Luz::getSpecular(float &r,float &g,float &b)
{
	r = reflexion_especular.r;
	g = reflexion_especular.g;
	b = reflexion_especular.b;
}


