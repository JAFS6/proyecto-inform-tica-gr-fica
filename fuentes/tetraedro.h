#ifndef TETRAEDRO_H
#define TETRAEDRO_H

#include "_object3d.h"

class Tetraedro : public _Object3D
{
    private:
        float lado;
        
    public:
        /**
          @brief Crea un tetraedro plateado de lado 1
        */
        Tetraedro();
        
        /**
          @brief Crea un tetraedro plateado de lado l
          @param[in] l El tama�o del lado del tetraedro
        */
        Tetraedro(float l);
        
        /**
          @brief Obtiene la medida del lado del tetraedro
          @return La medida del lado
        */
        float getLado();
        
        /**
          @brief Asigna una nueva medida al lado del tetraedro y recalcula la geometr�a
          @param[in] l La nueva medida del lado
        */
        void setLado(float l);
        
    private:
        /**
          @brief Crea la geometr�a del tetraedro
        */
        void creaGeometria();
        
        /**
          @brief Crea la topolog�a del tetraedro
        */
        void creaTopologia();
};

#endif // TETRAEDRO_H

