#ifndef _O3D_MODELO_ANIMADO_H_
#define _O3D_MODELO_ANIMADO_H_

#include "auxiliar.h"
#include "_modelo3d.h"
#include "_o3d_animado.h"

class _O3D_Modelo_Animado : public _O3D_Animado
{
	private:
		_Modelo3D * objeto1;
		_Modelo3D * objeto2;
		
	public:
		/**
			@brief Crea un controlador de objeto modelo animado por defecto asignándole velocidad lineal igual a 1, con estado idle, posición en el origen de coordenadas, y orientación hacia el lado positivo del eje Z
		*/
		_O3D_Modelo_Animado();
		
		/**
			@brief Crea un controlador de objeto modelo animado para el objeto indicado, asignándole por defecto velocidad lineal igual a 1, con estado idle, posición en el origen de coordenadas, y orientación hacia el lado positivo del eje Z
		*/
		_O3D_Modelo_Animado(_Modelo3D * o1);
		
		/**
			@brief Dibuja el objeto animado
			@param[in] iluminación Modo de iluminación: ninguno,plano,gouraud
			@param[in] ajedrez True para dibujar el objeto en modo ajedrez (caras pares de un color e impares de otro) False por defecto
		*/
		void dibuja(modo_ilu iluminacion=gouraud,bool ajedrez=false);
		
		/**
			@brief Establece el objeto que se animará
			@param[in] o El objeto
		*/
		void setObjeto(_Modelo3D * o);
		
		/**
			@brief Establece el 2º objeto que se animará
			@param[in] o El objeto
		*/
		void setObjeto2(_Modelo3D * o);
		
		/**
			@brief Obtiene el radio de la esfera englobante
			@return El radio obtenido
		*/
		float getRadioEsferaEnglobante();
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje X
			@return El tamaño obtenido
		*/
		float getTamX();
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje Y
			@return El tamaño obtenido
		*/
		float getTamY();
		
		/**
			@brief Obtiene el tamaño de la caja englobante en el eje Z
			@return El tamaño obtenido
		*/
		float getTamZ();
		
		void hacerAdulto();
};

#endif // _O3D_MODELO_ANIMADO_H

